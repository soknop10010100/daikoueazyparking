package com.daikou.eazy_pass.data.apiConfiguration

import android.content.Context
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.DeviceConfig
import com.daikou.eazy_pass.util.LanguageManager
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseViewModel
import java.util.*
import kotlin.collections.HashMap

object Header : BaseActivity<BaseViewModel>() {
    open fun getHeader(requireAuth : AuthRequire) : HashMap<String, Any>{
        val headerMap = HashMap<String , Any>()
        headerMap["app-type"] = Constants.Config.APP_TYPE
        headerMap["Content-Type"] = "application/json"
        val deviceId: String? = (this as BaseActivity<*>).deviceId()
        val token = Helper.getStringSharePreference(mContext,  Constants.Auth.TOKEN_KEY)
        when {
            requireAuth == AuthRequire.REQUIRED -> {
                headerMap["Authorization"] = "Bearer $token"
            }
        }
        when {
            deviceId != null -> {
                headerMap["device-id"] = deviceId
            }
            else -> {
                headerMap["device-id"] =  String.format("%s", Date().time)
            }
        }
        headerMap["language_code"] = LanguageManager.getLanguage(mContext)
//        headerMap["device-token"] = ""
//        headerMap["x-device-type"] = "mobile"
//        headerMap["Accept-Encoding"] = "Accept-Encoding"
//        headerMap["Content-Encoding"] = "gzip"
//        headerMap["current-latitude"] = 11.912344
//        headerMap["current-longitude"] = 11.783321
//        val serialNumber: String? = DeviceConfig.getSerialNumber()
        return headerMap
    }

    enum class AuthRequire{
        NOT_REQUIRE, REQUIRED
    }

    open fun getHeader() : HashMap<String, Any>{
        val headerMap = HashMap<String , Any>()
        headerMap["app-type"] = Constants.Config.APP_TYPE
        return headerMap
    }
}