package com.daikou.eazy_pass.data.model;

import com.daikou.eazy_pass.enumerable.HomeAction;

public class HomeRowLayoutModel {
    private String label;
    private int icon;
    private HomeAction action;
    public HomeRowLayoutModel(String label, int icon, HomeAction action) {
        this.label = label;
        this.icon = icon;
        this.action = action;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public int getIcon() {
        return icon;
    }

    public HomeAction getAction() {
        return action;
    }
}
