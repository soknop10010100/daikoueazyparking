package com.daikou.eazy_pass.data.model.response_model

data class Password (
    val newPwd: Int? = null,
    val confirmPwd: Int? = null,
    val pwdNotMatch: Int? = null,
    val hasDoneValidate: Boolean? = false)