package com.daikou.eazy_pass.data.handler

import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String?
        get() = "No connectivity exception"
}
