package com.daikou.eazy_pass.data.apiConfiguration

import android.annotation.SuppressLint
import android.util.Log
import com.daikou.eazy_pass.BuildConfig
import com.daikou.eazy_pass.data.service.ApiService
import com.daikou.eazy_pass.util.Constants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

open class ApiConfigure {
    protected lateinit var mService: ApiService

    init {
        initService()
    }

    private fun initService() {
        mService = createService()
    }

    fun intitRetrofit(): Retrofit {

        val logging = HttpLoggingInterceptor { message -> debug("RAW_BODY", message) }
        logging.level =  HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().apply {
            try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts: Array<TrustManager> = arrayOf(
                    object : X509TrustManager {
                        @SuppressLint("TrustAllX509TrustManager")
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {

                        }

                        @SuppressLint("TrustAllX509TrustManager")
                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {

                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }
                    }
                )

                // Install the all-trusting trust manager
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())
                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory
                this.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                this.hostnameVerifier { _, _ -> true }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            networkInterceptors().add(Interceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                    .method(original.method, original.body)
                    .build()
                chain.proceed(request)
            })
            if (BuildConfig.DEBUG) {
                addInterceptor(logging)
            }
        }

        client.readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
        val builder =
            Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.Config.BASE_URL)
        builder.client(client.build())
        return builder.build()
    }

    private fun createService() : ApiService {
        return intitRetrofit().create(ApiService:: class.java)
    }

    private val isLog = true
    fun debug(tag: String = "TEMP: ", message: String){
        if(isLog){
            Log.d(tag, message)
        }
    }

}