package com.daikou.eazy_pass.data.model.response_model

import com.google.gson.annotations.SerializedName

data class ActivityHistory(
    @SerializedName("created_at")
    val createAt : String? = null,
    @SerializedName("duration")
    val duration : String? = null,
    @SerializedName("from_date")
    val fromDate : String? = null,
    @SerializedName("id")
    val id : Int? = null,
    @SerializedName("payment_status")
    val paymentStatus : String? = null,
    @SerializedName("project_id")
    val projectId : Int? = null,
    @SerializedName("project_name")
    val projectName : String? = null,
    @SerializedName("status")
    val status : String? = null,
    @SerializedName("ticket_no")
    val ticketNo : String? = null,
    @SerializedName("to_date")
    val toDate : String? = null,
    @SerializedName("total_price")
    val totalPrice : Double? = null,



)
