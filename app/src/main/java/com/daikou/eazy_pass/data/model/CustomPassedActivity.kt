package com.daikou.eazy_pass.data.model

class CustomPassedActivity (
    total : Double? = null,
    message : String? = null,
    entranceAt : String = "",
    exitAt : String = "",
    var isShowHeadDate : Boolean = false
) : PassedActivityModel(total, message, entranceAt, exitAt)