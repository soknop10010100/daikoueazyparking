package com.daikou.eazy_pass.data.handler

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.helper.MessageUtil

object NetworkHandler {

    fun checkConnection(context: Context) : Boolean{
        return if (isConnectedNetwork(context)){
            true
        }else{
            MessageUtil.showError(context, "", context.getString(R.string.no_connection_hint))
            false
        }
    }
    private fun isConnectedNetwork(context: Context) : Boolean{
        val connectionManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = connectionManager.getNetworkCapabilities(connectionManager.activeNetwork)
        if(capabilities != null){
            when{
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ->{
                    return true
                }

                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->{
                    return  true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ->{
                    return true
                }
            }
        }

        return  false
    }
}