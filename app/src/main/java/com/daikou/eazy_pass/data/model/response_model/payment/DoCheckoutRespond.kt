package com.daikou.eazy_pass.data.model.response_model.payment

import com.google.gson.annotations.SerializedName
data class DoCheckoutRespond(
    @SerializedName("total_price")
    val total : Double? = null,
    @SerializedName("message")
    val message : String? = null,
)