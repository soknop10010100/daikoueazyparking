package com.daikou.eazy_pass.data.model.response_model.user

import com.google.gson.annotations.SerializedName

class LongTermModel (
    @SerializedName("project_id") var projectId: Int? = null,
    @SerializedName("location") var location: String? = null,
    @SerializedName("from_date") var fromDate: String? = null,
    @SerializedName("to_date") var toDate: String? = null,
    @SerializedName("total_months") var totalMonth: Int? = null,
    @SerializedName("total_price") var double: Double? = null,
    @SerializedName("total_day") var totalDay: Int? = null,
    @SerializedName("total_remaining_day") var totalRemainDay: Int? = null,
    @SerializedName("status") var status: String? = null,
) : java.io.Serializable