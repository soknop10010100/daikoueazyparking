package com.daikou.eazy_pass.data.model

import com.google.gson.annotations.SerializedName

open class PassedActivityModel(
    @SerializedName("total_price")
    val total : Double? = null,
    @SerializedName("message")
    val message : String? = null,
    var entranceAt: String = "",
    var exitAt : String = ""

)
