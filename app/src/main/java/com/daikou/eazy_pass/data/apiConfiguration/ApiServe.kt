package com.daikou.eazy_pass.data.apiConfiguration


import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.data.model.response_model.*
import com.daikou.eazy_pass.data.model.response_model.payment.DoCheckoutRespond
import com.daikou.eazy_pass.data.model.response_model.transaction.TransactionResponse
import com.daikou.eazy_pass.data.model.response_model.user.LongTermModel
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.enumerable.ForgotChangeEnum
import com.google.gson.JsonElement
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ApiServe : ApiConfigure(){

    fun registerUser(body : HashMap<String , Any>): Observable<BaseApiModel<SignUpRespond>> =
        mService.registerUser(Header.getHeader(Header.AuthRequire.NOT_REQUIRE), body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun login(body: HashMap<String, Any>): Observable<BaseApiModel<JsonElement>> =
        mService.loginUser(Header.getHeader(Header.AuthRequire.REQUIRED), body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun logout() : Observable<BaseApiModel<JsonElement>> =
        mService.logout(Header.getHeader(Header.AuthRequire.REQUIRED))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getUserInfo() : Observable<BaseApiModel<User>> =
        mService.getUerInfo(Header.getHeader(Header.AuthRequire.REQUIRED))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getTransaction(bodyQuery: HashMap<String, Any>) : Observable<BaseApiModel<TransactionResponse>> =
        mService.getTransaction(Header.getHeader(Header.AuthRequire.REQUIRED), bodyQuery)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun openGateDoPayment(body: HashMap<String, Any>) : Observable<BaseApiModel<DoCheckoutRespond>> =
        mService.openGate(Header.getHeader(Header.AuthRequire.REQUIRED),body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun deposit(body: HashMap<String, Any>) : Observable<BaseApiModel<DepositResponse>> =
        mService.deposit((Header.getHeader(Header.AuthRequire.REQUIRED)), body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun fetchVehicle() : Observable<BaseApiModel<JsonElement>> =
        mService.fetchVehicleTyp(Header.getHeader(Header.AuthRequire.NOT_REQUIRE))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun withdraw(body: HashMap<String, Any>) : Observable<BaseApiModel<JsonElement>> =
        mService.withdraw(Header.getHeader(Header.AuthRequire.REQUIRED), body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun verifyBankAccount(body: HashMap<String, Any>) : Observable<BaseApiModel<SaveBankAccount>> =
        mService.verifyBankAccount(Header.getHeader(Header.AuthRequire.REQUIRED), body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getActivities(queryMap: HashMap<String, Any>) : Observable<BaseApiModel<ArrayList<ActivityHistory>>> =
        mService.getActivities(Header.getHeader(Header.AuthRequire.REQUIRED), queryMap)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getUserTest() : Observable<JsonElement> =
        mService.getUserTesting()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun submitChangePwd(body : HashMap<String , Any>): Observable<BaseApiModel<JsonElement>> =
        mService.submitChangePwd(Header.getHeader(Header.AuthRequire.REQUIRED), body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun submitRequestVerifyChangePasswordOtp(verifyPinEnum: ForgotChangeEnum,  body : HashMap<String , Any>): Observable<BaseApiModel<JsonElement>> =
        when (verifyPinEnum) {
            ForgotChangeEnum.SubmitRequestOtp -> {  //  REQUEST OTP
                mService.submitRequestOtp(Header.getHeader(Header.AuthRequire.NOT_REQUIRE), body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
            ForgotChangeEnum.SubmitVerifyOtp -> { //  VERIFY OTP
                mService.submitVerifyOtp(Header.getHeader(Header.AuthRequire.NOT_REQUIRE), body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
            ForgotChangeEnum.SubmitChangePasswordByOtp -> { //  VERIFY OTP
                mService.submitChangePasswordByOtp(Header.getHeader(Header.AuthRequire.NOT_REQUIRE), body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }

    fun fetchParkingDevice()  : Observable<JsonElement> =
        mService.fetchParkingDevices(Header.getHeader(Header.AuthRequire.REQUIRED))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    fun resubmitKyc(body: HashMap<String, Any>) : Observable<BaseApiModel<JsonElement>> =
        mService.resubmitKyc(Header.getHeader(Header.AuthRequire.REQUIRED),body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getLongTermRecord(queryMap : HashMap<String, Any>) : Observable<BaseApiModel<ArrayList<LongTermModel>>> =
        mService.getLongTermRecord(Header.getHeader(Header.AuthRequire.REQUIRED),queryMap)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    fun renewLongTerm(projectId : Int) : Observable<BaseApiModel<JsonElement>> =
        mService.renewLongTerm(Header.getHeader(Header.AuthRequire.REQUIRED),projectId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}