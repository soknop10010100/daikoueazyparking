package com.daikou.eazy_pass.data.model

import com.google.gson.annotations.SerializedName

open class TransactionModel(
    @SerializedName("transaction_id")
    var id : String? = null,
    @SerializedName("pay_by")
    var payBy: String? = null,
    @SerializedName("total")
    var total: Double? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("type")
    var type: String? = null
)
