package com.daikou.eazy_pass.data.model.response_model.payment

import com.google.gson.annotations.SerializedName

data class PaymentResponse(
    @SerializedName("message")
    val message : String? = null
)
