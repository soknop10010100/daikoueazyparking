package com.daikou.eazy_pass.data.model

import com.google.gson.annotations.SerializedName

data class SaveBankAccount(
    var userId: Int = -100,
    @SerializedName("account_number")
    val accountNumber : String? = null,
    @SerializedName("owner_name")
    val ownerName :String? = null,
    @SerializedName("currency")
    val currency : String? = null,
    var bic : String = "",
    var accountNumberFormat: String = "",
    var logo: Int = -1,
) : java.io.Serializable
