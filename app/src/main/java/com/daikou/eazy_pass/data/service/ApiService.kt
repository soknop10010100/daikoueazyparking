package com.daikou.eazy_pass.data.service

import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.data.model.response_model.*
import com.daikou.eazy_pass.data.model.response_model.payment.DoCheckoutRespond
import com.daikou.eazy_pass.data.model.response_model.transaction.TransactionResponse
import com.daikou.eazy_pass.data.model.response_model.user.LongTermModel
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.google.gson.JsonElement
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.QueryMap
import rx.Observable

interface ApiService {
    @POST("api/auth/register_parking_customer")
    fun registerUser(
        @HeaderMap header: HashMap<String , Any>,
        @Body body: HashMap<String, Any>): Observable<BaseApiModel<SignUpRespond>>

    @GET("users") // testing api, not related
    fun getUserTesting() : Observable<JsonElement>

    @POST("api/auth/login")
    fun loginUser(
        @HeaderMap header: HashMap<String, Any>,
        @Body body: HashMap<String, Any>
    ): Observable<BaseApiModel<JsonElement>>

    @POST("api/auth/logout")
    fun logout(
        @HeaderMap header: HashMap<String, Any>,
    ): Observable<BaseApiModel<JsonElement>>

    @GET("api/auth/me")
    fun getUerInfo(
        @HeaderMap headerMap: HashMap<String, Any>
    ) : Observable<BaseApiModel<User>>

    @GET("api/wallet/getTransaction")
    fun getTransaction(
        @HeaderMap headerMap: HashMap<String, Any>,
        @QueryMap bodyQuery : HashMap<String, Any>
    ) : Observable<BaseApiModel<TransactionResponse>>

    @POST("api/gate/open")
    fun openGate(
        @HeaderMap headerMap: HashMap<String, Any>,
        @Body body: HashMap<String , Any>
    ) : Observable<BaseApiModel<DoCheckoutRespond>>

    @GET("api/parking/fetch_vehicle_types")
    fun fetchVehicleTyp(
        @HeaderMap headerMap: HashMap<String, Any>,
    ) : Observable<BaseApiModel<JsonElement>>


    @POST("api/wallet/deposit")
    fun deposit(
        @HeaderMap header: HashMap<String, Any>,
        @Body body: HashMap<String , Any>
    ) : Observable<BaseApiModel<DepositResponse>>

    @POST("api/user/withdraw")
    fun withdraw(
        @HeaderMap header: HashMap<String, Any>,
        @Body body: HashMap<String , Any>
    ) : Observable<BaseApiModel<JsonElement>>

    @POST("api/user/verify/bank-account")
    fun verifyBankAccount(
        @HeaderMap headerMap: HashMap<String, Any>,
        @Body body: HashMap<String, Any>
    ) : Observable<BaseApiModel<SaveBankAccount>>

    @GET("api/parking/fetch_activities")
    fun getActivities(
        @HeaderMap headerMap: HashMap<String, Any>,
        @QueryMap queryMap: HashMap<String, Any>
    ) : Observable<BaseApiModel<ArrayList<ActivityHistory>>>

    @POST("api/auth/change-password")
    fun submitChangePwd(
        @HeaderMap header: HashMap<String , Any>,
        @Body body: HashMap<String, Any>): Observable<BaseApiModel<JsonElement>>

    @POST("api/auth/requestOTPForgotPassword")
    fun submitRequestOtp(
        @HeaderMap header: HashMap<String , Any>,
        @Body body: HashMap<String, Any>): Observable<BaseApiModel<JsonElement>>

    @POST("api/auth/verifyOTPForgotPassword")
    fun submitVerifyOtp(
        @HeaderMap header: HashMap<String , Any>,
        @Body body: HashMap<String, Any>): Observable<BaseApiModel<JsonElement>>

    @POST("api/auth/changePasswordByOTP")
    fun submitChangePasswordByOtp(
        @HeaderMap header: HashMap<String , Any>,
        @Body body: HashMap<String, Any>): Observable<BaseApiModel<JsonElement>>


    @GET("api/user/fetch_all_parking_devices")
    fun fetchParkingDevices(
        @HeaderMap headerMap: HashMap<String, Any>,
    ) : Observable<JsonElement>

    @POST("api/auth/resubmit-kyc")
    fun resubmitKyc(
        @HeaderMap header: HashMap<String, Any>,
        @Body body : HashMap<String, Any>
    ) : Observable<BaseApiModel<JsonElement>>

    @GET("api/long_term/allFeeRecord")
    fun getLongTermRecord(
        @HeaderMap header: HashMap<String, Any>,
        @QueryMap queryMap: HashMap<String, Any>
    ) : Observable<BaseApiModel<java.util.ArrayList<LongTermModel>>>

    @GET("api/long_term/renew/{id}")
    fun renewLongTerm(
        @HeaderMap header: HashMap<String, Any>,
        @Path("id") projectId : Int
    ) : Observable<BaseApiModel<JsonElement>>


}