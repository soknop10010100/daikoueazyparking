package com.daikou.eazy_pass.data.handler

class ResponseErrorBody<T>(
    val code: Int,
    val error: T,
    val message: String,
    val success: Boolean
)