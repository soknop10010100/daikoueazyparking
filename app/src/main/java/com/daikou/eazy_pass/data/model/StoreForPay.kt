package com.daikou.eazy_pass.data.model

data class StoreForPay(
    var userId : String,
    var barrier : String
)