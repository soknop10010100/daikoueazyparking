package com.daikou.eazy_pass.data.model.response_model

import com.daikou.eazy_pass.data.model.response_model.user.User
import com.google.gson.annotations.SerializedName

data class DepositResponse(
    @SerializedName("payment_link")
    val paymentLink : String? = null,
    @SerializedName("user")
    val user: User? = null
) : java.io.Serializable
