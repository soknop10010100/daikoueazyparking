package com.daikou.eazy_pass.data.model.response_model

import com.google.gson.annotations.SerializedName

data class VehicleModel(
    @SerializedName("name")
    val name : String? = null,
    val image : String? = null
)
