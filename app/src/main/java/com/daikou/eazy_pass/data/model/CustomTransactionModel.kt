package com.daikou.eazy_pass.data.model

import com.daikou.eazy_pass.data.model.TransactionModel
import com.daikou.eazy_pass.data.model.response_model.transaction.Transaction
import com.daikou.eazy_pass.data.model.response_model.transaction.WithdrawRespond

class CustomTransactionModel (
    bookingCode: String?,
    kessTransactionId: String?,
    payBy: String?,
    total: Double?,
    walletTransactionStatus: String?,
    createdAt: String?,
    mUserWithdraw: WithdrawRespond? ,
    mType: String?,
    remark : String?,
    label : String?,
    var totalAmount : HashMap<String, Double>? = null,
    var isShowHeader : Boolean? = false
) : Transaction(
    bookingCode,
    kessTransactionId,
    payBy,
    total,
    walletTransactionStatus,
    createdAt,
    mUserWithdraw,
    mType,
    remark =  remark,
    label = label
)