package com.daikou.eazy_pass.data.model.response_model.user

import com.google.gson.annotations.SerializedName

data class BarrierRange(@SerializedName("name") val name : String? = null, @SerializedName("range") val range : Int? = null) {
}