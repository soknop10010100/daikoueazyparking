package com.daikou.eazy_pass.data.model.response_model

import com.daikou.eazy_pass.data.model.response_model.user.User
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SignUpRespond(
    @SerializedName("access_token")
    val accessToken : String?,
    @SerializedName("kyc_doc")
    val KycDoc : KYCModel?,
    @SerializedName("user")
    val user : User?,
) : Serializable
