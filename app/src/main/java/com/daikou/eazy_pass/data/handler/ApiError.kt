package com.daikou.eazy_pass.data.handler

data class ApiError(
    val message: String,
    val status: Int
    )
