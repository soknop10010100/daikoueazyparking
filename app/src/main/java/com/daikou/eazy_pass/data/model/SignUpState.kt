package com.daikou.eazy_pass.data.model

data class SignUpState(
    val firstName: Int? = null,
    val lastName: Int? = null,
    val gender: Int? = null,
    val dob: Int? = null,
    val email: Int? = null,
    val phoneNumber: Int? = null,
    val password: Int? = null,
    val confirmPassword: Int? = null,
    val hasDoneValidate: Boolean? = false
)
