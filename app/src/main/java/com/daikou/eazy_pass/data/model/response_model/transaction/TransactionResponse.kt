package com.daikou.eazy_pass.data.model.response_model.transaction

import com.google.gson.annotations.SerializedName

data class TransactionResponse(
    @SerializedName("blocked_balance") var blockedBalance: Double? = null,
    @SerializedName("total_balance") var totalBalance: Double? = null,
    @SerializedName("transactions") var transactions: ArrayList<Transaction> = arrayListOf()) {
}