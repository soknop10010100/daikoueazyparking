package com.daikou.eazy_pass.data.apiConfiguration

import com.google.gson.JsonObject

data class BaseApiModel<T> (
    val code: Int? = null,
    val message: String = "",
    val success_msg: String = "",
    val success: Boolean = false,
    val errors: JsonObject? = null,
    val data: T? = null
)