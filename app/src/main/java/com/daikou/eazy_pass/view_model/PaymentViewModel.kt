package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.PassedActivityModel
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.data.model.response_model.payment.DoCheckoutRespond
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class PaymentViewModel : BaseViewModel() {

    private val _dataPaymentResponse = MutableLiveData<BaseApiModel<DoCheckoutRespond>>()
    private val _errorMessage = MutableLiveData<String>()
    private val _bankAccountResponse = MutableLiveData<BaseApiModel<SaveBankAccount>>()
    private val _withdrawResponse = MutableLiveData<BaseApiModel<JsonElement>>()
    private val _barrierId = MutableLiveData<String>()

    val dataPaymentResponse : MutableLiveData<BaseApiModel<DoCheckoutRespond>> get() = _dataPaymentResponse
    val bankAccountResponse : MutableLiveData<BaseApiModel<SaveBankAccount>> get() = _bankAccountResponse
    val withdrawResponse : MutableLiveData<BaseApiModel<JsonElement>> get() = _withdrawResponse
    val errorMessage : MutableLiveData<String> get() = _errorMessage
    val barrierId : MutableLiveData<String> get() = _barrierId
    var subscription : Subscription? = null

    fun openGateDoPayment(body : HashMap<String, Any>){
        loading.value = true
        subscription = ApiServe().openGateDoPayment(body).subscribe({
            loading.value = false
            _dataPaymentResponse.value = it
//            _barrierId.value = body["barrier_id"]?.toString()
        },{
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }

            }
        })
    }

    fun verifyBackAccount(body: HashMap<String, Any>){
        loading.value = true
        subscription = ApiServe().verifyBankAccount(body).subscribe({
            loading.value = false
            _bankAccountResponse.value = it
        }, {
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }
            }
        })
    }
    fun withdrawMoney(body: HashMap<String, Any>){
        loading.value = true
        subscription = ApiServe().withdraw(body).subscribe({
            loading.value = false
            _withdrawResponse.value = it
        }, {
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    when(status){
                        NetworkErrorStatus.NOT_FOUND -> {
                            _errorMessage.value = "Please check your account number!"
                        }
                        else ->{
                            _errorMessage.value = data.toString()
                        }
                    }
                }

            }
        })
    }
}