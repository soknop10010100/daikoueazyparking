package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class LoginViewModel : BaseViewModel() {
    private val _loginDataResponse = MutableLiveData<BaseApiModel<JsonElement>>()
    private val _errorMessage = MutableLiveData<String>()

    val loginDataResponse : MutableLiveData<BaseApiModel<JsonElement>> get() = _loginDataResponse
    val errorMessage : MutableLiveData<String> get() = _errorMessage

    private var loginSubscription : Subscription? = null

    fun login(body : HashMap<String, Any>){
        loading.value = true
        loginSubscription = ApiServe().login(body).subscribe({
            loading.value = false
            _loginDataResponse.value = it
        }, {
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }
            }
        })
    }
}