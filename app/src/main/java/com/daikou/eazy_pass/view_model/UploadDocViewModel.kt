package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import retrofit2.http.Body
import rx.Subscription

class UploadDocViewModel : BaseViewModel() {
    private val _uploadResponse = MutableLiveData<BaseApiModel<JsonElement>>()
    private val _errorMessage = MutableLiveData<String>()
    private val _onLoading = MutableLiveData<Boolean>()

    var subscriptions  : Subscription? = null

    val uploadResponse : MutableLiveData<BaseApiModel<JsonElement>> get() =  _uploadResponse
    val errorMessage : MutableLiveData<String> get() =  _errorMessage
    val onLoading : MutableLiveData<Boolean> get() = _onLoading

    fun resubmitKyc(body: HashMap<String, Any>){
        _onLoading.value = true
        subscriptions = ApiServe().resubmitKyc(body).subscribe({
            _onLoading.value = false
            _uploadResponse.value = it
        }, {
            _onLoading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }

            }

        })
    }
}