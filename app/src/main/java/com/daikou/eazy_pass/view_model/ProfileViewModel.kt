package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import rx.Subscription

class ProfileViewModel : BaseViewModel(){
    private val _logoutSuccess = MutableLiveData<Boolean>()
    private val _errorMessage = MutableLiveData<String>()
    private val _userResponseData = MutableLiveData<BaseApiModel<User>>()

    val logoutSuccess : MutableLiveData<Boolean> get() = _logoutSuccess
    val userResponseData : MutableLiveData<BaseApiModel<User>> get() = _userResponseData
    val errorMessage : MutableLiveData<String> get() = _errorMessage

    private var logoutSubscription : Subscription? = null
    private var getUserSubscription : Subscription? = null

    fun logout(){
        loading.value = true
        logoutSubscription = ApiServe().logout().subscribe({
            loading.value = false
            _logoutSuccess.value = it.success
        },{
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }
            }
        })
    }

    fun getUserInfo(){
        loading.value = true
        logoutSubscription = ApiServe().getUserInfo().subscribe({
            loading.value = false
            _userResponseData.value = it
        },{
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }

            }
        })
    }

}