package com.daikou.eazy_pass.view_model

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.callback.ApiResponseCallback
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.apiConfiguration.Header
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.SignUpState
import com.daikou.eazy_pass.data.model.response_model.SignUpRespond
import com.daikou.eazy_pass.data.model.response_model.VehicleModel
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class SignUpViewModel() : BaseViewModel() {
    val user = MutableLiveData<JsonElement>()
    private val _signupResponseData = MutableLiveData<BaseApiModel<SignUpRespond>>()
    private val _onErrorData = MutableLiveData<String>()
    private val _signupUserValidate = MutableLiveData<SignUpState>()
    private val _vehicleRespond = MutableLiveData<BaseApiModel<JsonElement>>()

    val signUpRespondData : MutableLiveData<BaseApiModel<SignUpRespond>> get() =  _signupResponseData
    val onErrorData :  MutableLiveData<String> get() = _onErrorData
    val vehicleRespond : MutableLiveData<BaseApiModel<JsonElement>> get() = _vehicleRespond
    val signUpStateValidate : MutableLiveData<SignUpState> get() =  _signupUserValidate


    var signupUserSubscription : Subscription? = null

    //TODO handle when error to show in activity

    fun registerUser(body : HashMap<String, Any>){
        loading.value = true
        signupUserSubscription = ApiServe().registerUser(body).subscribe({
            loading.value = false
            _signupResponseData.value = it
        }, {object  : ApiCallbackWrapper(it){
            override fun onCallback(status: NetworkErrorStatus, data: Any) {
                loading.value = false
                _onErrorData.value = data.toString()
            }
        }
        })
    }

    fun fetchVehicleType(){
        signupUserSubscription = ApiServe().fetchVehicle().subscribe({
            _vehicleRespond.value = it
        }, {
            object  : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _onErrorData.value = data.toString()
                }
            }
        })
    }

    fun validateSignUpUserField(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        password: String,
        confirmPassword: String,
    ) {
        val signupUserState: SignUpState
        if (TextUtils.isEmpty(firstName)) {
            signupUserState = SignUpState(R.string.please_enter_your_first_name)
        } else if (TextUtils.isEmpty(lastName)) {
            signupUserState = SignUpState(R.string.please_enter_your_last_name)
        } else if (TextUtils.isEmpty(phoneNumber) ||
            phoneNumber.length < Constants.PhoneNumberConfig.PHONE_MIN_LENGTH ||
            phoneNumber.length > Constants.PhoneNumberConfig.PHONE_MAX_LENGTH ||
            !Helper.isPhoneValid(phoneNumber)
        ) {
            signupUserState = SignUpState(R.string.please_enter_a_valid_mobile_number)
        } else if (TextUtils.isEmpty(password.trim())) {
            signupUserState = SignUpState(R.string.please_enter_password)
        } else if (password.trim().length < 6) {
            signupUserState = SignUpState(R.string.pin_code_must_6_digit)
        } else if (TextUtils.isEmpty(confirmPassword.trim())) {
            signupUserState = SignUpState(R.string.please_enter_confirm_password)
        } else if (confirmPassword.trim().length < 6) {
            signupUserState = SignUpState(R.string.pin_code_must_6_digit)
        } else if (password.trim() != confirmPassword.trim()) {
            signupUserState = SignUpState(R.string.password_do_not_match)
        } else {
            signupUserState = SignUpState(hasDoneValidate = true)
        }
        _signupUserValidate.value = signupUserState
    }

    //    fun getUser(){
//        loading.value = true
//        userSubscription?.unsubscribe()
//        userSubscription = ApiServe().getUserTest().subscribe({
//            Log.d("yyyyyyyyyyyyyy", "hhhhhhhhhhhhhhhhhhhhhhhhh")
//            user.value = it
//            loading.value = false
//        }, {
//            print("Hellooooooooooooooooooooooooooooooo")
//        })
//    }
}