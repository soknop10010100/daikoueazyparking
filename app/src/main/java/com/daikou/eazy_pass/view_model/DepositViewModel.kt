package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.DepositResponse
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class DepositViewModel : BaseViewModel() {
    private val _depositResponse = MutableLiveData<BaseApiModel<DepositResponse>>()
    private val _errorMessage = MutableLiveData<String>()
    private val _loading = MutableLiveData<Boolean>()

    val depositResponseData : MutableLiveData<BaseApiModel<DepositResponse>> get() = _depositResponse
    val errorMessage : MutableLiveData<String> get() = _errorMessage
    val onLoading : MutableLiveData<Boolean> get() = _loading

    private var depositSubscription : Subscription? = null

    fun deposit(body : HashMap<String, Any>){
        _loading.value = true
        depositSubscription = ApiServe().deposit(body).subscribe({
            _loading.value = false
            _depositResponse.value = it
        }, {
            _loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }
            }
        })
    }


}