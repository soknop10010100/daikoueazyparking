package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.user.LongTermModel
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class LongTermViewModel : BaseViewModel() {
    private val _longTermLiveData = MutableLiveData<BaseApiModel<ArrayList<LongTermModel>>>()
    private val _renewLongTermLiveData = MutableLiveData<BaseApiModel<JsonElement>>()
    private val _onErrorLiveData = MutableLiveData<String> ()

    var subscription : Subscription? = null
    val longTermLiveData : MutableLiveData<BaseApiModel<ArrayList<LongTermModel>>> get() = _longTermLiveData
    val renewLongTermLiveData : MutableLiveData<BaseApiModel<JsonElement>> get() = _renewLongTermLiveData
    val onErrorLiveData : MutableLiveData<String> get() = _onErrorLiveData

    fun getLongTermRecord(queryMap: HashMap<String, Any>){
        loading.value = true
       subscription =  ApiServe().getLongTermRecord(queryMap).subscribe({
           loading.value = false
           _longTermLiveData.value = it
       }, {
           loading.value = false
           object : ApiCallbackWrapper(it){
               override fun onCallback(status: NetworkErrorStatus, data: Any) {
                   _onErrorLiveData.value = data.toString()
               }
           }
       })
    }

    fun renewLongTerm(projectId : Int){
        loading.value = true
        subscription =  ApiServe().renewLongTerm(projectId).subscribe({
            loading.value = false
            _renewLongTermLiveData.value = it
        }, {
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _onErrorLiveData.value = data.toString()
                }
            }
        })
    }

}