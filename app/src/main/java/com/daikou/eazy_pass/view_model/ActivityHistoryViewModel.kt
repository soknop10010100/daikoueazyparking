package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.ActivityHistory
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import rx.Subscription

class ActivityHistoryViewModel : BaseViewModel() {
    private val _onLoading = MutableLiveData<Boolean>()
    private val _activityHistoryResponse = MutableLiveData<BaseApiModel<ArrayList<ActivityHistory>>>()
    private  val _errorMessage = MutableLiveData<String>()

    val onLoading : MutableLiveData<Boolean> get() = _onLoading
    val activityHistoryResponse :  MutableLiveData<BaseApiModel<ArrayList<ActivityHistory>>> get() = _activityHistoryResponse
    val errorMessage : MutableLiveData<String> get() = _errorMessage

    private var subscriptions : Subscription? = null

    fun getActivities(queryMap : HashMap<String, Any>){
        _onLoading.value = true
        subscriptions = ApiServe().getActivities(queryMap).subscribe({
            _onLoading.value = false
            _activityHistoryResponse.value = it
        }, {
            _onLoading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }
            }
        })
    }

}