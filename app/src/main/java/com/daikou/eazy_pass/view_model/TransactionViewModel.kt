package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.transaction.TransactionResponse
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import rx.Subscription

class TransactionViewModel : BaseViewModel() {
    private val _loading = MutableLiveData<Boolean>()
    private val _transactionRespond = MutableLiveData<BaseApiModel<TransactionResponse>>()
    private val _errorMessage = MutableLiveData<String>()

    val onLoadingTrx :  MutableLiveData<Boolean> get() = _loading
    val transactionRespond : MutableLiveData<BaseApiModel<TransactionResponse>> get() = _transactionRespond
    val errorMessage : MutableLiveData<String> get() = _errorMessage
    var transactionSubscription : Subscription? = null

    fun fetchTransaction(bodyQuery : HashMap<String, Any>){
        _loading.value = true
        transactionSubscription = ApiServe().getTransaction(bodyQuery).subscribe({
            _loading.value = false
            _transactionRespond.value = it
        }, {
            _loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _errorMessage.value = data.toString()
                }

            }
        })
    }
}