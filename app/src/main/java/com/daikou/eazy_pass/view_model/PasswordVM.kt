package com.daikou.eazy_pass.view_model

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.Password
import com.daikou.eazy_pass.enumerable.ForgotChangeEnum
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class PasswordVM : BaseViewModel(){

    /**
     * change password
     * */

    private val _changePwdValidate = MutableLiveData<Password>()
    val changePwdValidate: LiveData<Password> get() = _changePwdValidate

    fun validateField(
        newPwd: String,
        oldPwd: String,
    ) {
        val pwdState: Password = if (TextUtils.isEmpty(newPwd) || newPwd.length < 6) {
            Password(
                newPwd = R.string.please_enter_new_password
            )
        } else if (TextUtils.isEmpty(oldPwd) || oldPwd.length < 6) {
            Password(confirmPwd = R.string.please_enter_confirm_password)
        } else if (newPwd.trim() != oldPwd.trim()) {
            Password(pwdNotMatch = R.string.password_do_not_match)
        } else {
            Password(hasDoneValidate = true)
        }
        _changePwdValidate.value = pwdState

    }

    private var _submitChangePasswordMutableLiveData: MutableLiveData<BaseApiModel<JsonElement>> =
        MutableLiveData<BaseApiModel<JsonElement>>()

    val submitChangePasswordMutableLiveData
        get() =
            _submitChangePasswordMutableLiveData

    private val _onErrorData = MutableLiveData<String>()
    val onErrorData :  MutableLiveData<String> get() = _onErrorData

    private var signupUserSubscription : Subscription? = null

    fun submitChangePwd(body : HashMap<String, Any>){
        loading.value = true
        signupUserSubscription = ApiServe().submitChangePwd(body).subscribe({
            loading.value = false
            if (it.success) {
                _submitChangePasswordMutableLiveData.value = it
            }
        }, {object  : ApiCallbackWrapper(it){
            override fun onCallback(status: NetworkErrorStatus, data: Any) {
                loading.value = false
                _onErrorData.value = data.toString()
            }
        }
        })
    }

    /**
     * request otp / verify otp / change forgot pass
     * */

    private var _requestOtpMutableLiveData: MutableLiveData<BaseApiModel<JsonElement>> =
        MutableLiveData<BaseApiModel<JsonElement>>()

    val requestOtpMutableLiveData: LiveData<BaseApiModel<JsonElement>> get() = _requestOtpMutableLiveData

    fun submitRequestOtp(verifyPinEnum: ForgotChangeEnum, body : HashMap<String, Any>){
        loading.value = true
        signupUserSubscription = ApiServe().submitRequestVerifyChangePasswordOtp(verifyPinEnum, body).subscribe({
            loading.value = false
            if (it.success) {
                _requestOtpMutableLiveData.value = it
            }
        }, {
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    _onErrorData.value = data.toString()
                }
            }
        })
    }

}