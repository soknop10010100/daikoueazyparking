package com.daikou.eazy_pass.view_model

import androidx.lifecycle.MutableLiveData
import com.daikou.eazy_pass.data.apiConfiguration.ApiServe
import com.daikou.eazy_pass.data.apiConfiguration.BaseApiModel
import com.daikou.eazy_pass.data.handler.ApiCallbackWrapper
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.enumerable.NetworkErrorStatus
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.google.gson.JsonElement
import rx.Subscription

class HomeViewModel : BaseViewModel() {

    private val _userResponseData = MutableLiveData<BaseApiModel<User>>()
    private val _parkingDeviceResponse = MutableLiveData<JsonElement>()
    private val _loadingFetchParkingDevice = MutableLiveData<Boolean>()

    val userResponseData : MutableLiveData<BaseApiModel<User>> get() = _userResponseData
    val parkingDeviceData : MutableLiveData<JsonElement> get() = _parkingDeviceResponse
    val loadingFetchParkingDevice : MutableLiveData<Boolean>  get() = _loadingFetchParkingDevice
    val errorMessage = MutableLiveData<String>()

    private var getUserSubscription : Subscription? = null
    private var fetchParkingDevice : Subscription? = null

    fun getUserInfo(){
        loading.value = true
        getUserSubscription = ApiServe().getUserInfo().subscribe({
            loading.value = false
            _userResponseData.value = it
        },{
            loading.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {

                }
            }
        })
    }

    fun fetchParkingDevice(){
        loadingFetchParkingDevice.value = true
        fetchParkingDevice = ApiServe().fetchParkingDevice().subscribe({
            loadingFetchParkingDevice.value = false
            _parkingDeviceResponse.value = it
        },{
            loadingFetchParkingDevice.value = false
            object : ApiCallbackWrapper(it){
                override fun onCallback(status: NetworkErrorStatus, data: Any) {
                    errorMessage.value = data.toString()
                }
            }
        })
    }
}