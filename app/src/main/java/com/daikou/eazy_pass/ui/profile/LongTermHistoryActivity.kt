package com.daikou.eazy_pass.ui.profile

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.user.LongTermModel
import com.daikou.eazy_pass.databinding.ActivityLongTermHistoryBinding
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.view_model.LongTermViewModel

class LongTermHistoryActivity : BaseActivity<LongTermViewModel>() {

    private var firstLoadData: Boolean = true
    private var pageNo = 1
    private lateinit var binding : ActivityLongTermHistoryBinding
    private var projectId : Int = -1
    private var longTermRecordList = ArrayList<LongTermModel>()
    private lateinit var longTermAdapter: LongTermAdapter

    companion object{
        const val PROJECT_ID = "project_id_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLongTermHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        initAction()
        doObserve()
    }

    private fun initView(){
        this.setSupportActionBar(binding.appBar.toolbar)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.appBar.title.text = getString(R.string.long_term_history)

        if (intent.hasExtra(PROJECT_ID)){
            projectId = intent.getIntExtra(PROJECT_ID, -1)
        }
        loadLongTermData()
    }

    private fun initAction(){

        binding.swipeRefresh.setOnRefreshListener {
            onRefreshItem()
        }

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView : RecyclerView, newState : Int) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            override fun onScrolled(recyclerView : RecyclerView, dx : Int, dy : Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager =  recyclerView.layoutManager as LinearLayoutManager
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == longTermRecordList.size - 1) {
                    //bottom of list!
                    getMoreLongTermData();
                }
            }
        });
    }

    private fun doObserve(){
        mViewModel.loading.observe(self()){
            binding.loadingView.root.visibility = if(it && firstLoadData) View.VISIBLE else  View.GONE
            binding.progressBar.visibility = if (it && !firstLoadData) View.VISIBLE else View.GONE
        }
        mViewModel.longTermLiveData.observe(self()){
            if (it.success){
                val longTermList = it.data
                updateRecyclerView(longTermList)
            }
        }
        mViewModel.onErrorLiveData.observe(self()){
            MessageUtil.showError(self(), "", it)
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    private fun updateRecyclerView(longTermList : ArrayList<LongTermModel>?){
        if (longTermList != null) {
            if(!firstLoadData) {
                longTermRecordList.addAll(longTermList)
                longTermAdapter.notifyDataSetChanged()
            }else{
                longTermAdapter = LongTermAdapter(longTermList)
                longTermRecordList = longTermList
                binding.recyclerView.apply {
                    layoutManager = LinearLayoutManager(self(), LinearLayoutManager.VERTICAL, false)
                    adapter = longTermAdapter
                }
                longTermAdapter.onSelect = {
                }
            }
        }

        binding.noDataFoundView.root.visibility = if (longTermRecordList.isEmpty()) View.VISIBLE else View.GONE
        firstLoadData = false
    }
    private fun loadLongTermData() {
//        binding.loadingView.root.visibility = View.VISIBLE
//        binding.trxRecyclerView.visibility = View.GONE
        val bodyQuery  = HashMap<String, Any>()
        bodyQuery["page"] = pageNo
        bodyQuery["limit"] = 10
        bodyQuery["project_id"] = projectId
        mViewModel.getLongTermRecord(bodyQuery)
    }

    private fun onRefreshItem() {
        firstLoadData = true
        pageNo = 1
        binding.swipeRefresh.isRefreshing = false
        loadLongTermData()
    }

    private fun getMoreLongTermData() {
        pageNo ++
        loadLongTermData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}