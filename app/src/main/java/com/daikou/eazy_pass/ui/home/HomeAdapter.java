package com.daikou.eazy_pass.ui.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daikou.eazy_pass.data.model.HomeRowLayoutModel;
import com.daikou.eazy_pass.data.model.HomeRowModel;
import com.daikou.eazy_pass.databinding.HomeScreenRowLayoutBinding;

import java.util.ArrayList;

class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>{

    private ArrayList<HomeRowLayoutModel> homeRowModels;
    private ClickRowCallback clickRowCallback;
    private LongClickCallback longClickCallback;

    public void setHomeRowModels(ArrayList<HomeRowLayoutModel> homeRowModels) {
        this.homeRowModels = homeRowModels;
    }
    public void setAction(ClickRowCallback clickRowCallback, LongClickCallback longClickCallback){
        this.clickRowCallback = clickRowCallback;
        this.longClickCallback = longClickCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        HomeScreenRowLayoutBinding homeScreenRowLayoutBinding = HomeScreenRowLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(homeScreenRowLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(homeRowModels.get(position));
        holder.homeRowBinding.getRoot().setOnClickListener(v -> {
            clickRowCallback.onClick(homeRowModels.get(position));
        });
        holder.homeRowBinding.getRoot().setOnLongClickListener(l -> {
            longClickCallback.onClick(homeRowModels.get(position));
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return homeRowModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private HomeScreenRowLayoutBinding homeRowBinding;
        public ViewHolder(@NonNull HomeScreenRowLayoutBinding homeRowBinding) {
            super(homeRowBinding.getRoot());
            this.homeRowBinding = homeRowBinding;
        }

        void bind(HomeRowLayoutModel homeRowModel){
            homeRowBinding.icon.setImageResource(homeRowModel.getIcon());
            homeRowBinding.titleTv.setText(homeRowModel.getLabel());
        }
    }
    interface ClickRowCallback{
        void onClick(HomeRowLayoutModel homeRowModel);
    }
    interface LongClickCallback{
        void onClick(HomeRowLayoutModel homeRowLayoutModel);
    }
}
