package com.daikou.eazy_pass.ui.bottom_sheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.callback.SelectDateListener
import com.daikou.eazy_pass.databinding.FragmentDateBottomSheetDialogBinding
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.util.Date

class DateBottomSheetDialogFragment : BottomSheetDialogFragment() {

    private lateinit var selectedDateListener : SelectDateListener
    private var currentDate : Date?  = null
    private var title : String? = null
    private lateinit var binding : FragmentDateBottomSheetDialogBinding
    private lateinit var selectDatePicker: SingleDateAndTimePicker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.TopRoundCornerBottomSheetDialogTheme)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDateBottomSheetDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAction()
    }

    private fun initView(){
        selectDatePicker = binding.selectDateTimePicker
        selectDatePicker.setDefaultDate(currentDate)
        selectDatePicker.setDisplayDays(false)
        selectDatePicker.setDisplayHours(false)
        selectDatePicker.setDisplayMinutes(false)
        selectDatePicker.setDisplayMonths(true)
        selectDatePicker.setDisplayYears(true)
        selectDatePicker.setDisplayDaysOfMonth(true)
        binding.titlePickerDate.text = "$title"
    }

    companion object {
        @JvmStatic
        fun newInstance(
            title: String = "Select Date",
            currentDate : Date, selectDateListener: SelectDateListener) =
            DateBottomSheetDialogFragment().apply {
                this.title = title
                this.currentDate = currentDate
                this.selectedDateListener = selectDateListener
            }
    }

    fun initAction(){
        binding.actionOk.setOnClickListener {
            selectedDateListener.selectDateListener(selectDatePicker.date)
            dismiss()
        }
    }
}