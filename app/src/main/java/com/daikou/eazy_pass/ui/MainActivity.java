package com.daikou.eazy_pass.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.daikou.eazy_pass.R;
import com.daikou.eazy_pass.databinding.ActivityMainBinding;
import com.daikou.eazy_pass.helper.Helper;
import com.daikou.eazy_pass.helper.PermissionHelper;
import com.daikou.eazy_pass.helper.PermissionRequest;
import com.daikou.eazy_pass.ui.home.HomeFragment;
import com.daikou.eazy_pass.ui.maps.MapsFragment;
import com.daikou.eazy_pass.ui.passed_activity.ActivityPassedFragment;
import com.daikou.eazy_pass.util.Constants;
import com.daikou.eazy_pass.util.base.BaseActivity;
import com.daikou.eazy_pass.util.base.BaseViewModel;
import com.daikou.eazy_pass.util.base.LoadingDialog;
import com.daikou.eazy_pass.util.broadcast_receiver.BluetoothBroadcastReceiver;
import com.daikou.eazy_pass.util.broadcast_receiver.NetworkChangeReceiver;
import com.daikou.eazy_pass.view_model.HomeViewModel;
import com.daikou.eazy_pass.view_model.PaymentViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;

import java.util.ArrayList;
import java.util.HashMap;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

@SuppressLint("MissingPermission")
public class MainActivity extends BaseActivity<BaseViewModel> {

    private ActivityMainBinding binding;
    private final int LOCATION_CODE = 111;

    private HomeFragment homeFragment;
    private MapsFragment mapsFragment;
    private ActivityPassedFragment activityPassedFragment;
    private FragmentManager fragmentManager;
    private Fragment active;
    private int selectedItem;

    private String parking_status = "";
    private ArrayList<String> deviceList = new ArrayList<>();

    private BluetoothBroadcastReceiver bluetoothBroadcastReceiver;
    public static PaymentViewModel paymentViewModel  = new PaymentViewModel();

    public static HomeViewModel homeViewModel = new HomeViewModel();
    public static boolean isHomeScreen = true;
    public static boolean alreadyPaid = false;
    public static boolean isCheckConnect = false;
    public static String notification = "notification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        bluetoothBroadcastReceiver = new BluetoothBroadcastReceiver(self());
        this.registerReceiver(bluetoothBroadcastReceiver, intentFilter);

        initView();
        initBottomBarAction();
    }

    private void initBottomBarAction(){
        binding.navView.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.navigation_home) {
                replaceFragment(homeFragment);
                return true;
            } else if (item.getItemId() == R.id.navigation_past_trips) {
                replaceFragment(activityPassedFragment);
                return true;
            }
            return false;
        });
    }

    private void initView() {
        PermissionRequest.AppMultiPermission(self());
        homeFragment = new HomeFragment();
        activityPassedFragment = new ActivityPassedFragment();
        fragmentManager = getSupportFragmentManager();
        active = homeFragment;


        fragmentManager.beginTransaction().add(R.id.nav_host_fragment_activity_main, activityPassedFragment, "2").hide(activityPassedFragment).commit();
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment_activity_main, homeFragment, "1").show(homeFragment).commit();

        if(getIntent().getAction() != null){
            if( getIntent().getAction().equals(notification)) {
                replaceFragment(activityPassedFragment);
                binding.navView.setSelectedItemId(R.id.navigation_past_trips);
            }
        }
        if(PermissionRequest.IsHasBluetoothPermission(self())){
            gotoEnableBluetooth();
        }
    }

    private void replaceFragment(Fragment fragment) {
        fragmentManager.beginTransaction().hide(active).show(fragment).commit();
        active = fragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bluetoothBroadcastReceiver);
    }

    void gotoEnableBluetooth(){
        BluetoothManager bluetoothManager = getSystemService(BluetoothManager.class);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if(!bluetoothAdapter.isEnabled()) {
            Intent intent1 = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(intent1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case (PermissionRequest.MULTI_PERMISSION) : {
                break;
            } case(PermissionRequest.LOCATION) : {

            }
        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}