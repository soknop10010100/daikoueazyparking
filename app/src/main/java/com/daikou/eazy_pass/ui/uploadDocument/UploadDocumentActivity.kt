package com.daikou.eazy_pass.ui.uploadDocument

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.core.util.forEach
import androidx.core.util.size
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.VehicleModel
import com.daikou.eazy_pass.ui.adapter.VehicleAdapter
import com.daikou.eazy_pass.databinding.ActivityUploadDocumentBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.activity.SignUpActivity
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseCameraActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.SignUpViewModel
import com.daikou.eazy_pass.view_model.UploadDocViewModel
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.internal.userAgent

class UploadDocumentActivity : BaseActivity<SignUpViewModel>() {
    private lateinit var binding : ActivityUploadDocumentBinding
    private var vehicleAdapter = VehicleAdapter()
    private var selectedVehicleType = ""
    private var bodyRequest = HashMap<String, Any>()
    private lateinit var uploadDocViewModel : UploadDocViewModel
    private var kycTemp  = HashMap<String, Any>()
    //private var kycDoc  = HashMap<String, Any>()
    companion object{
        const val isReUpload = "isReUpload"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUploadDocumentBinding.inflate(layoutInflater)
        setContentView(binding.root)
        uploadDocViewModel = UploadDocViewModel()
        initView()
        initViewData()
        initAction()
        doObserve()
    }

    private fun initView() {
        if(intent.hasExtra(SignUpActivity.KYC_TEMP_KEY)){
            kycTemp = intent.getSerializableExtra(SignUpActivity.KYC_TEMP_KEY) as HashMap<String, Any>
            if (kycTemp.isNotEmpty()){
                binding.profileImg.apply {
                    visibility = View.VISIBLE
                    Glide.with(this).load(kycTemp[Constants.KycDocKey.profile]).into(this)
                }
                binding.idCardImg.apply {
                    visibility = View.VISIBLE
                    Glide.with(this).load(kycTemp[Constants.KycDocKey.idCard]).into(this)
                }
                binding.vehicleImgUploaded.apply {
                    visibility = View.VISIBLE
                    Glide.with(this).load(kycTemp[Constants.KycDocKey.vehiclePicture]).into(this)
                }
                binding.plateNumberImgUploaded.apply {
                    visibility = View.VISIBLE
                    Glide.with(this).load(kycTemp[Constants.KycDocKey.plateNumberImg]).into(this)
                }
                binding.plateNumberEditText.setText(kycTemp[Constants.KycDocKey.plateNumberText].toString())
            }
        }
    }

    fun initViewData(){
        mViewModel.fetchVehicleType()
    }

    private fun initChooseVehicleUI(vehicleList : ArrayList<VehicleModel>){
        vehicleAdapter.setVehicleList(vehicleList)
        binding.vehicleSelectRecyclerView.layoutManager = GridLayoutManager(self(), 2, GridLayoutManager.VERTICAL, false)
        binding.vehicleSelectRecyclerView.adapter = vehicleAdapter
    }

    private fun initAction() {
        binding.actionBackImg.setOnClickListener {
            kycTemp[Constants.KycDocKey.plateNumberText] = binding.plateNumberEditText.text.toString()
            //Log.d("dddd", kycTemp[Constants.KycDocKey.plateNumberText].toString())
            val intent = Intent()
            intent.putExtra(SignUpActivity.KYC_TEMP_KEY, kycTemp)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        vehicleAdapter.onSelectedVehicle  = {
            selectedVehicleType = it
        }

        binding.actionDoneMtb.setOnClickListener {

            //Log.d("dddd", selectedVehicleType)

            val kycDoc = HashMap<String , Any>()
            when {
                binding.profileImg.visibility == View.GONE ->{
                    MessageUtil.showError(self(), "",getString(R.string.selfie_picture_is_required))
                }
                binding.idCardImg.visibility ==  View.GONE -> {
                    MessageUtil.showError(self(), "", getString(R.string.id_card_is_required))
                }
                selectedVehicleType.isEmpty()->{
                    MessageUtil.showError(self(), "", getString(R.string.please_select_vehicle_type))
                }
                binding.vehicleImgUploaded.visibility == View.GONE -> {
                    MessageUtil.showError(self(), "", getString(R.string.vehicle_picture_is_required))
                }
                binding.plateNumberImgUploaded.visibility == View.GONE -> {
                    MessageUtil.showError(self(), "", getString(R.string.plate_number_picture_is_required))
                }
                binding.plateNumberEditText.text.toString().isEmpty() ->{
                    MessageUtil.showError(self(), "", getString(R.string.please_enter_your_plate_number))
                }
                else -> {
                    val profileImageBitmap = (binding.profileImg.drawable as BitmapDrawable).bitmap
                    val idCardBitmap = (binding.idCardImg.drawable as BitmapDrawable).bitmap
                    val vehicleImageBitmap = (binding.vehicleImgUploaded.drawable as BitmapDrawable).bitmap
                    val plateNumberImageBitmap = (binding.plateNumberImgUploaded.drawable as BitmapDrawable).bitmap

                    kycDoc["id_card_img"] = String.format(
                        "data:image/jpeg;base64,%s",
                        Helper.convertToBase64(
                            idCardBitmap
                        )
                    )
                    kycDoc["profile_img"] = String.format(
                        "data:image/jpeg;base64,%s",
                        Helper.convertToBase64(
                            profileImageBitmap
                        )
                    )
                    kycDoc["plate_number_img"] = String.format(
                        "data:image/jpeg;base64,%s",
                        Helper.convertToBase64(
                            plateNumberImageBitmap
                        )
                    )
                    kycDoc["vehicle_img"] = String.format(
                        "data:image/jpeg;base64,%s",
                        Helper.convertToBase64(
                            vehicleImageBitmap
                        )
                    )

                    if(intent.hasExtra(SignUpActivity.SIGNUP_BODY_KEY)) {
                        bodyRequest =
                            intent.getSerializableExtra(SignUpActivity.SIGNUP_BODY_KEY) as HashMap<String, Any>
                        bodyRequest["kyc_documents"] = kycDoc
                        bodyRequest["vehicle_type"]  = selectedVehicleType
                        Log.d("dddd", selectedVehicleType)
                        bodyRequest["plate_number"]  = binding.plateNumberEditText.text.toString()
                        mViewModel.registerUser(bodyRequest)
                    }else if (intent.hasExtra(UploadDocumentActivity.isReUpload)){
                        bodyRequest["kyc_documents"] = kycDoc
                        bodyRequest["vehicle_type"]  = selectedVehicleType
                        bodyRequest["plate_number"]  = binding.plateNumberEditText.text.toString()
                        uploadDocViewModel.resubmitKyc(bodyRequest)
                    }
                }
            }
        }

        binding.actionSelfiePictureFrameLayout.setOnClickListener{
            val intent = Intent(this@UploadDocumentActivity, BaseCameraActivity::class.java)
            intent.putExtra(BaseCameraActivity.isProfileKey, true)
            gotoActivityForResult(this, intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
                override fun onActivityResult(result: ActivityResult) {
                    if (result.resultCode == Activity.RESULT_OK) {
                        val data = result.data
                        if (data?.hasExtra("image") == true){
                            val imagePath = data.getStringExtra("image")
//                            val imageUri = Uri.parse(imagePath)
//                            val imageBitmap = Helper.convertBitmap(self(), imageUri)
                            Glide.with(this@UploadDocumentActivity).load(imagePath).into(binding.profileImg)
                            kycTemp[Constants.KycDocKey.profile] = imagePath!!
                            binding.profileImg.visibility = View.VISIBLE
                            binding.addSelfiePictureImg.visibility = View.GONE
                            binding.selfiePictureImg.visibility = View.GONE
                        }
                    }
                }
            })
        }

        binding.actionIdCardFrameLayout.setOnClickListener {
            val intent = Intent(this@UploadDocumentActivity, BaseCameraActivity::class.java)
            intent.putExtra(BaseCameraActivity.isIdCardKey, true)
            gotoActivityForResult(this, intent, object : BetterActivityResult.OnActivityResult<ActivityResult>{
                override fun onActivityResult(result: ActivityResult) {
                    if (result.resultCode == Activity.RESULT_OK) {
                        val data = result.data
                        if (data?.hasExtra("image") == true){
                            val imagePath = data.getStringExtra("image")
//                            val imageUri = Uri.parse(imagePath)
//                            val imageBitmap = Helper.convertBitmap(self(), imageUri)
                            Glide.with(this@UploadDocumentActivity).load(imagePath).into(binding.idCardImg)
                            kycTemp[Constants.KycDocKey.idCard] = imagePath!!
                            binding.idCardImg.visibility = View.VISIBLE
                            binding.addIdCardImg.visibility = View.GONE
                            binding.idCardHolderImg.visibility = View.GONE
                        }
                    }
                }
            })
        }

        binding.actionSelectVehicleFrameLayout.setOnClickListener {
            val intent = Intent(this@UploadDocumentActivity, BaseCameraActivity::class.java)
            intent.putExtra(BaseCameraActivity.isVehicleKey, true)
            gotoActivityForResult(this, intent, object : BetterActivityResult.OnActivityResult<ActivityResult>{
                override fun onActivityResult(result: ActivityResult) {
                    if (result.resultCode == Activity.RESULT_OK) {
                        val data = result.data
                        if (data?.hasExtra("image") == true){
                            val imagePath = data.getStringExtra("image")
//                            val imageUri = Uri.parse(imagePath)
//                            val imageBitmap = Helper.convertBitmap(self(), imageUri)
                            Glide.with(this@UploadDocumentActivity).load(imagePath).into(binding.vehicleImgUploaded)
                            kycTemp[Constants.KycDocKey.vehiclePicture] = imagePath!!
                            binding.vehicleImgUploaded.visibility = View.VISIBLE
                            binding.vehiclePictureImg.visibility = View.GONE
                            binding.addSelectVehicleImg.visibility = View.GONE
                        }
                    }
                }
            })
        }

        binding.actionPlateNumberFrameLayout.setOnClickListener {
            val intent = Intent(this@UploadDocumentActivity, BaseCameraActivity::class.java)
            intent.putExtra(BaseCameraActivity.isPlateNumberKey, true)
            gotoActivityForResult(this, intent, object : BetterActivityResult.OnActivityResult<ActivityResult>{
                override fun onActivityResult(result: ActivityResult) {
                    if (result.resultCode == Activity.RESULT_OK) {
                        val data = result.data
                        if (data?.hasExtra("image") == true){
                            val imagePath = data.getStringExtra("image")
                            val imageUri = Uri.parse(imagePath)
                            val imageBitmap = Helper.convertBitmap(self(), imageUri)
                            Glide.with(this@UploadDocumentActivity).load(imagePath).into(binding.plateNumberImgUploaded)
                            kycTemp[Constants.KycDocKey.plateNumberImg] = imagePath!!
                            binding.plateNumberImgUploaded.visibility = View.VISIBLE
                            binding.plateNumberImg.visibility = View.GONE
                            binding.addPlateNumberImg.visibility = View.GONE
                            // set text to plate number editText
//                            binding.plateNumberEditText.setText( getPlateNumberFromImage(imageBitmap))
                        }
                    }
                }
            })
        }
    }
    private fun doObserve(){
        mViewModel.loading.observe(this){
            binding.loading.root.visibility = if (it) View.VISIBLE else View.GONE
        }
        uploadDocViewModel.onLoading.observe(this){
            binding.loading.root.visibility = if (it) View.VISIBLE else View.GONE
        }
        mViewModel.signUpRespondData.observe(this){
            if (it.success){
                val signupData = it.data
                val user = signupData?.user
                val accessToken = signupData?.accessToken
                if (accessToken != null) {
                    if (user != null) {
                        Helper.AuthHelper.saveUserSharePreference(self(), user)
                    }
                    Helper.AuthHelper.saveAccessToken(self(), accessToken)
                    Helper.AuthHelper.savePassword(self(), bodyRequest["password"].toString())
                    RedirectClass.gotoMainActivity(self())
                    finish()
                }else{
                    MessageUtil.showError(self(),"", "Token null")
                }
            }
        }
        uploadDocViewModel.uploadResponse.observe(this){
            if (it.success){
                RedirectClass.gotoMainActivity(self(), true)
            }else{
                val errorJson = it.errors
                errorJson?.let {errorObj ->
                    if (errorObj.has("plate_number")) {
                        val message = errorObj["pate_number"].asString
                        MessageUtil.showError(self(), "", message)
                    }
                }
            }
        }

        uploadDocViewModel.errorMessage.observe(this){
            if(Helper.StringResource.isHasInResource(it)){
                MessageUtil.showError(self(), "",Helper.StringResource.getResourceString(it))
            }else {
                MessageUtil.showError(self(), "", it.toString())
            }
        }

        mViewModel.vehicleRespond.observe(this){
            if (it.success){
                val vehicleList = ArrayList<VehicleModel>()
                val jsonElement = it.data
                jsonElement?.let { vehicleListJson->
                    val jsonArray = vehicleListJson.asJsonArray
                    jsonArray.forEach { vehicleJson->
                        val vehicleJsonObj = vehicleJson.asJsonObject
                        val typeToken = object : TypeToken<VehicleModel>(){}.type
                        val gson = Gson()
                        val vehicleModel = gson.fromJson<VehicleModel>(vehicleJsonObj, typeToken)
                        vehicleList.add(vehicleModel)
                    }
                }
                initChooseVehicleUI(vehicleList)
            }
        }
        mViewModel.onErrorData.observe(this){
            MessageUtil.showError(self(), "", it) { sweetAlertDialog ->
                sweetAlertDialog.dismiss()
                finish()
            }
        }
    }

    fun getPlateNumberFromImage( bitmap: Bitmap) : String{
        val textRecognizer = TextRecognizer.Builder(self()).build()
        val resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.width, 100, true)
        val frame = Frame.Builder().setBitmap(bitmap).build()
        var sparseTexts = textRecognizer.detect(frame) // get text
        val buffer = StringBuffer()
        sparseTexts.forEach { text, _ ->
            val textBlock = sparseTexts[text]
            val textStr = textBlock.value
            buffer.append(textStr)
        }
        return buffer.toString()
    }
}