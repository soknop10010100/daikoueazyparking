package com.daikou.eazy_pass.ui.home;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.daikou.eazy_pass.BuildConfig;
import com.daikou.eazy_pass.R;
import com.daikou.eazy_pass.data.model.HomeRowLayoutModel;
import com.daikou.eazy_pass.data.model.response_model.user.User;
import com.daikou.eazy_pass.data.model.response_model.payment.DoCheckoutRespond;
import com.daikou.eazy_pass.databinding.FragmentHomeBinding;
import com.daikou.eazy_pass.enumerable.HomeAction;
import com.daikou.eazy_pass.helper.Helper;
import com.daikou.eazy_pass.helper.MessageUtil;
import com.daikou.eazy_pass.helper.ParkingId;
import com.daikou.eazy_pass.ui.CaptureScanActivity;
import com.daikou.eazy_pass.ui.MainActivity;
import com.daikou.eazy_pass.ui.bluetooth_scan.DoScanBluetoothActivity;
import com.daikou.eazy_pass.ui.bottom_sheet.CallServiceBottomSheet;
import com.daikou.eazy_pass.ui.dailog.QrCodeDialog;
import com.daikou.eazy_pass.ui.wallet.MainWalletActivity;
import com.daikou.eazy_pass.ui.profile.ProfileActivity;
import com.daikou.eazy_pass.util.Constants;
import com.daikou.eazy_pass.util.base.BaseFragment;
import com.daikou.eazy_pass.util.redirect.RedirectClass;
import com.daikou.eazy_pass.view_model.HomeViewModel;
import com.daikou.eazy_pass.view_model.PaymentViewModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;
import com.daikou.eazy_pass.sdk.ParkingController;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

//@RequiresApi(api = Build.VERSION_CODES.S)
@RequiresApi(api = Build.VERSION_CODES.S)
public class HomeFragment extends BaseFragment<HomeViewModel> {
    private String mServiceUuid = "";
    private String mCharUuid = "";
    private boolean isSuccessSever;
    private RecyclerView homeRecyclerview;
    private HomeAdapter homeAdapter;
    private FragmentActivity fContext;
    AlertDialog scanDialog;
    public static SweetAlertDialog sweetAlertDialog;
    public static boolean isAlreadyShow = false ;
    public static boolean dialogShowing = false;
    public static BluetoothDevice mBluetoothDevice = null;
    private final Handler sliderHandler = new Handler();
    private String barrierId = "";
    private FragmentHomeBinding binding;
    public static ParkingController parkingController;
    private PaymentViewModel paymentViewModel;
    private View dialogView;
    public static boolean isScanUserId = false;
    private ScanOptions options;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fContext = (FragmentActivity) context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        paymentViewModel = new PaymentViewModel();
    }
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        homeAdapter = new HomeAdapter();
        homeRecyclerview = binding.homeScreenRecyclerView;
        parkingController = new ParkingController();

        ParkingId.PARKING_ID = Helper.ParkingInfo.INSTANCE.getParkingDevice(getContext());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        dialogView = getLayoutInflater().inflate(R.layout.bluetooth_scan_alert_dialog, null);
        builder.setView(dialogView);
        builder.setCancelable(false);
        scanDialog = builder.create();
        setupHomeRow();
        homeAction();
        initAction();
        fetchData();
        doObserve();
    }

    private void fetchData() {
        mViewModel.fetchParkingDevice();
        mViewModel.getUserInfo();
    }

    private void setupHomeRow() {
        ArrayList<HomeRowLayoutModel> homeRowList = new ArrayList<>();
        homeRowList.add(new HomeRowLayoutModel(getString(R.string.scan_qr), R.drawable.scan_icon, HomeAction.ScanQR));
        homeRowList.add(new HomeRowLayoutModel(getString(R.string.scan_nearby), R.drawable.bluetooth_icon, HomeAction.Pay));
        homeRowList.add(new HomeRowLayoutModel(getString(R.string.wallet), R.drawable.wallet_icon, HomeAction.Wallet));
        homeRowList.add(new HomeRowLayoutModel(getString(R.string.profile), R.drawable.profile_icon, HomeAction.Profile));

        homeAdapter.setHomeRowModels(homeRowList);
        homeRecyclerview.setAdapter(homeAdapter);
    }

    private void homeAction() {
        @SuppressLint("MissingPermission") HomeAdapter.ClickRowCallback clickRowCallback = homeRowModel -> {
            if (homeRowModel.getAction() == HomeAction.Wallet) {

                Intent intent = new Intent(getActivity(), MainWalletActivity.class);
                startActivity(intent);

            } else if (homeRowModel.getAction() == HomeAction.Profile) {

                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);

            } else if (homeRowModel.getAction() == HomeAction.ScanQR) {
                if(isBluetoothEnable()){
                    if(checkForScan()){
                        doScanQr(0);
                    }
                }else {
                    MessageUtil.INSTANCE.showError(
                            fContext,
                            "",
                            fContext.getString(R.string.bluetooth_opening_suguestion), sweetAlertDialog -> {
                                sweetAlertDialog.dismiss();
                        gotoEnableBluetooth();
                    });
                }
            } else if (homeRowModel.getAction() == HomeAction.Pay) {
                if(isBluetoothEnable()){
                    if(checkForScan()){
                        doScanBluetooth();
                    }
                }else {
                    MessageUtil.INSTANCE.showError(
                            fContext,
                            "",
                            fContext.getString(R.string.bluetooth_opening_suguestion), sweetAlertDialog -> {
                                sweetAlertDialog.dismiss();
                                gotoEnableBluetooth();
                            });
                }
            }
        };

        HomeAdapter.LongClickCallback longClickCallback = new HomeAdapter.LongClickCallback() {
            @Override
            public void onClick(HomeRowLayoutModel homeRowLayoutModel) {
                if (homeRowLayoutModel.getAction() == HomeAction.ScanQR) {
                    if (checkForScan()) {
                        doScanQr(1);
                    }
                }
            }
        };
        homeAdapter.setAction(clickRowCallback, longClickCallback);
    }
    private void initAction(){
        binding.errorQrView.rescanBtn.setOnClickListener(v ->{
            showErrorQrCode(false);
            doScanQr(0);
        });
        binding.errorQrView.actionBackImg.setOnClickListener(v -> {
            showErrorQrCode(false);
        });

        binding.serviceBtn.setOnClickListener(v -> {
            String phoneNumber = Helper.AuthHelper.INSTANCE.getUserSharePreference(fContext).getCustomerServicePhone();
            if(phoneNumber != null) {
                CallServiceBottomSheet.newInstance(phoneNumber).show(getParentFragmentManager(), "CallServiceBottomSheet");
            }
        });

        binding.qrCode.setOnClickListener(v -> {
            Date date1 = new Date();
            long timestampSeconds = (long) (date1.getTime() / 1000.0);
            Log.d("qqqqqqqq", timestampSeconds + "");
            String userId =  Helper.AuthHelper.INSTANCE.getUserId(fContext);
            String userPhone = Helper.AuthHelper.INSTANCE.getUserSharePreference(fContext).getPhone();
            String secretKey = Constants.QRCodeSec.secretKey;
            String plaintText;
            if(BuildConfig.IS_DEBUG){
                plaintText = Constants.QRCodeSec.headerPlaintDev + userId + "_" + userPhone + "_" +timestampSeconds;
            }else {
                plaintText = Constants.QRCodeSec.headerPlaintProd + userId + "_" + userPhone + "_" +timestampSeconds;
            }

            try {
                String qrcodeString = Helper.INSTANCE.encrypt(plaintText, secretKey);
                Log.d("qqqqqqqq", qrcodeString);
                Log.d("qqqqqqqq", Helper.INSTANCE.decrypt(qrcodeString, secretKey));

                new QrCodeDialog().showDialog(fContext, qrcodeString);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        });
    }

    private void doScanBluetooth(){
        gotoDoScanBluetoothActivity(fContext, result -> {
            if(result.getResultCode() == Activity.RESULT_OK){
                Intent intent = result.getData();
                if(intent != null) {
                    if (intent.hasExtra(DoScanBluetoothActivity.errorFromServerKey)) {
                        String error = intent.getStringExtra(DoScanBluetoothActivity.errorFromServerKey);
                        if(error.contains("topup")){
                            String[] arrStr = error.split("\\$");
                            String message = String.format("%s $%s", getString(R.string.insuffient_fund_please_topup_more), arrStr[1]);
                            MessageUtil.INSTANCE.showError(fContext, "", message, getString(R.string.deposit), sweetAlertDialog -> {
                                sweetAlertDialog.dismiss();
                                RedirectClass.INSTANCE.gotoDepositActivity(fContext);
                            });
                        }else {
                            MessageUtil.INSTANCE.showError(fContext, "", error);
                        }
                    }else if(intent.hasExtra(DoScanBluetoothActivity.errorConnectingKey)){
                        String error = intent.getStringExtra(DoScanBluetoothActivity.errorConnectingKey);
                        MessageUtil.INSTANCE.showError(fContext, "", error);
                    }
                }
            }
        });
    }

    public void doScanQr(int specific_camera_id) {
        options = new ScanOptions();
        options.setPrompt("Scan QR Code");
        options.setCameraId(specific_camera_id);  // Use a specific camera of the device
        options.setBeepEnabled(true);
        options.setOrientationLocked(true);
        options.setCaptureActivity(CaptureScanActivity.class);
        barcodeLauncher.launch(options);
    }
    private final ActivityResultLauncher<ScanOptions> barcodeLauncher = registerForActivityResult(new ScanContract(),
            result -> {
                Log.d("onActivityResult", ": "+ result.getContents());
                if (result.getContents() == null) {

                } else if (result.getContents().contains("@DZA")) {
                    //checkForPayment(result.getContents());
                    barrierId = result.getContents();
                    connectOpenGate();
                }  else if (result.getContents().contains("KQtqHi0yAbS1")) { // user qr code scanning
                    try {
                        String decryptString = Helper.INSTANCE.decrypt(result.getContents(), Constants.QRCodeSec.secretKey);

                        if(decryptString != null) {
                            if (decryptString.contains(Constants.QRCodeSec.headerPlaintDev) || decryptString.contains(Constants.QRCodeSec.headerPlaintProd)) {
                                try {
                                    Log.d("qqqqqqqq", Helper.INSTANCE.decrypt(result.getContents(), Constants.QRCodeSec.secretKey));
                                    QrUserScanConnectGate(decryptString);
                                } catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                } else{ // qr code not in types
                    showErrorQrCode(true);
                }
            });


    @RequiresApi(api = Build.VERSION_CODES.S)
    private void checkForPayment(String barrier){
        HashMap<String, Object> body = new HashMap<>();
        //barrierId = barrier;
        body.put("barrier_id",barrier);
        if(hasInternetConnection()){
            paymentViewModel.openGateDoPayment(body);
        }else{// offline
            isSuccessSever = true;
            storeLocalPayment();
           // openGate();
        }
    }

    private void storeLocalPayment(){
        String userParkingStatus = Helper.ParkingInfo.INSTANCE.getParkingStatus(fContext);
        String content = "";
        if(userParkingStatus.equals(Constants.exit)){
            content = "You entered successfully. Thank you for using EZ Pass!";
            Helper.ParkingInfo.INSTANCE.saveParkingStatus(fContext, Constants.entrance); // save status
            Date date = new Date();
            Helper.ParkingInfo.INSTANCE.storeDataNotPay(fContext, barrierId, Constants.entrance, date); // save data when entrance
        }else {
            content = "We will debit from you wallet later. Thank you for using EZ Pass!";
            Helper.ParkingInfo.INSTANCE.saveParkingStatus(fContext, Constants.exit); // save status
            Date date = new Date();
            Helper.ParkingInfo.INSTANCE.storeDataNotPay(fContext, barrierId, Constants.exit, date); // save data when exit
        }
        alertNotification(getString(R.string.no_internet_connection),content);
    }

    private void showErrorQrCode(boolean isShow){
        binding.errorQrView.getRoot().setVisibility(isShow ? View.VISIBLE : View.GONE);
        binding.errorQrView.errorMsgTv.setText(getString(R.string.invalid_chat_qr_code));
    }
    boolean isRequested = false ;
    boolean isDone = false;

    @RequiresApi(api = Build.VERSION_CODES.S)
    @SuppressLint("MissingPermission")
    private void doObserve() {
        mViewModel.getLoadingFetchParkingDevice().observe(this, isLoading ->{
            binding.loadingView.getRoot().setVisibility(isLoading ? View.VISIBLE : View.GONE);
        });
        mViewModel.getLoading().observe(this, isLoading -> {
            binding.loadingView.getRoot().setVisibility(isLoading ? View.VISIBLE : View.GONE);
        });
        mViewModel.getUserResponseData().observe(this, userBaseApiModel -> {
            if (userBaseApiModel.getSuccess()) {
                if (userBaseApiModel.getData() != null) {
                    User user = userBaseApiModel.getData();
                    Helper.AuthHelper.INSTANCE.saveUserSharePreference(fContext, user);
                    checkUserBalance(user);
                }
            }
        });
        mViewModel.getParkingDeviceData().observe(this, parkingDeviceResponse -> {
            JsonObject jsonObjectResponse = parkingDeviceResponse.getAsJsonObject();
            JsonObject jsonObject = jsonObjectResponse.getAsJsonObject("data");
            if (jsonObject.has("user")) {
                JsonObject jsonUser = jsonObject.getAsJsonObject("user");
                String userParkingStatus = jsonUser.get("parking_status").getAsString();
                Helper.ParkingInfo.INSTANCE.saveParkingStatus(fContext, userParkingStatus);
            }
            if (jsonObject.has("entrance_parking_devices")) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("entrance_parking_devices");
                for (int i = 0 ; i<jsonArray.size() ; i++){
                    String deviceName = jsonArray.get(i).getAsString();
                    ParkingId.PARKING_ID.put(deviceName,"exit");
                }
            }
            if (jsonObject.has("exit_parking_devices")) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("exit_parking_devices");
                for (int i = 0 ; i<jsonArray.size() ; i++){
                    String deviceName = jsonArray.get(i).getAsString();
                    ParkingId.PARKING_ID.put(deviceName,"entrance");
                }
            }
            Helper.ParkingInfo.INSTANCE.saveParkingDevices(fContext, ParkingId.PARKING_ID);
        });
        mViewModel.getErrorMessage().observe(this, error->{
            MessageUtil.INSTANCE.showError(fContext, "", error);
        });
        paymentViewModel.getLoading().observe(this, isLoading ->{
            binding.loadingView.getRoot().setVisibility(isLoading ? View.VISIBLE : View.GONE);
        });
        paymentViewModel.getDataPaymentResponse().observe(this, doCheckoutRespond -> {
            if (doCheckoutRespond.getSuccess()) {
                if (mServiceUuid.isEmpty()) {
                    isSuccessSever = true;
                } else {
                    isSuccessSever = false;
                    parkingController.openLastParking(mServiceUuid, mCharUuid);
                    mServiceUuid = "";
                    mCharUuid = "";
                    barrierId = ""; // clear barrierId
                    parkingController.disconnectFromDevice();
                }
//                if(!barrierId.isEmpty()) { // in case scan qr code
//                    openGate();
//                }
                DoCheckoutRespond doCheckout = doCheckoutRespond.getData();
                if (!isScanUserId){
                    if (doCheckout != null){
                        if(doCheckout.getTotal() != null){
                            String paidString = "was debited from your wallet. Thank you for using EZ Pass!";
                            String contentText = String.format("$%.2f USD %s",doCheckout.getTotal() , paidString );
                            alertNotification(getString(R.string.app_name), contentText);
                        }else {
                            String thank = "Thank you for using EZ Pass!";
                            String contentText = String.format("%s %s", doCheckout.getMessage(), thank);
                            alertNotification(getString(R.string.app_name), contentText);
                        }
                    }
                    String parkingStatus = Helper.ParkingInfo.INSTANCE.getParkingStatus(fContext);
                    if (parkingStatus.equals(Constants.exit)) {
                        Helper.ParkingInfo.INSTANCE.saveParkingStatus(fContext, Constants.entrance);
                    } else if (parkingStatus.equals( Constants.entrance)) {
                        Helper.ParkingInfo.INSTANCE.saveParkingStatus(fContext, Constants.exit);
                    }
                    if(scanDialog != null) {
                        if (scanDialog.isShowing()) {
                            scanDialog.dismiss();
                        }
                    }
                    Helper.ParkingInfo.INSTANCE.clearUserNotPay(fContext);
                } else {
                    // go to scan again
                    barcodeLauncher.launch(options);
                    isScanUserId = false;
                }
            }

        });
        paymentViewModel.getErrorMessage().observe(fContext, errorMessage -> {
            mServiceUuid = "";
            mCharUuid = "";
            barrierId = ""; // clear barrierId
            parkingController.disconnectFromDevice();
            parkingController.cancelScanning();
            if(mBluetoothDevice != null) {
                parkingController.disconnectFromDevice();
                mBluetoothDevice = null; // clear bluetooth
                isRequested = !isRequested;
                isDone = false;
            }

            if(!barrierId.isEmpty()){
                barrierId = ""; // clear barrierId
            }
            if(errorMessage.contains("topup")){
                String[] arrStr = errorMessage.split("\\$");
                String message = String.format("%s $%s", getString(R.string.insuffient_fund_please_topup_more), arrStr[1]);
                MessageUtil.INSTANCE.showError(fContext, "", message, getString(R.string.deposit), sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                    RedirectClass.INSTANCE.gotoDepositActivity(fContext);
                });
            }else {
                if(isScanUserId) {
                    MessageUtil.INSTANCE.showError(fContext, "", errorMessage, sweetAlertDialog -> {
                        sweetAlertDialog.dismiss();
                        // go to scan again
                        barcodeLauncher.launch(options);
                        isScanUserId = false;
                    });
                }else{
                    MessageUtil.INSTANCE.showError(fContext, "", errorMessage);
                }
            }

            if(scanDialog != null) {
                if (scanDialog.isShowing()) {
                    scanDialog.dismiss();
                }
            }
        });
        // handle loading for local pay
        MainActivity.paymentViewModel.getLoading().observe(fContext, isLoading -> {
            binding.loadingView.getRoot().setVisibility(isLoading? View.VISIBLE : View.GONE);
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    private void connectOpenGate(){
        binding.loadingView.getRoot().setVisibility(View.VISIBLE);
        parkingController.connectToParking(fContext, barrierId, new ParkingController.ParkingCallBack() {
            @Override
            public void onParkingOpen(@Nullable String s, @Nullable String s1,@Nullable String serviceUuid, @Nullable String charUuid,BluetoothDevice bluetoothDevice) {
                binding.loadingView.getRoot().setVisibility(View.GONE);
                checkForPayment(barrierId);
                mServiceUuid = serviceUuid;
                mCharUuid = charUuid;
                if (isSuccessSever) {
                    parkingController.openLastParking(serviceUuid, charUuid);
                    // store data
//                    if(!hasInternetConnection()){
//                        storeLocalPayment();
//                    }
                    mServiceUuid = "";
                    mCharUuid = "";
                    barrierId = ""; // clear barrierId
                }
            }
            @Override
            public void onConnect(@NonNull BluetoothDevice bluetoothDevice) {
                //parkingController.openParking(bluetoothDevice);
                parkingController.cancelScanning();
            }
            @Override
            public void onError(@Nullable String s) {
                binding.loadingView.getRoot().setVisibility(View.GONE);
                MessageUtil.INSTANCE.showError(fContext, "", s);
            }
        });

    }


    private int notificationId = 1;
    private void alertNotification(String title,String contentText){
        String CHANNEL_ID = "eazy_pass_notification";
        String description = "checkout_notification";
        Intent intent = new Intent(fContext, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setAction(MainActivity.notification);
        PendingIntent pendingIntent = PendingIntent.getActivity(fContext, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        NotificationManager notificationManager = (NotificationManager) fContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;
        int importance = NotificationManager.IMPORTANCE_HIGH;

        Uri sound = Uri. parse (ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + fContext.getPackageName() + "/raw/notification_sound.mp3" ) ;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, description, importance );
//                notificationChannel.enableLights(true);
//                notificationChannel.setLightColor( Color.RED);
                notificationChannel.enableVibration(true);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            notificationChannel.setSound(sound , audioAttributes) ;
            notificationManager.createNotificationChannel(notificationChannel);

            builder = new NotificationCompat.Builder(fContext, CHANNEL_ID);

        } else {

            builder = new NotificationCompat.Builder(fContext);
            //builder.setSound(sound);

        }
        builder.setContentTitle(title);
        builder.setSmallIcon(R.drawable.ez_app_logo);
        builder.setContentText(contentText);
        builder.setSound(sound, AudioManager.STREAM_NOTIFICATION);
        builder.setContentIntent(pendingIntent);
        builder.setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle("EZ Parking").bigText(contentText));
        notificationManager.notify(notificationId, builder.build());
        notificationId += notificationId;
    }

    private boolean checkForScan(){
        HashMap<String, HashMap<String, Object>> dataNotPay = Helper.ParkingInfo.INSTANCE.getDataNotPay(fContext);
        if(!hasInternetConnection()){
            User savedUser = Helper.AuthHelper.INSTANCE.getUserSharePreference(fContext);
            double balance = savedUser.getTotalBalance() != null ? savedUser.getTotalBalance() : 0.00;
            if(Helper.AuthHelper.INSTANCE.isHasBalance(fContext) && !Helper.AuthHelper.INSTANCE.isKycRefused(fContext) && !dataNotPay.containsKey(Constants.exit) && !(balance<3)){
                return true;
            } else if (Helper.AuthHelper.INSTANCE.isKycRefused(fContext)){
                MessageUtil.INSTANCE.showConfirm(fContext, getString(R.string.kyc_is_refused), getString(R.string.please_resubmit_your_kyc_again), getString(R.string.resubmit), sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                    RedirectClass.INSTANCE.gotoUploadDocument(fContext, true);
                });
                return false;
            } else if (dataNotPay.containsKey(Constants.exit)) {
                MessageUtil.INSTANCE.showError(fContext, getString(R.string.not_allow), getString(R.string.you_have_to_pay_the_previous_parking));
                return false;
            } else{
                MessageUtil.INSTANCE.showConfirm(fContext, getString(R.string.wallet_warning), getString(R.string.your_must_have_at_least_in_your_wallet), getString(R.string.deposit), sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                    RedirectClass.INSTANCE.gotoDepositActivity(fContext);
                });
                return false;
            }
        }else{
            if (Helper.AuthHelper.INSTANCE.isKycRefused(fContext)){
                MessageUtil.INSTANCE.showConfirm(fContext, getString(R.string.kyc_is_refused), getString(R.string.please_resubmit_your_kyc_again), getString(R.string.resubmit), sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                    RedirectClass.INSTANCE.gotoUploadDocument(fContext, true);
                });
                return false;
            }
        }
        return true;
    }


    private void checkUserBalance(User user){
        if(user.getTotalBalance() != null && user.getTotalBalance() < 3 && !isAlreadyShow){
            if(!dialogShowing){
                dialogShowing = true;
                MessageUtil.INSTANCE.showWarning(getActivity(), getString(R.string.wallet_warning), getString(R.string.your_must_have_at_least_in_your_wallet), getString(R.string.deposit), sweetAlertDialog -> {
                    dialogShowing = false;
                    sweetAlertDialog.dismiss();
                    RedirectClass.INSTANCE.gotoDepositActivity(fContext);
                });
            }
            isAlreadyShow = true;
//            else if(!sweetAlertDialog.isShowing()) {
//                sweetAlertDialog.show();
//            }
        }
    }

    private void qrBluetoothScan(@Nullable boolean isUpdate){
        scanDialog.show();
        LottieAnimationView lottieAnimationView = dialogView.findViewById(R.id.animation_view);
        TextView textView = dialogView.findViewById(R.id.text_description);
        Button btn = dialogView.findViewById(R.id.cancelScanningBtn);
        btn.setVisibility(View.GONE);
        if(isUpdate){
            textView.setText(R.string.description_bluetooth_done_scan);
            lottieAnimationView.setAnimation(R.raw.success_animation);
        }
        lottieAnimationView.playAnimation();
    }

    //Qr user scanning
    private void QrUserScanConnectGate (String decryptString){
        Log.d("ddddddd", "QrUserScanConnectGate: ");
        Log.d("ddddddd", "QrUserScanConnectGate: " + mServiceUuid);

        isScanUserId = true;
        binding.loadingView.getRoot().setVisibility(View.VISIBLE);
        parkingController.connectToParking(fContext, new ParkingController.ParkingCallBack() {
            @SuppressLint("MissingPermission")

            @Override
            public void onParkingOpen(@Nullable String s, @Nullable String s1,@Nullable String serviceUuid, @Nullable String charUuid,BluetoothDevice bluetoothDevice) {
                binding.loadingView.getRoot().setVisibility(View.GONE);

                doPaymentForScanUserQr(decryptString, bluetoothDevice.getName());
                mServiceUuid = serviceUuid;
                mCharUuid = charUuid;
                if (isSuccessSever) {
                    parkingController.openLastParking(serviceUuid, charUuid);
                    mServiceUuid = "";
                    mCharUuid = "";
                    barrierId = ""; // clear barrierId
                }
            }
            @Override
            public void onConnect(@NonNull BluetoothDevice bluetoothDevice) {
                //parkingController.openParking(bluetoothDevice);
                Log.d("ddddddd", "onConnect: ");
                parkingController.cancelScanning();
            }
            @Override
            public void onError(@Nullable String s) {
                MessageUtil.INSTANCE.showError(fContext, "", s , sweetAlertDialog1 ->{
                    sweetAlertDialog1.dismiss();
                    isScanUserId = false;
                    // go to scan again
                    barcodeLauncher.launch(options);
                });
                parkingController.disconnectFromDevice();
                binding.loadingView.getRoot().setVisibility(View.GONE);
            }
        });
    }
    private void doPaymentForScanUserQr(String decryptString , String barrierId){

        String finalString;
        if(BuildConfig.IS_DEBUG){
            finalString = decryptString.replace(Constants.QRCodeSec.headerPlaintDev, "");
        }else {
            finalString = decryptString.replace(Constants.QRCodeSec.headerPlaintProd, "");
        }
        String delimiter = "_";
        String[] splitStrings = finalString.split(delimiter);
        String userId = splitStrings[0];
        String userPhone = splitStrings[1];
        String time = splitStrings[2];

        HashMap<String, Object> body = new HashMap<>();
        body.put("barrier_id", barrierId);
        body.put("time", time);
        body.put("user_id", userId);
        body.put("user_phone", userPhone);

        paymentViewModel.openGateDoPayment(body);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        //mViewModel.getUserInfo();
        if(!hasInternetConnection()){
            if(Helper.AuthHelper.INSTANCE.hasUserInSharePreference(fContext)){
                User user = Helper.AuthHelper.INSTANCE.getUserSharePreference(fContext);
                checkUserBalance(user);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        isAlreadyShow = false;
    }
}