package com.daikou.eazy_pass.ui.passed_activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.daikou.eazy_pass.data.model.PassedActivityModel;
import com.daikou.eazy_pass.data.model.response_model.ActivityHistory;
import com.daikou.eazy_pass.helper.Helper;
import com.daikou.eazy_pass.ui.MainActivity;
import com.daikou.eazy_pass.R;
import com.daikou.eazy_pass.ui.adapter.PassedActivityAdapter;
import com.daikou.eazy_pass.data.model.CustomPassedActivity;
import com.daikou.eazy_pass.databinding.FragmentActivityPassedBinding;
import com.daikou.eazy_pass.helper.UpdatePassActivity;
import com.daikou.eazy_pass.util.base.BaseFragment;
import com.daikou.eazy_pass.view_model.ActivityHistoryViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

public class ActivityPassedFragment extends BaseFragment<ActivityHistoryViewModel> {

    private FragmentActivityPassedBinding binding;

    private FragmentActivity fContext ;
    private ArrayList<ActivityHistory> activityHistoryList;

    private PassedActivityAdapter passedActivityAdapter ;

    private int pageNo = 1;

    private boolean firstFetchActivity = true;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fContext = (FragmentActivity) context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentActivityPassedBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
//        ArrayList<PassedActivityModel> listActivity = Helper.PassedActivityHelper.INSTANCE.getSavePassedActivity(getContext());
//        passedActivityList = updatePassActivity.getUpdatedPassedActivityList(listActivity);
//        Collections.reverse(passedActivityList);
//        passedActivityAdapter = new PassedActivityAdapter(passedActivityList);
//        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        binding.recyclerView.setAdapter(passedActivityAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initAction();
        doObserve();
    }


    private void initView() {
        ((MainActivity) requireActivity()).setSupportActionBar(binding.appBar.toolbar);
        Objects.requireNonNull(((MainActivity) requireActivity()).getSupportActionBar()).setDisplayShowTitleEnabled(false);
        binding.appBar.title.setText(R.string.activity_text);

        fetchActivities(1);
    }

    private void fetchActivities(int page){
        initShowUnpaid();
        HashMap<String, Object> queryMap = new HashMap<>();
        queryMap.put("page", page);
        queryMap.put("limit", 10);

        mViewModel.getActivities(queryMap);
    }

    public void initShowUnpaid(){
        binding.unpaidLayout.getRoot().setVisibility(hasInternetConnection()? View.GONE : View.VISIBLE);
        int localStore = Helper.ParkingInfo.INSTANCE.getDataNotPay(fContext).size();
        binding.unpaidLayout.textTv.setText(String.format("%s [%d]", getString(R.string.unrequest_barrier), localStore));
    }

    @SuppressLint("NotifyDataSetChanged")
    private void doObserve(){
        mViewModel.getOnLoading().observe(fContext, isLoading -> {
            binding.loadingView.getRoot().setVisibility((isLoading && firstFetchActivity)? View.VISIBLE : View.GONE);
            binding.progressBar.setVisibility((isLoading && !firstFetchActivity)? View.VISIBLE : View.GONE);
        });
        mViewModel.getActivityHistoryResponse().observe(fContext, baseResponse -> {
            if(baseResponse.getSuccess()){
                if(baseResponse.getData() != null) {
                    initUi(baseResponse.getData());
                }
            }
        });

        mViewModel.getErrorMessage().observe(fContext, error ->{
            binding.noDataFoundView.messageView.setText(error);
            binding.noDataFoundView.getRoot().setVisibility(View.VISIBLE);
            binding.recyclerView.setVisibility(View.GONE);
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void initUi(ArrayList<ActivityHistory> activities) {
        if(firstFetchActivity){
            activityHistoryList = activities;
            binding.recyclerView.setVisibility(activityHistoryList.isEmpty() ? View.GONE : View.VISIBLE);
            binding.noDataFoundView.getRoot().setVisibility(activityHistoryList.isEmpty() ? View.VISIBLE : View.GONE);
            passedActivityAdapter = new PassedActivityAdapter(activityHistoryList);
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            binding.recyclerView.setAdapter(passedActivityAdapter);
        }else{
            activityHistoryList.addAll(activities);
            passedActivityAdapter.notifyDataSetChanged();
        }
        firstFetchActivity = false;

    }

    private void initAction() {
        binding.swipeRefresh.setOnRefreshListener(() -> {
            binding.swipeRefresh.setRefreshing(false);
            firstFetchActivity = true;
            fetchActivities(1);
            pageNo = 1;
        });

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if(linearLayoutManager.findLastCompletelyVisibleItemPosition() == activityHistoryList.size() -1){
                    getMoreActivityHistory();
                }
            }
        });
    }

    private void getMoreActivityHistory(){
        pageNo ++;
        fetchActivities(pageNo);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}