package com.daikou.eazy_pass.ui.wallet

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.CustomTransactionModel
import com.daikou.eazy_pass.data.model.response_model.transaction.TransactionResponse
import com.daikou.eazy_pass.databinding.ActivityMainWalletBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.helper.UpdateTransaction
import com.daikou.eazy_pass.ui.adapter.TransactionAdapter
import com.daikou.eazy_pass.ui.bottom_sheet.TransactionDetailFragment
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.TransactionViewModel

class MainWalletActivity : BaseActivity<TransactionViewModel>() {
    private var transactionAdapter: TransactionAdapter = TransactionAdapter()
    private lateinit var binding : ActivityMainWalletBinding
    private lateinit var customTransactions : ArrayList<CustomTransactionModel>
    private var pageNo  = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainWalletBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initAction()
        doObserve()
    }

    private fun initView() {
        if (Helper.AuthHelper.hasUserInSharePreference(self())){
            val user = Helper.AuthHelper.getUserSharePreference(self())
            binding.tvTotalBalance.text = Helper.amountFormat("$", user.totalBalance ?: 0.0)
        }
        // loadTransactions()
    }

    private fun loadTransactions() {
//        binding.loadingView.root.visibility = View.VISIBLE
//        binding.trxRecyclerView.visibility = View.GONE
        val bodyQuery  = HashMap<String, Any>()
        bodyQuery["page"] = pageNo
        bodyQuery["limit"] = 20
        mViewModel.fetchTransaction(bodyQuery)
    }

    private var firstLoadTransaction = true

    private fun doObserve(){
        mViewModel.onLoadingTrx.observe(this){
            binding.loadingViewRecyclerView.root.visibility = if (it && firstLoadTransaction) View.VISIBLE else View.GONE
            binding.trxRecyclerView.visibility = if(it && firstLoadTransaction) View.GONE else View.VISIBLE
            binding.progressBar.visibility = if (it && !firstLoadTransaction) View.VISIBLE else View.GONE
        }
        mViewModel.transactionRespond.observe(this){
            if (it.success){
                val transactionResponse = it.data
                initUI(transactionResponse)
            }
        }
        mViewModel.errorMessage.observe(this){
            MessageUtil.showError(self(), "", it)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initUI(transactionResponse: TransactionResponse?){
        transactionResponse?.transactions.let {listTransaction->
            if (listTransaction != null){
                var tempList = UpdateTransaction().getCustomTransactions(listTransaction)
                if (!firstLoadTransaction){
                    customTransactions.addAll(tempList)
                    transactionAdapter.notifyDataSetChanged()
                } else {
                    customTransactions = tempList
                    transactionAdapter.setTransactionList(customTransactions)
                    binding.trxRecyclerView.apply {
                        layoutManager = LinearLayoutManager(self())
                        adapter = transactionAdapter
                    }
                }
                binding.trxRecyclerView.visibility = if (customTransactions.isEmpty()) View.GONE else View.VISIBLE
                binding.noDataFoundView.root.visibility = if (customTransactions.isEmpty()) View.VISIBLE else View.GONE
            }
        }

        // Save Amount To User
        val user = Helper.AuthHelper.getUserSharePreference(self())
        if (transactionResponse?.totalBalance != null) {
            user.totalBalance = transactionResponse.totalBalance
            Helper.AuthHelper.saveUserSharePreference(self(), user)
        }

        binding.tvTotalBalance.text = Helper.amountFormat("$",transactionResponse?.totalBalance?: 0.0)

        firstLoadTransaction = false
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initAction() {
        var isShow = false
        binding.actionBackImg.setOnClickListener {
            finish()
        }

        binding.trxRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView : RecyclerView, newState : Int) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            override fun onScrolled( recyclerView : RecyclerView, dx : Int,dy : Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager =  recyclerView.layoutManager as LinearLayoutManager
                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == customTransactions.size - 1) {
                        //bottom of list!
                        getMoreTransactionData();
                }
            }
        });


        binding.showMoneyImg.setOnClickListener {
            isShow = !isShow
            binding.showMoneyImg.setImageResource(if(isShow) R.drawable.eye_ic else R.drawable.visibility_off_ic)
            binding.hideBalanceTv.visibility = if (isShow) View.GONE else View.VISIBLE
            binding.tvTotalBalance.visibility = if (isShow) View.VISIBLE else View.GONE
        }

        binding.actionWithdrawLinearLayout.setOnClickListener {
            val intent = Intent(this@MainWalletActivity, WithdrawActivity::class.java)
            startActivity(intent)
        }

        binding.actionDepositLinearLayout.setOnClickListener {
            RedirectClass.gotoDepositActivity(self())
        }

        binding.swipeRefresh.setOnRefreshListener {
            onRefreshItem()
        }

        transactionAdapter.selectTransaction = {
            val transactionFragment = TransactionDetailFragment.newInstance(it)
            transactionFragment.show(supportFragmentManager, "transactionFragment")
        }
    }

    private fun onRefreshItem() {
        firstLoadTransaction = true
        pageNo = 1
        binding.swipeRefresh.isRefreshing = false
        loadTransactions()
    }

    private fun getMoreTransactionData() {
        pageNo ++
        loadTransactions()
    }

    override fun onResume() {
        super.onResume()
        onRefreshItem()
    }

}