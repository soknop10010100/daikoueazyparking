package com.daikou.eazy_pass.ui.adapter

import android.content.Context
import android.widget.Toast

fun Context.nameTest() {
    Toast.makeText(this, " ", Toast.LENGTH_SHORT).show()
}

val String.nameTest get() = "$this hello"
