package com.daikou.eazy_pass.ui.bottom_sheet

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.FragmentBiometricDialogBottomSheetBinding
import com.daikou.eazy_pass.helper.BiometricSecurity
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.util.Constants
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BiometricDialogBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentBiometricDialogBottomSheetBinding
    private lateinit var fContext: FragmentActivity

    companion object {
        @JvmStatic
        fun newInstance() =
            BiometricDialogBottomSheetFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.TopRoundCornerBottomSheetDialogTheme)
        arguments?.let {
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBiometricDialogBottomSheetBinding.inflate(inflater, container, false)

        initView()
        initAction()

        return binding.root
    }

    private fun initView() {
        val biometricKey = Helper.getStringSharePreference(fContext, Constants.BIOMETRIC_KEY)
        if (biometricKey.isNotEmpty()){
            binding.enableBiometricSwitchmaterial.isChecked = biometricKey == "1"
        }
    }


    private fun initAction(){
        binding.actionCloseImg.setOnClickListener {
            dialog?.dismiss()
        }
        binding.enableBiometricSwitchmaterial.setOnClickListener {
            val hasBiometric = BiometricSecurity.checkBiometric(fContext)
            if(hasBiometric){
                if (binding.enableBiometricSwitchmaterial.isChecked) {
                    Helper.setStringSharePreference(fContext, Constants.BIOMETRIC_KEY, "1")
                }else{
                    Helper.setStringSharePreference(fContext, Constants.BIOMETRIC_KEY , "0")
                }
            }else{
                binding.enableBiometricSwitchmaterial.isChecked = false
            }
        }
    }
}