package com.daikou.eazy_pass.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.annotation.RequiresApi
import androidx.core.widget.doOnTextChanged
import com.bumptech.glide.Glide
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.callback.SelectDateListener
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.databinding.ActivityEditProfileBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.ui.bottom_sheet.DateBottomSheetDialogFragment
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseCameraActivity
import com.daikou.eazy_pass.view_model.SignUpViewModel
import java.util.*

class EditProfileActivity : BaseActivity<SignUpViewModel>() {

    private lateinit var binding : ActivityEditProfileBinding
    companion object{
        const val user_key = "user_key"
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAppBar()
        initView()
        initAction()
        validateInputField()
    }

    private fun initView() {
        if (intent.hasExtra(user_key) && intent.getSerializableExtra(user_key) != null){
            val user = intent.getSerializableExtra(user_key) as User
            Glide.with(this)
                .load(user.avatarUrl)
                .placeholder(R.drawable.default_profile)
                .into(binding.profileCImg)
            binding.firstNameTextInputEditText.setText(user.firstName)
            binding.lastNameTextInputEditText.setText(user.lastName)
            binding.genderTextEditText.setText(if (user.gender == 0) R.string.male else R.string.female)
            binding.birthdayTextInputLayout.visibility = if (user.birthday != null) View.VISIBLE else View.GONE
            binding.birthdayLabel.visibility = if (user.birthday != null) View.VISIBLE else View.GONE
            user.birthday?.let {
                val formatDate = Helper.formatDatFromDatetime(it , "yyyy-MM-dd", "E, dd MMMM yyyy")
                binding.birthdayEdt.setText(formatDate)
            }
            binding.vehicleType.setText(user.kycDocument?.vehicleType ?: "")
            Log.d("editprofile", "initView: " + user.kycDocument.toString())
            binding.plateNumberEditText.setText(user.kycDocument?.plateNumber ?: "")
        }
    }

    private fun validateInputField() {
        binding.firstNameTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(text, binding.firstNameTextInputLayout, getString(R.string.field_is_required))
            }
        }

        binding.lastNameTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(text, binding.lastNameTextInputLayout, getString(R.string.field_is_required))
            }
        }
//        binding.emailTextInputEditText.doOnTextChanged { text, start, before, count ->
//            text?.let {
//                Helper.validateTextInputField(text, binding.emailTextInputLayout, getString(R.string.field_is_required))
//            }
//        }
//        binding.genderEdt.doOnTextChanged { text, start, before, count ->
//            text?.let {
//                Helper.validateTextInputField(text, binding.genderTextInputLayout, getString(R.string.field_is_required))
//            }
//        }

        binding.birthdayEdt.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(text, binding.birthdayTextInputLayout, getString(R.string.field_is_required))
            }
        }
//        binding.vehicleEdt.doOnTextChanged { text, start, before, count ->
//            text?.let {
//                Helper.validateTextInputField(text, binding.vehicleTextInputLayout, getString(R.string.field_is_required))
//            }
//        }
//        binding.plateNumberTextInputEditText.doOnTextChanged { text, start, before, count ->
//            text?.let {
//                Helper.validateTextInputField(text, binding.plateNumberTextInputLayout, getString(R.string.field_is_required))
//            }
//        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initAppBar(){
        this.setSupportActionBar(binding.appBarLayout.toolbar)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.appBarLayout.title.text = getString(R.string.my_profile)
    }


    private fun initAction(){
        validateInputField()
        val genderLabels = resources.getStringArray(R.array.gender_label)
        val date = Date()

//        binding.genderEdt.setOnClickListener{
//            val builder = AlertDialog.Builder(this)
//            builder.setSingleChoiceItems(genderLabels, 0
//            ) { dialog, position ->
//
//            }
//            builder.create().show()
//        }

        binding.birthdayEdt.setOnClickListener {
            val selectedDateListener = object : SelectDateListener{
                override fun selectDateListener(date: Date) {

                }

            }
            val birthdayDialog = DateBottomSheetDialogFragment.newInstance("Select Date", date, selectedDateListener)
            birthdayDialog.show(supportFragmentManager, "dateBottomSheetDialog")
        }

        binding.profileCImg.setOnClickListener {
           val intent = Intent(this@EditProfileActivity, BaseCameraActivity::class.java)
            gotoActivityForResult(this, intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
                override fun onActivityResult(result: ActivityResult) {
                    if (result.resultCode == Activity.RESULT_OK) {
                        val data = result.data
                        if (data?.hasExtra("image") == true){
                            val imagePath = data.getStringExtra("image")
//                            val imageUri = Uri.parse(imagePath)
//                            val imageBitmap = Helper.convertBitmap(self(), imageUri)
                            Glide.with(this@EditProfileActivity).load(imagePath).into(binding.profileCImg)
                        }
                    }
                }
            })
        }
    }
}