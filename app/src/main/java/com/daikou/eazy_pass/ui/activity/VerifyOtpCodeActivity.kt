package com.daikou.eazy_pass.ui.activity

import android.os.Bundle
import android.os.CountDownTimer
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.handler.NetworkHandler
import com.daikou.eazy_pass.data.handler.NetworkHandler.checkConnection
import com.daikou.eazy_pass.databinding.ActivityVerifyOtpCodeBinding
import com.daikou.eazy_pass.enumerable.ForgotChangeEnum
import com.daikou.eazy_pass.enumerable.VerifyPinEnum
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.PasswordVM
import java.util.*
import java.util.concurrent.TimeUnit

class VerifyOtpCodeActivity : BaseActivity<PasswordVM>() {

    private lateinit var binding: ActivityVerifyOtpCodeBinding
    private var countDownTimer: CountDownTimer? = null
    private var globalOtpCodeView: String? = null
    private var otpToken: String? = ""

    companion object {
        const val OTP_TOKEN_KEY = "otp_token_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityVerifyOtpCodeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intiView()

        initObserved()

        doAction()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    private fun intiView() {
        this.setSupportActionBar(binding.appBar.toolbar)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.title.text = getString(R.string.verify_otp)

        if (intent.hasExtra(OTP_TOKEN_KEY)) {
            otpToken = intent.getStringExtra(OTP_TOKEN_KEY)
        }
        startCountDownCode()
    }

    private fun initObserved() {
        mViewModel.loading.observe(this) { hasLoading ->
            binding.loadingView.root.visibility = if (hasLoading) View.VISIBLE else View.GONE
        }

        mViewModel.requestOtpMutableLiveData.observe(this) {
            if (it.success) {
                RedirectClass.gotoChangePasswordActivity(
                    self(),
                    globalOtpCodeView ?: "",
                    otpToken ?: "",
                    VerifyPinEnum.ForgetPasswordScreen
                )
            }
        }

        mViewModel.requestOtpMutableLiveData.observe(this) {
            if (it.success) {
                Toast.makeText(self(), "Resend code success..!", Toast.LENGTH_SHORT).show()
            }
        }

        mViewModel.onErrorData.observe(this){
            MessageUtil.showError(self(), null, it.toString())
        }
    }

    private fun doAction() {

        binding.actionSubmit.setOnClickListener {
            if ((binding.pinView.text?.length ?: 0) < 6) {
                MessageUtil.showError(self(), null, "Please fill the otp box")
            } else {
                if (checkConnection(this)) {
                    val codeOtp: String = binding.pinView.text.toString().trim()
                    if (codeOtp.length == 6) {
                        globalOtpCodeView = codeOtp.trim()
                        val requestBodyMap = HashMap<String, Any>()
                        requestBodyMap["otp"] = codeOtp.trim()
                        requestBodyMap["otp_token"] = otpToken ?: ""
                        mViewModel.submitRequestOtp(ForgotChangeEnum.SubmitVerifyOtp, requestBodyMap)
                    }
                }
            }
        }

        binding.resentCodeTv.setOnClickListener {
            if (checkConnection(this)) {
                val user = Helper.AuthHelper.getUserSharePreference(self())
                user.phone?.let {
                    binding.resentCodeTv.isEnabled = false
                    val body = HashMap<String, Any>()
                    body["phone"] = it.trim()
                    mViewModel.submitRequestOtp(ForgotChangeEnum.SubmitRequestOtp, body)
                }
                startCountDownCode()
            }
        }

        binding.didNotReceiveCodeTv.setOnClickListener {
            val message = String.format("%s\n%s\n%s\n%s\n%s",
                resources.getString(R.string.could_not_received),
                resources.getString(R.string.sms_message_has_been_send_to_your_phone_if_you_don_t_get_the_code_after_serveral_attempts_try),
                resources.getString(R.string._1_check_if_the_funds_are_on_the_balance_of_your_phone),
                resources.getString(R.string._2_check_the_section_with_remote_sms),
                resources.getString(R.string._3_make_sure_you_are_using_this_number))
            MessageUtil.showError(self(), null, message)
            // DidnotReceiveTheCodeAlertDialog.newInstance().show(supportFragmentManager, "DidnotReceiveTheCodeAlertDialog")
        }
    }

    private fun startCountDownCode() {
        try {
            binding.resentCodeTv.visibility = View.GONE
            binding.countDownTv.visibility = View.VISIBLE
            countDownTimer = object : CountDownTimer(105000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val time = String.format(
                        Locale.getDefault(), "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60
                    )
                    binding.countDownTv.text = time
                }

                override fun onFinish() {
                    runOnUiThread {
                        binding.countDownTv.text = getString(R.string.time_countdown)
                        binding.countDownTv.visibility = View.GONE
                        binding.resentCodeTv.visibility = View.VISIBLE
                    }
                }
            }.start()
        } catch (ignored: Exception) {
        }
    }


}