package com.daikou.eazy_pass.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.databinding.MySaveBankLayoutBinding
import com.daikou.eazy_pass.ui.payment.PaymentMethodFragment
import com.daikou.eazy_pass.util.Constants

class BanksAdapter() : RecyclerView.Adapter<BanksAdapter.BanksViewHolder>() {
    private  lateinit var savedBankList: ArrayList<SaveBankAccount>
    lateinit var fContext: FragmentActivity
    private var tickBankAccountId: Int? = null
    private lateinit var layoutBinding: MySaveBankLayoutBinding
    private var checkedPosition : Int  = -1
    lateinit var onClickBank : (isClick : Boolean, saveBackAccount : SaveBankAccount) -> Unit

    constructor( context : FragmentActivity, savedBankList: ArrayList<SaveBankAccount> , tickBankAccountId: Int) : this() {
        this.savedBankList = savedBankList
        fContext = context
        this.tickBankAccountId = tickBankAccountId
    }

    fun initCheckedPosition(savedBankList: ArrayList<SaveBankAccount>, tickBankAccountId: Int){
        for (bankAccount in savedBankList){
            if(bankAccount.userId == tickBankAccountId){
                checkedPosition = savedBankList.indexOf(bankAccount)
                break
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateAdapter(savedBankList: ArrayList<SaveBankAccount>, tickBankAccountId: Int){
        this.savedBankList = savedBankList
        this.tickBankAccountId = tickBankAccountId
        for (bankAccount in savedBankList){
            if(bankAccount.userId == tickBankAccountId){
                layoutBinding.unTick.visibility = View.GONE
                this.notifyItemChanged(savedBankList.indexOf(bankAccount))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BanksViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        layoutBinding = MySaveBankLayoutBinding.inflate(layoutInflater, parent, false)
        return BanksViewHolder(layoutBinding)
    }

    override fun getItemCount(): Int {
        return savedBankList.size
    }

    override fun onBindViewHolder(holder: BanksViewHolder, position: Int) {
        val savedBankAccount = savedBankList[position]
        holder.bind(savedBankAccount)
        holder.saveBankLayoutBinding.root.setOnClickListener {
            holder.saveBankLayoutBinding.unTick.visibility = View.GONE
            if (checkedPosition != holder.adapterPosition) {
                notifyItemChanged(checkedPosition)
                checkedPosition = holder.adapterPosition
            }
            onClickBank.invoke(true, savedBankAccount)

        }
    }

    inner class BanksViewHolder(saveBankLayoutBinding : MySaveBankLayoutBinding) : ViewHolder(saveBankLayoutBinding.root){
        val saveBankLayoutBinding by lazy { saveBankLayoutBinding }
        fun bind(savedBankAccount : SaveBankAccount){
            saveBankLayoutBinding.accountNumberTv.text = savedBankAccount.accountNumberFormat
            if(savedBankAccount.bic == Constants.BankBic.bicABA){
                saveBankLayoutBinding.accountNameTv.text = savedBankAccount.ownerName ?: ""
                saveBankLayoutBinding.logoImg.setImageResource(R.drawable.aba_pay_icon)
            }else{
                saveBankLayoutBinding.accountNameTv.text = fContext.resources.getString(R.string.acleda_bank_plc)
                saveBankLayoutBinding.logoImg.setImageResource(R.drawable.acleda_logo)
            }
            if (checkedPosition  == adapterPosition){
                saveBankLayoutBinding.unTick.visibility = View.GONE
            }else{
                saveBankLayoutBinding.unTick.visibility = View.VISIBLE
            }
        }
    }

}