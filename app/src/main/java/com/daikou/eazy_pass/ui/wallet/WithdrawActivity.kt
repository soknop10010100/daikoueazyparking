package com.daikou.eazy_pass.ui.wallet

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import androidx.activity.result.ActivityResult
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.databinding.ActivityWithdrawBinding
import com.daikou.eazy_pass.enumerable.PaymentOption
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.payment.PaymentMethodFragment
import com.daikou.eazy_pass.util.AutoFillNumberRecyclerView
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseEdittextListener
import com.daikou.eazy_pass.util.base.BaseListenerAutoFillNumber
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.PaymentViewModel
import com.daikou.eazy_pass.view_model.SignUpViewModel

class WithdrawActivity : BaseActivity<PaymentViewModel>() {
    private var isShowAmount: Boolean = false
    private lateinit var binding : ActivityWithdrawBinding
    private lateinit var savedListBank : ArrayList<SaveBankAccount>
    private lateinit var saveBankAccount: SaveBankAccount
    private var mAmount : Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWithdrawBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()

        initAction()

        initBankViewInUi(false)

        doObserve()
    }

    private fun initView() {
        saveBankAccount = Helper.getSelectedBank(self())
        if (Helper.getListBankJson(self()).size < 1){
            binding.mySaveBankLinearLayout.visibility = View.GONE
            binding.actionChangeCardBankAccountTv.visibility = View.GONE
            binding.actionAddCardOtherBankTv.visibility = View.VISIBLE
        }else {
            savedListBank = Helper.getListBankJson(self())
        }

        //binding.withdrawBtn.isEnabled = enableButtonSubmit()

        setAmount(0.0)
    }

    private fun enableButtonSubmit() : Boolean {
        if (binding.amountEdt.text?.isEmpty() == true)
            return false

        val amountDoubleString = binding.amountEdt.text.toString()
        val amountDouble = amountDoubleString.toDoubleOrNull()
        if(amountDouble != null && (amountDouble <= (Helper.AuthHelper.getUserSharePreference(self()).totalBalance ?: 0.0))){   // Amount input smaller that main wallet no allow
            if (binding.actionAddCardOtherBankTv.visibility == View.GONE) {
                return true
            }
        }

        return false
    }

    private fun initAction() {
        binding.autoFillNumberRecyclerView.setBaseListener(numberClickListener)
        binding.actionBackImage.setOnClickListener {
            finish()
        }
        binding.hideImg.setOnClickListener {
            isShowAmount = !isShowAmount
            if (isShowAmount){
                binding.amountTv.visibility = View.VISIBLE
                binding.hideAmountTv.visibility = View.GONE
                binding.hideImg.setImageResource(R.drawable.eye_ic)
            }else{
                binding.hideAmountTv.visibility = View.VISIBLE
                binding.amountTv.visibility = View.GONE
                binding.hideImg.setImageResource(R.drawable.visibility_off_ic)
            }
        }

        binding.actionAddCardOtherBankTv.setOnClickListener {
            PaymentMethodFragment.newInstance(
                saveBankAccount,
                PaymentOption.AddNewCard,
                savedBankList = null,
                idTickBankAccount = -1
            ) { hasChange, savedBank ->
                this.saveBankAccount = savedBank
                initBankViewInUi(false)
            }.apply {
                show(supportFragmentManager, "PaymentMethodFragment")
            }
        }
        binding.actionChangeCardBankAccountTv.setOnClickListener {
            PaymentMethodFragment.newInstance(
                saveBankAccount = saveBankAccount,
                actionPaymentOption = PaymentOption.ChangeCard,
                savedBankList = savedListBank,
                idTickBankAccount = saveBankAccount.userId
            ){ hasChange , savedBank ->
                this.saveBankAccount = savedBank
                initBankViewInUi(hasChange)
            }.apply {
                show(supportFragmentManager, "PaymentMethodFragment")
            }
        }

        binding.withdrawBtn.setOnClickListener {
            val body = HashMap<String, Any>()
            val amountString = binding.amountEdt.text.toString()
            body["bank_account_number"] = saveBankAccount.accountNumber ?: ""
            body["bic"] = saveBankAccount.bic
            body["remark"] = binding.commentEdt.text.toString()
            body["total_amount"] = amountString

            if (!TextUtils.isEmpty(amountString)) {
                mAmount = amountString.toDouble()
            }

            mViewModel.withdrawMoney(body)
        }

        binding.amountEdt.addTextChangedListener(object : BaseEdittextListener() {
            override fun afterTextChangedNotEmpty(editable: Editable) {
                //binding.withdrawBtn.isEnabled = enableButtonSubmit()
            }

            override fun afterTextChangedIsEmpty() {
                //binding.withdrawBtn.isEnabled = enableButtonSubmit()
            }

        })
    }

    private fun doObserve(){
        mViewModel.loading.observe(this){
            binding.loadingView.root.visibility = if (it) View.VISIBLE else View.GONE
        }

        mViewModel.withdrawResponse.observe(this){
            if (it.success){
                val jsonObject = it.data?.asJsonObject
                jsonObject?.let {
                    if (jsonObject.has("verify_otp_url")){
                        val url = jsonObject.get("verify_otp_url").asString
                        RedirectClass.gotoWebPay(self(), url, object : BetterActivityResult.OnActivityResult<ActivityResult>{
                            override fun onActivityResult(result: ActivityResult) {
                                if (result.resultCode == Activity.RESULT_OK) {
                                    val data = result.data
                                    if (data != null && data.hasExtra("status")){
                                        val mStatus = data.getStringExtra("status")
                                        if(mStatus.equals("success=1", true)) {
                                            setAmount(mAmount)
                                            finish()
                                        }
                                    }
                                }
                            }

                        })
                    }
                }
            }
        }
        mViewModel.errorMessage.observe(this){
            MessageUtil.showError(self(), "", it)
        }
    }

    private fun initBankViewInUi(isViewChange : Boolean){
        savedListBank = Helper.getListBankJson(context = self())
        if(!isViewChange){
            if(savedListBank.size > 0){
                binding.mySaveBankLinearLayout.visibility = View.VISIBLE
                binding.actionAddCardOtherBankTv.visibility = View.GONE
                binding.actionChangeCardBankAccountTv.visibility = View.VISIBLE

                binding.withdrawBtn.isEnabled = binding.amountEdt.text.toString().isNotEmpty()

                binding.saveBankLayout.bankName.text = saveBankAccount.ownerName
                binding.saveBankLayout.bankAccount.text = saveBankAccount.accountNumberFormat
                if (saveBankAccount.bic == Constants.BankBic.bicACL){
                    binding.saveBankLayout.bankLogo.setImageResource(R.drawable.acleda_logo)
                }else{
                    binding.saveBankLayout.bankLogo.setImageResource(R.drawable.aba_pay_icon)
                }
            }else{
                binding.mySaveBankLinearLayout.visibility = View.GONE
                binding.actionAddCardOtherBankTv.visibility = View.VISIBLE
                binding.actionChangeCardBankAccountTv.visibility = View.GONE
            }
        }else{
            if (savedListBank.size >0){
                binding.mySaveBankLinearLayout.visibility = View.VISIBLE
                binding.actionAddCardOtherBankTv.visibility = View.GONE
                binding.actionChangeCardBankAccountTv.visibility = View.VISIBLE

                binding.saveBankLayout.bankName.text = saveBankAccount.ownerName
                binding.saveBankLayout.bankAccount.text = saveBankAccount.accountNumberFormat
                if (saveBankAccount.bic == Constants.BankBic.bicACL){
                    binding.saveBankLayout.bankLogo.setImageResource(R.drawable.acleda_logo)
                }else{
                    binding.saveBankLayout.bankLogo.setImageResource(R.drawable.aba_pay_icon)
                }
            }
        }
    }

    private val numberClickListener : BaseListenerAutoFillNumber<AutoFillNumberRecyclerView.Item?> = object : BaseListenerAutoFillNumber<AutoFillNumberRecyclerView.Item?>{
        override fun onResult(data: AutoFillNumberRecyclerView.Item?) {
            if(data != null) {
                binding.amountEdt.setText(String.format("%s", data.numberStr))

                //binding.withdrawBtn.isEnabled = enableButtonSubmit()
            }
        }

    }

//    private fun initAppbar() {
//        this.setSupportActionBar(binding.appBarLayout.toolbar)
//        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
//        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//
//        binding.appBarLayout.title.text = getString(R.string.withdraw)
//    }

    private fun setAmount(totalBalance: Double) {
        val user = Helper.AuthHelper.getUserSharePreference(self())
        val mTotalAmount = (user.totalBalance ?: 0.0) - totalBalance
        binding.amountTv.text = Helper.amountFormat("$", mTotalAmount)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}