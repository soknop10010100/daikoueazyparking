package com.daikou.eazy_pass.ui.profile

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.databinding.ActivityProfileBinding
import com.daikou.eazy_pass.enumerable.LanguageSetting
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.ui.bottom_sheet.BiometricDialogBottomSheetFragment
import com.daikou.eazy_pass.ui.bottom_sheet.LanguageBottomSheetDialogFragment
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.LanguageManager
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.LongTermViewModel
import com.daikou.eazy_pass.view_model.ProfileViewModel

class ProfileActivity : BaseActivity<ProfileViewModel>() {
    private lateinit var binding : ActivityProfileBinding
    private lateinit var user : User
    private lateinit var longTermAdapter : LongTermAdapter
    private lateinit var longTermViewModel: LongTermViewModel
    var isFirstInit = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        this.setSupportActionBar(binding.appBar.toolbar)

        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        longTermViewModel = LongTermViewModel()

        binding.appBar.title.setText(R.string.profile)
        initData()
        initAction()
        doObserve()
    }

    private fun initData() {
        if (Helper.AuthHelper.hasUserInSharePreference(self())){
            user = Helper.AuthHelper.getUserSharePreference(self())
            updateUI(user)
            mViewModel.getUserInfo()
            if (!hasInternetConnection()){
                initLongTermView(user)
            }
        }
    }
    private fun updateUI(user : User){
        Glide.with(this)
            .load(user.avatarUrl ?: "")
            .placeholder(R.drawable.default_profile)
            .into(binding.profileImg)

        binding.nameTv.text = user.name?: "N/A"
        binding.phoneNumberTv.text = user.phone?: "N/A"

        initUserVerified()

    }

    private fun initUserVerified() {
        val isKycUser = user?.kycDocument?.status.equals(Constants.KycDocStatus.Verified)
        val isKycPending = user?.kycDocument?.status.equals(Constants.KycDocStatus.Pending)
        binding.verifyContainer.background = if (isKycUser) ContextCompat.getDrawable(
            this,
            R.drawable.verify_user_bg_drawable
        ) else if (isKycPending) ContextCompat.getDrawable(
            this,
            R.drawable.verify_user_bg_drawable
        )
        else ContextCompat.getDrawable(
            this,
            R.drawable.un_verify_user_background_drawable
        )
        binding.verifiedImg.setImageDrawable(
            when {
                isKycUser -> ContextCompat.getDrawable(
                    this,
                    R.drawable.verified
                )
                isKycPending -> ContextCompat.getDrawable(this, R.drawable.watting_icon)
                else -> ContextCompat.getDrawable(this, R.drawable.not_verified)
            }
        )
        binding.verifiedImg.setColorFilter(
            when {
                isKycUser -> ContextCompat.getColor(
                    this,
                    R.color.verified_color_text
                )
                isKycPending -> ContextCompat.getColor(this, R.color.orange_300)
                else -> ContextCompat.getColor(this, R.color.un_verified_color_text)
            }
        )
        binding.verifiedTv.setTextColor(
            when {
                isKycUser -> ContextCompat.getColor(
                    this,
                    R.color.verified_color_text
                )
                isKycPending -> ContextCompat.getColor(this, R.color.orange_300)
                else -> ContextCompat.getColor(this, R.color.un_verified_color_text)
            }
        )
        binding.verifiedTv.text = if (isKycUser) this.getString(R.string.verified)
        else getString(
            R.string.not_verified
        )

        if (isKycUser) binding.verifiedTv.text = getString(R.string.verified)
        else if (isKycPending) binding.verifiedTv.text = getString(R.string.pending)
        else binding.verifiedTv.text = getString(R.string.refused)
    }

    private fun initLongTermView(user: User){
        if (user.longTermList!!.isNotEmpty()){
            longTermAdapter = LongTermAdapter(user.longTermList!!, true)
            binding.userLongTermRecyclerView.apply {
                layoutManager = LinearLayoutManager(self(), LinearLayoutManager.VERTICAL, false)
                adapter = longTermAdapter
            }
            longTermAdapter.onSelect = {
                if (it.status == "active") {
                    RedirectClass.gotoLongTermHistoryActivity(self(), it.projectId ?: -1)
                }
            }
            longTermAdapter.onClickRenew = {
                MessageUtil.showConfirm(self(), "", "Do you want to renew","renew", true){sweetDialog ->
                    longTermViewModel.renewLongTerm(it.projectId!!)
                    sweetDialog.dismiss()
                }
            }
        }else{
            binding.userLongTermRecyclerView.visibility = View.GONE
        }
    }

    private fun doObserve() {
        mViewModel.loading.observe(this){
            binding.loadingView.root.visibility = if (it && !isFirstInit) View.VISIBLE else View.GONE
        }
        mViewModel.logoutSuccess.observe(this){
            if (it){
                RedirectClass.gotoLoginActivity(self())
                finishAffinity()
            }
        }
        mViewModel.userResponseData.observe(this){
            if (it.success){
                it.data?.let { userResponse ->
                    user = userResponse
                    if(isFirstInit) {
                        initLongTermView(user)
                        isFirstInit = false
                    }else{
                        updateUI(user)
                        initLongTermView(user)
                    }
                    Helper.AuthHelper.saveUserSharePreference(self(), user)
                }
            }
        }

        mViewModel.errorMessage.observe(this){
            MessageUtil.showError(self(), "", it)
        }

        // renew long term
        longTermViewModel.loading.observe(this){
            binding.loadingView.root.visibility = if (it) View.VISIBLE else View.GONE
        }

        longTermViewModel.renewLongTermLiveData.observe(this){
            if (it.success){
                MessageUtil.showSuccess(self(), getString(R.string.update_successfully), "Renew Successfully", true){sweetDialog ->
                    sweetDialog.dismiss()
                }
            }
        }

        longTermViewModel.onErrorLiveData.observe(this){
            if (it.contains("enough balance")){
                MessageUtil.showConfirm(self(), getString(R.string.wallet_warning ), it, getString(R.string.deposit)){sweetDialog ->
                    RedirectClass.gotoDepositActivity(self())
                    sweetDialog.dismiss()
                }
            }else{
                MessageUtil.showError(self(), "", it)
            }
        }

    }


    private fun initAction(){
        binding.actionEditProfileLinearLayout.setOnClickListener {
            RedirectClass.gotoEditProfileActivity(self(), user)
        }

        binding.actionIdentityKyc.setOnClickListener {
            val kycData = HashMap<String, Any>()
            kycData["status"] = user?.kycDocument?.status.toString()
            kycData["refused_msg"] = if (user?.kycDocument?.refuseMsg.toString() == "null") "" else user?.kycDocument?.refuseMsg.toString()
            RedirectClass.gotoIdentityVerifyActivity(
                self(),
                kycData
            )
        }

        binding.actionEnableBiometricLinearLayout.setOnClickListener {
            BiometricDialogBottomSheetFragment.newInstance().show(supportFragmentManager, "biometricDialog")
        }

        binding.actionLanguageLinearLayout.setOnClickListener {
            LanguageBottomSheetDialogFragment.newInstance {
                when (it) {
                    LanguageSetting.Khmer -> {
                        LanguageManager.setLanguage(self(), LanguageManager.KHMER)
                        RedirectClass.gotoSplashScreenActivity(self())
                        finishAffinity()
                    }
                    LanguageSetting.English ->{
                        LanguageManager.setLanguage(self(), LanguageManager.ENGLISH)
                        RedirectClass.gotoSplashScreenActivity(self())
                        finishAffinity()
                    }
                }
            }.show(supportFragmentManager, "languageDialog")
        }

        binding.actionChangePasswordLinearLayout.setOnClickListener {
            RedirectClass.gotoVerificationActivity(self(), Constants.CHANG_PASSWORD_CALLER)
        }

        binding.signOutLinear.setOnClickListener {
            MessageUtil.showConfirm(
                this,
                getString(R.string.sign_out),
                getString(R.string.confirm_to_sign_out),
                cancelAble = true
            ) {
                it.dismiss()
                mViewModel.logout()
            }
        }

        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = false
            mViewModel.getUserInfo()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}