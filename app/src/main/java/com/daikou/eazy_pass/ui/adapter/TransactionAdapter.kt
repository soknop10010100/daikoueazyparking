package com.daikou.eazy_pass.ui.adapter

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.CustomTransactionModel
import com.daikou.eazy_pass.databinding.TransactionRawLayoutBinding
import com.daikou.eazy_pass.helper.Helper

class TransactionAdapter: RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    private lateinit var transactionList: ArrayList<CustomTransactionModel>
    fun setTransactionList (transactionList: ArrayList<CustomTransactionModel>){
        this.transactionList = transactionList
    }

    lateinit var  selectTransaction : (CustomTransactionModel) -> Unit
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val transactionBinding = TransactionRawLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TransactionViewHolder(transactionBinding)
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transactionModel = transactionList[position]
        holder.bind(transactionModel)
        holder.transactionBinding.root.setOnClickListener {
            selectTransaction.invoke(transactionModel)
        }
    }

    class TransactionViewHolder(
        transactionBinding: TransactionRawLayoutBinding
        ) : ViewHolder(transactionBinding.root) {
        val transactionBinding by lazy { transactionBinding }
        fun bind(transactionModel: CustomTransactionModel){
            val noRemark = "No remark"

            transactionBinding.timeTv.text = Helper.formatTimeFromDate(transactionModel.createdAt ?: "", "yyyy-MM-dd HH:mm:ss", "h:mm a")
            transactionBinding.remarkTv.text = transactionModel.remark ?: noRemark
            transactionBinding.transferNameTv.text = transactionModel.type ?: "N/A"
            val amount = transactionModel.total ?: 0.0
            transactionBinding.amountTv.text =Helper.amountFormat("$", amount)
            if (amount < 0){
                transactionBinding.amountTv.setTextColor(
                    ContextCompat.getColor(transactionBinding.root.context, R.color.red_300)
                )
            }else{

                transactionBinding.amountTv.setTextColor(
                    ContextCompat.getColor(transactionBinding.root.context, R.color.green_600)
                )
            }

            val createAt = transactionModel.createdAt ?: ""
            val dateStr = Helper.formatDatFromDatetime(createAt, "yyyy-MM-dd HH:mm:ss", "EEEE, dd MMMM yyyy")
            transactionBinding.headerContainerView.visibility = if (transactionModel.isShowHeader == true) View.VISIBLE else View.GONE
            val date = Helper.formatToDate(createAt,  "yyyy-MM-dd HH:mm:ss")
            date?.time?.let {
                if (DateUtils.isToday(it)){ // today check
                    transactionBinding.titleDateTv.text = transactionBinding.root.context.getString(R.string.today)
                }else if (DateUtils.isToday(it + DateUtils.DAY_IN_MILLIS)){ // yesterday check
                    transactionBinding.titleDateTv.text = transactionBinding.root.context.getString(R.string.yesterday)
                }else{
                    transactionBinding.titleDateTv.text = dateStr
                }
            }
            var totalAmount = 0.0
            if (transactionModel.totalAmount != null) {
                totalAmount = transactionModel.totalAmount!![dateStr] ?: 0.0
                transactionBinding.dailyTotalAmountTv.text =
                    Helper.amountFormat("$", totalAmount)
            }
            if (totalAmount < 0){
                transactionBinding.totalByDateCardView.setCardBackgroundColor(
                    ContextCompat.getColor(transactionBinding.root.context, R.color.red_300 )
                )
            }else{
                transactionBinding.totalByDateCardView.setCardBackgroundColor(
                    ContextCompat.getColor(transactionBinding.root.context, R.color.green_600)
                )
            }
        }
    }
}