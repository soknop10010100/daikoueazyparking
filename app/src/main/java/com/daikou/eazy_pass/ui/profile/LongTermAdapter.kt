package com.daikou.eazy_pass.ui.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.user.LongTermModel
import com.daikou.eazy_pass.databinding.UserLongTermLayoutBinding
import com.daikou.eazy_pass.helper.Helper

class LongTermAdapter(val longTermList : ArrayList<LongTermModel>, var showArrow : Boolean? = null) : RecyclerView.Adapter<LongTermAdapter.LongTermViewHolder>() {

    lateinit var onSelect : (longTerm : LongTermModel) -> Unit?
    lateinit var onClickRenew : (longTerm : LongTermModel) -> Unit?

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LongTermViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val layoutBinding = UserLongTermLayoutBinding.inflate(layoutInflater, parent, false)
        return LongTermViewHolder(layoutBinding)
    }

    override fun getItemCount(): Int {
        return longTermList.size
    }

    override fun onBindViewHolder(holder: LongTermViewHolder, position: Int) {
        val longTerm = longTermList[position]
        holder.bind(longTerm)
    }

    inner class LongTermViewHolder(val layoutBinding : UserLongTermLayoutBinding) : RecyclerView.ViewHolder(layoutBinding.root){
        fun bind(longTerm : LongTermModel){
            layoutBinding.projectNameTv.text = longTerm.location ?: ""
            val fromDate = Helper.formatDatFromDatetime(longTerm.fromDate!!, "yyyy-MM-dd HH:ss:mm", "dd MMM yyyy")
            val toDate = Helper.formatDatFromDatetime(longTerm.toDate!!, "yyyy-MM-dd HH:ss:mm", "dd MMM yyyy")
            val context = layoutBinding.root.context
            val duration = String.format("%s %s %s %s", context.getString(R.string.from), fromDate, context.getString(R.string.to), toDate)
            layoutBinding.durationTv.text = duration
            layoutBinding.remainDayTv.text = longTerm.totalRemainDay.toString()
            layoutBinding.dayTv.text = if(longTerm.totalRemainDay!! > 0) context.getString(R.string.day) else context.getString(R.string.day).replace("s", "")
            val remainDayPercentage = (longTerm.totalRemainDay!!.toDouble() / longTerm.totalDay!!.toDouble()) * 100
            layoutBinding.remainDateIndicator.progress = remainDayPercentage.toInt()
            layoutBinding.layoutExpiredTv.visibility = if (longTerm.status == "active") View.GONE else View.VISIBLE
            layoutBinding.layoutShowRemainDay.visibility = if (longTerm.status == "active") View.VISIBLE else View.GONE
            layoutBinding.renewBtn.visibility = if (showArrow == true) View.VISIBLE else View.GONE // hide in history activity
            layoutBinding.renewBtn.setOnClickListener {
                onClickRenew.invoke(longTerm)
            }
            layoutBinding.arrowEnd.visibility = if (longTerm.status == "active" && showArrow == true)View.VISIBLE else View.GONE

            layoutBinding.root.setOnClickListener {
                onSelect.invoke(longTerm)
            }
        }
    }
}