package com.daikou.eazy_pass.ui.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import com.budiyev.android.codescanner.CodeScanner
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.ActivityScanQrBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.helper.PermissionHelper
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.view_model.PaymentViewModel
import com.daikou.eazy_pass.sdk.ParkingController
import pl.aprilapps.easyphotopicker.ChooserType
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.FileNotFoundException
import java.io.InputStream

class ScanQrActivity : BaseActivity<PaymentViewModel>(){
    private lateinit var binding : ActivityScanQrBinding
    lateinit var codeScanner: CodeScanner
    lateinit var parkingController: ParkingController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScanQrBinding.inflate(layoutInflater)
        setContentView(binding.root)
        parkingController = ParkingController()

        initView()
        initAction()
        doScanQR()
        doObserve()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initAction() {
        binding.errorQrView.rescanBtn.setOnClickListener{
            showErrorScan(false)
        }

        binding.uploadQrLinearLayout.setOnClickListener {
            if(PermissionHelper.isHasExternalStoragePermission(this)){
//                chooseQrImage()
                chooseFromGallery(ChooserType.CAMERA_AND_GALLERY,easyCallbacks)
            }
        }
    }

    private val easyCallbacks = object  : EasyImage.Callbacks{
        override fun onCanceled(source: MediaSource) {

        }

        override fun onImagePickerError(error: Throwable, source: MediaSource) {
            MessageUtil.showError(self(),"", error.message)
        }

        override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
            val uri = Uri.fromFile(imageFiles[0].file)
            binding.loadingView.root.visibility = View.VISIBLE
            codeScanner.stopPreview()

            uri?.let {
                Glide.with(this@ScanQrActivity)
                    .asBitmap()
                    .load(it)
                    .into(object : CustomTarget<Bitmap?>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap?>?
                        ) {
                            binding.loadingView.root.visibility = View.GONE
                            val resultStr = Helper.convertQRImageToText(resource)
                            if (resultStr.equals("")){
                                showErrorScan(true, getString(R.string.no_qr_detect))
                            }else {
                                Log.d("hhhhhhhhhhhhh", resultStr)
                            }
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            binding.loadingView.root.visibility = View.GONE
                        }

                    })
            }

        }

    }

    private fun chooseQrImage() {
        codeScanner.stopPreview()
        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        activityLauncher.launch(intent, object : BetterActivityResult.OnActivityResult<ActivityResult>{
            override fun onActivityResult(result: ActivityResult) {
                if(result.resultCode == Activity.RESULT_OK) {
                    val intentImagePickup = result.data
                    if (intentImagePickup != null) {
                        val selectedImageUri: Uri? = intentImagePickup.data
                        var imageStream: InputStream? = null
                        try {
                            //getting the image
                            imageStream = contentResolver.openInputStream(selectedImageUri!!)
                        } catch (e: FileNotFoundException) {
                            Toast.makeText(applicationContext, "File not found", Toast.LENGTH_SHORT)
                                .show()
                            e.printStackTrace()
                        }
                        val bMap: Bitmap = BitmapFactory.decodeStream(imageStream)
                        val resultText = Helper.convertQRImageToText(bMap)
                        Log.d("qr result", resultText)
                    } else {
                        Log.d("error get image ", "nonoooooooooooooooooo")
                        showErrorScan(true, "Can not upload the image")
                    }
                }
            }

        })
    }
    private fun doObserve(){
        mViewModel.dataPaymentResponse.observe(this){
//            if (it.success){
//                if (it.data != null){
//                    val passedActivityModel = it.data
//                    passedActivityModel.let { activityPassed ->
//                        val date = Date()
//                        if (activityPassed.total != null) {
//                            val exitAt = Helper.formatDate(date, "E,dd-MM-yyy h:mm:ss a")
//                            val entranceAt = Helper.PassedActivityHelper.getEntranceDate(self())
//                            activityPassed.exitAt = exitAt ?: ""
//                            activityPassed.entranceAt = entranceAt
//                            Helper.PassedActivityHelper.savePassedActivity(self(), activityPassed)
//                        }else{
//                            Helper.PassedActivityHelper.saveEntranceDate(self(), date)
//                            val entranceAt = Helper.formatDate(date, "E,dd-MM-yyy h:mm:ss a")
//                            activityPassed.entranceAt = entranceAt ?: ""
//                            Helper.PassedActivityHelper.savePassedActivity(self(), activityPassed)
//                        }
//                    }
//                }
//            }
        }

        mViewModel.barrierId.observe(this){
//            parkingController.connectToParking(this, it, object : ParkingController.ParkingCallBack{
//                override fun onError(message: String?) {
//
//                }
//
//                override fun onParkingOpen(message: String?, name : String?) {
//
//                }
//
//            })
        }

        mViewModel.errorMessage.observe(this){
            MessageUtil.showError(self(), "", it)
            startCameraScan()
        }
    }



    private fun doScanQR(){
        codeScanner = CodeScanner(this, binding.codeScannerView)

        PermissionHelper.requestMultiPermission(self(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)){ isHasPermission ->
            if(isHasPermission){
                startCameraScan()
            }else{
                MessageUtil.showError(this, null, "Permission Camera Deny")
            }
        }
    }

    private fun initView(){
        this.setSupportActionBar(binding.appBar.toolbar)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.appBar.title.setText(R.string.scan_qr)

    }

    private fun startCameraScan() {
        codeScanner.setDecodeCallback {
            if (it.text.isNotEmpty()){
                val body = HashMap<String, Any>()
                body["barrier_id"] = "@DZA53310"
                mViewModel.openGateDoPayment(body)
            }else{
                showErrorScan(true, "Invalid QR code")
            }
        }
    }

    private fun showErrorScan(isShow: Boolean, errorMessage: String? = "---") {
        if(isShow){
            codeScanner.stopPreview()
            binding.errorQrView.root.visibility = View.VISIBLE
            binding.uploadQrLinearLayout.visibility = View.GONE
            binding.errorQrView.errorMsgTv.text = errorMessage
        }else{
            codeScanner.startPreview()
            binding.uploadQrLinearLayout.visibility = View.VISIBLE
            binding.errorQrView.root.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        super.onPause()
        codeScanner.releaseResources()
    }

    override fun onDestroy() {
        super.onDestroy()
        codeScanner.stopPreview()
    }

}