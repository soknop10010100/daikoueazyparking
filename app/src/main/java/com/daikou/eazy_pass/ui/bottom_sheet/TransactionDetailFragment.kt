package com.daikou.eazy_pass.ui.bottom_sheet

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.CustomTransactionModel
import com.daikou.eazy_pass.databinding.FragmentTransactionDetailBinding
import com.daikou.eazy_pass.helper.Helper
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class TransactionDetailFragment : BottomSheetDialogFragment() {
    private lateinit var binding : FragmentTransactionDetailBinding
    lateinit var customTransaction: CustomTransactionModel
    private lateinit var fContext: FragmentActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TopRoundCornerBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTransactionDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    companion object {
        @JvmStatic
        fun newInstance(transaction: CustomTransactionModel) =
            TransactionDetailFragment().apply {
                this.customTransaction = transaction
            }
    }

    private fun initView(){
        val createDate = customTransaction.createdAt ?: ""
        val dateString = Helper.formatDatFromDatetime(createDate, "yyyy-MM-dd HH:mm:ss",
            "EEEE, dd MMMM yyyy")
        binding.dateTrxDetailTv.text = dateString

        binding.toBankTrxDetailTv.text = customTransaction.payBy ?: "N/A"
        binding.typeTrxDetailTv.text = customTransaction.type ?: "---"
        binding.statusTrxDetailTv.text = customTransaction.walletTransactionStatus ?: "---"
        binding.labelTv.text = customTransaction.label?: "---"

        val amount = customTransaction.total ?: 0.0
        val amountColor =
            if (amount < 0 ) ContextCompat.getColor(fContext, R.color.red_300) else ContextCompat.getColor(fContext,
                R.color.green_600
            )
        binding.totalAmountTv.text = Helper.amountFormat("$", amount)
        binding.totalAmountTv.setTextColor(amountColor)
    }
}