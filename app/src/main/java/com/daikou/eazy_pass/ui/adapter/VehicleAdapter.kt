package com.daikou.eazy_pass.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.VehicleModel
import com.daikou.eazy_pass.databinding.VehicleSelectRowBinding

class VehicleAdapter : RecyclerView.Adapter<VehicleAdapter.ViewHolder>(){
    private lateinit var vehicleList : ArrayList<VehicleModel>
    var selectedItemName : String = ""
    lateinit var onSelectedVehicle : (vehicleType: String) -> Unit

    @SuppressLint("NotifyDataSetChanged")
    fun setVehicleList(vehicleList : ArrayList<VehicleModel>){
        this.vehicleList = vehicleList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val layoutBinding = VehicleSelectRowBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(layoutBinding)
    }

    override fun getItemCount(): Int {
        return vehicleList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vehicleModel = vehicleList[position]
        holder.bind(vehicleModel)
        holder.setSelectItem(selectedItemName == vehicleModel.name)
        holder.layoutBinding.cardVehicleCardView.setOnClickListener {
            selectedItemName = vehicleModel.name!!
            holder.setSelectItem(true)
            notifyDataSetChanged()
            onSelectedVehicle.invoke(selectedItemName)
        }
    }

    inner class ViewHolder(val layoutBinding : VehicleSelectRowBinding) : RecyclerView.ViewHolder(layoutBinding.root){
        fun bind(vehicleModel: VehicleModel){
            layoutBinding.vehicleNameTv.text = vehicleModel.name ?: ""
            Glide.with(layoutBinding.root.context).load(vehicleModel.image).into(layoutBinding.vehicleIconImg)
//            layoutBinding.vehicleIconImg.setImageResource(vehicleModel.image ?: -1)
        }

        fun setSelectItem(isSelect : Boolean){
            val context = layoutBinding.root.context
            layoutBinding.cardVehicleCardView.strokeColor = if (isSelect) ContextCompat.getColor(context, R.color.colorPrimary) else ContextCompat.getColor(context, R.color.white)
            layoutBinding.cardVehicleCardView.strokeWidth = if (isSelect) 2 else 0
            layoutBinding.cardVehicleCardView.alpha = if (isSelect) 1f else 0.6f
        }
    }

}