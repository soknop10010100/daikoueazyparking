package com.daikou.eazy_pass.ui.payment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.FragmentSelectBankBinding
import com.daikou.eazy_pass.helper.Helper

class SelectBankFragment : Fragment() {

    private lateinit var fContext: FragmentActivity
    private lateinit var binding: FragmentSelectBankBinding
    private lateinit var navController: NavController

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSelectBankBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        inAction()
    }

    private fun initView() {
        navController = Navigation.findNavController(requireView())
        if (Helper.getListBankJson(fContext).size < 1){
            binding.actionBackImg.visibility = View.GONE
        }else{
            binding.actionBackImg.visibility
        }
    }

    private fun inAction() {
        binding.actionAcledaRelative.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("bankType", "acl")
            navController.navigate(R.id.action_selectBankFragment_to_addABAAccountNumberFragment, bundle)
        }

        binding.actionAbaRelative.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("bankType", "aba")
            navController.navigate(R.id.action_selectBankFragment_to_addABAAccountNumberFragment, bundle)
        }

        binding.actionBackImg.setOnClickListener {
            navController.popBackStack()
        }

        binding.actionCloseImg.setOnClickListener {
            accessParentFragment()?.dismiss()
        }
    }

    private fun accessParentFragment() : PaymentMethodFragment?{
        if (fContext.supportFragmentManager.findFragmentByTag("PaymentMethodFragment") is PaymentMethodFragment) {
            return fContext.supportFragmentManager.findFragmentByTag("PaymentMethodFragment") as PaymentMethodFragment
        }
        return null
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            SelectBankFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}