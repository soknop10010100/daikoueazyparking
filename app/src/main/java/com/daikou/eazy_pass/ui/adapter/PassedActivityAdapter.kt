package com.daikou.eazy_pass.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.CustomPassedActivity
import com.daikou.eazy_pass.data.model.response_model.ActivityHistory
import com.daikou.eazy_pass.databinding.LayoutActivityPassedBinding
import com.daikou.eazy_pass.helper.Helper
import java.text.SimpleDateFormat

class PassedActivityAdapter (private val passActivityList : ArrayList<ActivityHistory>) : RecyclerView.Adapter<PassedActivityAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = LayoutActivityPassedBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return passActivityList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val passActivity = passActivityList[position]
        holder.bind(passActivity)
    }
    class ViewHolder(val layoutBinding: LayoutActivityPassedBinding) :
        RecyclerView.ViewHolder(layoutBinding.root) {

        @SuppressLint("SimpleDateFormat")
        fun bind(passActivity : ActivityHistory){
            layoutBinding.amountTv.visibility = if (passActivity.totalPrice != null) View.VISIBLE else View.GONE
            layoutBinding.unpaidTv.visibility = if (passActivity.totalPrice != null) View.GONE else View.VISIBLE
            layoutBinding.labelTv.text = if (passActivity.status == "exit") layoutBinding.root.context.getString(R.string.exit) else layoutBinding.root.context.getString(R.string.entrance)
            layoutBinding.durationTv.text = passActivity.duration?: ""

            if (passActivity.fromDate != null){
                val fromTime = Helper.formatTimeFromDate(passActivity.fromDate, "yyyy-MM-dd HH:mm:ss", "hh:mm:ss a")
                if (Helper.isToday(passActivity.fromDate, "yyyy-MM-dd hh:mm:ss")){
                    layoutBinding.fromDateTv.text = String.format("%s, %s", layoutBinding.root.context.getString(R.string.today), fromTime)
                }else if (Helper.isYesterday(passActivity.fromDate, "yyyy-MM-dd hh:mm:ss")) {
                    layoutBinding.fromDateTv.text = String.format("%s, %s", layoutBinding.root.context.getString(R.string.yesterday), fromTime)
                }else {
                    layoutBinding.fromDateTv.text = Helper.formatDatFromDatetime(passActivity.fromDate, "yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy, hh:mm:ss a")
                }
            }

            val toDate = if (passActivity.toDate == null) "" else Helper.formatTimeFromDate(passActivity.toDate, "yyyy-MM-dd hh:mm:ss", "hh:mm:ss a")
            if (passActivity.toDate != null){
                if (Helper.isToday(passActivity.toDate, "yyyy-MM-dd hh:mm:ss")){
                    layoutBinding.endDateTv.text = String.format("%s, %s", layoutBinding.root.context.getString(R.string.today), toDate)
                }else if (Helper.isYesterday(passActivity.toDate, "yyyy-MM-dd hh:mm:ss")) {
                    layoutBinding.endDateTv.text = String.format("%s, %s", layoutBinding.root.context.getString(R.string.yesterday), toDate)
                }else {
                    layoutBinding.endDateTv.text = Helper.formatDatFromDatetime(passActivity.toDate, "yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy, hh:mm:ss a")
                }
            }

            if (passActivity.paymentStatus == "unpaid"){
                layoutBinding.endDateTv.text = ""
                layoutBinding.durationTv.text = ""
            }
            val totalPrice = Helper.amountFormat("$", passActivity.totalPrice?: 0.0)
            layoutBinding.amountTv.text = String.format("%s %s",layoutBinding.root.context.getString(R.string.paid), totalPrice)
            layoutBinding.titleTv.text = passActivity.projectName ?: "None"
            //layoutBinding.headerDate.visibility = if (passActivity.isShowHeadDate) View.VISIBLE else View.GONE
            //layoutBinding.headerDate.text = Helper.formatDatFromDatetime(passActivity.entranceAt, "E,dd-MM-yyy h:mm:ss a", "dd MMM, yyyy")
//            layoutBinding.timeTv.text = Helper.formatTimeFromDate(passActivity.entranceAt, "E,dd-MM-yyy h:mm:ss a", "h:mm a")

        }
    }
}


