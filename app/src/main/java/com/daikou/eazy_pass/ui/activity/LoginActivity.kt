package com.daikou.eazy_pass.ui.activity

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.databinding.ActivityLoginBinding
import com.daikou.eazy_pass.enumerable.LanguageSetting
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.bottom_sheet.LanguageBottomSheetDialogFragment
import com.daikou.eazy_pass.util.LanguageManager
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.LoginViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class LoginActivity : BaseActivity<LoginViewModel>() {
    private lateinit var binding : ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initAction()
        doObserve()
    }

    private fun initAction() {
        binding.singUpTv.setOnClickListener {
            RedirectClass.gotoSignUpActivity(this)
        }

        binding.actionLoginMtb.setOnClickListener {
            when{
                TextUtils.isEmpty(binding.phoneNumberTf.text.toString()) ->{
                    MessageUtil.showError(self(), "", getString(R.string.please_enter_account_number))
                }
                TextUtils.isEmpty(binding.passwordTf.text.toString()) -> {
                    MessageUtil.showError(self(), "", getString(R.string.please_enter_password))
                }
                else ->{
                    val body = HashMap<String, Any>()
                    body["password"] = binding.passwordTf.text.toString()
                    body["phone"] = binding.phoneNumberTf.text.toString()
                    mViewModel.login(body)
                }
            }
        }

        binding.forgetPwdTv.setOnClickListener {
            RedirectClass.gotoForgetPasswordActivity(this)
        }

        binding.languageImg.setOnClickListener { languageImage ->
            LanguageBottomSheetDialogFragment.newInstance {
                when (it) {
                    LanguageSetting.English -> {
                        LanguageManager.setLanguage(self(), LanguageManager.ENGLISH)
                        binding.languageImg.setImageResource(R.drawable.uk_flag)
                        recreate()
                    }
                    LanguageSetting.Khmer -> {
                        LanguageManager.setLanguage(self(), LanguageManager.KHMER)
                        binding.languageImg.setImageResource(R.drawable.kh_flag)
                        recreate()
                    }
                }
            }.show(supportFragmentManager, "LanguageBottomSheet")
        }
    }

    private fun initView() {
        binding.languageImg.setImageResource(
            if (LanguageManager.getLanguage(self()) == LanguageManager.ENGLISH) R.drawable.uk_flag
            else R.drawable.kh_flag
        )
        binding.title.text = getString(R.string.login)

    }

    private fun doObserve(){
        mViewModel.loading.observe(this){
            binding.loadingView.root.visibility = if (it) View.VISIBLE else View.GONE
        }

        mViewModel.loginDataResponse.observe(this){ baseResponse ->
            if (baseResponse.success){
                baseResponse.data?.let {
                    val jsonObject = it.asJsonObject
                    val userJson = jsonObject["user"].asJsonObject
                    val accessToken = jsonObject["access_token"].asString
                    val gson  = Gson()
                    val typeToken = object : TypeToken<User>() {}.type
                    val user = gson.fromJson(userJson, User::class.java)
                    Helper.AuthHelper.saveUserSharePreference(self(), user)
                    Helper.AuthHelper.saveAccessToken(self(),accessToken)
                    Helper.AuthHelper.savePassword(self(), binding.passwordTf.text.toString())

                    RedirectClass.gotoMainActivity(this@LoginActivity)
                    finishAffinity()
                }
            }else{
                val errorJsonObj = baseResponse.errors
                errorJsonObj?.let {
                    if (it.has("password")){
                        val message = it["password"].asString
                        MessageUtil.showError(self(), "", message)
                    }
                }
            }
        }
        mViewModel.errorMessage.observe(this){
            MessageUtil.showError(self(),"", it)
        }
    }
}