package com.daikou.eazy_pass.ui.payment

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.enumerable.PaymentOption
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson

class PaymentMethodFragment : BottomSheetDialogFragment() {

    private lateinit var fContext: FragmentActivity
    private lateinit var navController: NavController
    private lateinit var actionPaymentOption: PaymentOption
    private var savedBankList: ArrayList<SaveBankAccount>? = null
    private lateinit var saveBankAccount: SaveBankAccount
    private var idTickBankAccount: Int? = null
    lateinit var saveListBankAccountListener : (Boolean , SaveBankAccount) -> Unit

    companion object {
        @JvmStatic
        fun newInstance(
            saveBankAccount: SaveBankAccount,
            actionPaymentOption: PaymentOption,
            savedBankList: ArrayList<SaveBankAccount>? = null,
            idTickBankAccount: Int,
            saveAbaAccountListsListener: (Boolean, SaveBankAccount) -> Unit,
        ) =
            PaymentMethodFragment().apply {
                this.saveListBankAccountListener = saveAbaAccountListsListener
                this.actionPaymentOption = actionPaymentOption
                this.savedBankList = savedBankList
                this.saveBankAccount = saveBankAccount
                this.idTickBankAccount = idTickBankAccount
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_payment_method, container, false)
        if(dialog != null){
            dialog!!.setOnShowListener { dialogListener : DialogInterface ->
                val dialog = dialogListener as BottomSheetDialog
                val bottomSheetInternal = dialog.findViewById<View>(R.id.design_bottom_sheet)
                if(bottomSheetInternal != null){
                    BottomSheetBehavior.from(bottomSheetInternal).state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAction()
    }
    private fun initView() {
        val navHostFragment = childFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment?
        navHostFragment?.let {
            navController = it.navController
            when(actionPaymentOption){
                PaymentOption.AddNewCard ->{
                    navController.setGraph(R.navigation.payment_method_nav_graph)
                    val graph = navController.graph
                    graph.setStartDestination(R.id.selectBankFragment)
                }
                PaymentOption.ChangeCard ->{
                    savedBankList?.let { saveBanks ->
                        val bundle = Bundle()
                        bundle.putSerializable(
                            ChangeCardAndBankAccountFragment.SaveBankAccountKey,
                            saveBanks
                        )
                        val gson = Gson()
                        val bankAccount = gson.toJson(saveBankAccount)
                        bundle.putString(
                            ChangeCardAndBankAccountFragment.firstSaveBankAccountKey,
                            bankAccount
                        )
                        bundle.putInt(ChangeCardAndBankAccountFragment.idTickBankAccountKey,
                            idTickBankAccount ?: -1)
                        navController.setGraph(R.navigation.payment_method_save_nav_graph, bundle)
                        val graph = navController.graph
                        graph.setStartDestination(R.id.changeCardAndBankAccountFragment)
                    }
                }
            }
        }
    }

    private fun initAction() {

    }


}