package com.daikou.eazy_pass.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.callback.BiometricListener
import com.daikou.eazy_pass.databinding.ActivityVerificationBinding
import com.daikou.eazy_pass.enumerable.VerifyPinEnum
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BiometricClass
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.SignUpViewModel
import com.mukeshsolanki.OnOtpCompletionListener

class VerificationActivity : BaseActivity<SignUpViewModel>() {
    private lateinit var binding: ActivityVerificationBinding
    private var caller : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initAction()
    }

    companion object{
        const val changePasswordTitle = "Change Password"
        const val hasBiometricKey = "hasBiometricKey"
    }

    private fun initView() {
        if(intent.action != null){
            caller = intent.action!!
            when {
                caller == Constants.CHANG_PASSWORD_CALLER -> {
                    binding.titleAppBarTv.text = changePasswordTitle
                    binding.loginTv.visibility = View.GONE
                }
                caller == Constants.SPLASH_SCREEN ->{

                }
            }
        }
        if(intent.hasExtra(hasBiometricKey) && intent.getBooleanExtra(hasBiometricKey, false)){
            getBiometric()?.let {
                if (it == "1"){
                    BiometricClass.newInstance(self(), faceScanListener)
                }
            }
        }
    }
    private val faceScanListener = object  : BiometricListener{
        override fun onSuccess() {
            RedirectClass.gotoMainActivity(self())
            finish()
        }

        override fun onFailed(error: CharSequence) {
            TODO("Not yet implemented")
        }

    }

    private fun initAction() {
        val password = Helper.AuthHelper.getPassword(self())
        binding.otpView.setOtpCompletionListener { code ->
            if(code.isNotEmpty()){
                if (code == password){
                    if (caller == Constants.CHANG_PASSWORD_CALLER) {
                        RedirectClass.gotoChangePasswordActivity(self(), code, "", VerifyPinEnum.VerificationPinScreen)
                        finish()
                    }else if(caller == Constants.SPLASH_SCREEN ){
                        RedirectClass.gotoMainActivity(self())
                        finish()
                    }
                } else {
                    MessageUtil.showError(self(), "", getString(R.string.incorrrect_password))
                    binding.otpView.setText("")
                }
            }
        }

        binding.lockImgBtn.setOnClickListener {
            binding.otpView.setText(password)
        }
        binding.actionForgotPin.setOnClickListener {
            RedirectClass.gotoForgetPasswordActivity(self());
        }

        binding.loginTv.setOnClickListener {
            RedirectClass.gotoLoginActivity(self())
            finish()
        }

        binding.numericKeyboard.keySpecialListener = View.OnClickListener { v: View? ->
            binding.numericKeyboard.field?.postDelayed(
                Runnable {
                    binding.numericKeyboard.let {
                        it.field?.let { editText ->
                            editText.setText("")
                        }
                    }
                }, 300
            )
        }
    }
}