package com.daikou.eazy_pass.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.ActivityIndentityVerifyBinding
import com.daikou.eazy_pass.util.Constants
import java.util.HashMap

class IdentityVerifyActivity : AppCompatActivity() {
    private lateinit var binding :ActivityIndentityVerifyBinding

    companion object {
        const val KycDataKey = "kyc_data_key"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIndentityVerifyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        this.setSupportActionBar(binding.appBarLayout.toolbar)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.appBarLayout.title.setText(R.string.identity_verification)

        initCurrentKycStatusUser()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    private fun initCurrentKycStatusUser() {
        if (intent.hasExtra(KycDataKey)) {
            val kycHm: HashMap<String, Any> =
                intent.getSerializableExtra(KycDataKey) as HashMap<String, Any>

            val refusedMsg = kycHm["refused_msg"].toString()
            val verifiedStatus = kycHm["status"].toString() == Constants.KycDocStatus.Verified
            val pendingStatus = kycHm["status"].toString() == Constants.KycDocStatus.Pending
            val refusedStatus = kycHm["status"].toString() == Constants.KycDocStatus.Refused

            if (verifiedStatus) {
                binding.kycImg.setBackgroundResource(
                    R.drawable.smart_done_icon
                )
                binding.kycTextTv.text = getString(R.string.kyc_verified)
                binding.actionReSubmitAgain.visibility = View.GONE
            } else if (pendingStatus) {
                binding.kycImg.setImageResource(
                    R.drawable.watting_icon
                )
                binding.kycTextTv.text = getString(R.string.kyc_pending)
                binding.actionReSubmitAgain.visibility = View.GONE
            } else if (refusedStatus) {
                binding.kycImg.setBackgroundResource(
                    R.drawable.close_img
                )
                binding.kycTextTv.text = refusedMsg.ifEmpty { "KYC Refused." }
                binding.actionReSubmitAgain.visibility = View.VISIBLE
            } else {
                binding.kycImg.visibility = View.GONE
                binding.kycTextTv.visibility = View.GONE
                binding.actionReSubmitAgain.visibility = View.GONE
            }

        }
    }
}