package com.daikou.eazy_pass.ui.bluetooth_scan

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.bluetooth.BluetoothDevice
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.ActivityDoScanBluetoothBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.sdk.ParkingController
import com.daikou.eazy_pass.ui.MainActivity
import com.daikou.eazy_pass.ui.home.HomeFragment
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.view_model.PaymentViewModel
import java.util.*


class DoScanBluetoothActivity : BaseActivity<PaymentViewModel>() {

    private var notificationId = 1
    private lateinit var binding : ActivityDoScanBluetoothBinding
    private lateinit var parkingController: ParkingController
    private lateinit var homeFragment: HomeFragment
    private lateinit var countDownTimer : CountDownTimer
    private var mBluetoothDevice : BluetoothDevice? = null
    private var isSuccessSever = false
    private var mServiceUuid = ""
    private var mCharUuid = ""


    companion object{
        const val bluetoothDeviceId = "bluetoothDeviceIdKey"
        const val errorConnectingKey = "errorConnectingKey"
        const val errorFromServerKey = "errorFromServerKey"
    }

    @RequiresApi(Build.VERSION_CODES.S)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDoScanBluetoothBinding.inflate(layoutInflater)
        setContentView(binding.root)

        parkingController = ParkingController()
        homeFragment = HomeFragment()
        initView()
        initAction()
        connectToParking()
        doObserve()
    }

    fun initView(){
        binding.bluetoothScanAnimationView.playAnimation()

        binding.cancelScanningBtn.visibility = View.VISIBLE
        binding.linearLayoutRetry.visibility = View.GONE
        countDownTimer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val second = millisUntilFinished / 1000
                binding.cancelScanningBtn.text = String.format("%s (%s)", getString(R.string.action_cancel),second.toString() )
            }
            @RequiresApi(Build.VERSION_CODES.S)
            override fun onFinish() {
                parkingController.cancelScanning()
                parkingController.disconnectFromDevice()
                binding.bluetoothScanAnimationView.cancelAnimation()
                countDownTimer.cancel()
                binding.cancelScanningBtn.visibility = View.GONE
                binding.linearLayoutRetry.visibility = View.VISIBLE
                //finish()
            }
        }.start()
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun initAction() {
        binding.cancelScanningBtn.setOnClickListener {
            parkingController.cancelScanning()
            parkingController.disconnectFromDevice()
            finish()
        }
        binding.actionCancel.setOnClickListener {
            finish()
        }
        binding.actionRetry.setOnClickListener {
            initView()
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun connectToParking(){
            parkingController.connectToParking(this, object : ParkingController.ParkingCallBack{
                @SuppressLint("MissingPermission")
                override fun onConnect(bluetoothDevice: BluetoothDevice) {
                   // parkingController.openParking(bluetoothDevice)
                }

                override fun onError(message: String?) {
                    updateUi(true, message)
                }

                override fun onParkingOpen(message: String?, name: String?, serviceUuid: String?, charUuid: String?,bluetoothDevice: BluetoothDevice) {
                    updateUi()
                    doneScan(bluetoothDevice)
                    mServiceUuid = serviceUuid!!
                    mCharUuid = charUuid!!
                }
            })
    }

    @SuppressLint("MissingPermission")
    private fun doneScan(bluetoothDevice: BluetoothDevice){
        val body = HashMap<String, Any>()
        mBluetoothDevice = bluetoothDevice
        body["barrier_id"] = mBluetoothDevice!!.name

        if(hasInternetConnection()){
            mViewModel.openGateDoPayment(body)
        }else{
            var content = ""
            isSuccessSever = true
            val userParkingStatus = Helper.ParkingInfo.getParkingStatus(self())
            if(userParkingStatus == Constants.exit){
                content = "You entered successfully. Thank you for using EZ Pass!"
                Helper.ParkingInfo.saveParkingStatus(self(), Constants.entrance) // save status
                val date = Date()
                Helper.ParkingInfo.storeDataNotPay(self(), bluetoothDevice.name, Constants.entrance, date) // save data when entrance
            }else {
                content = "We will debit from you wallet later. Thank you for using EZ Pass!"
                Helper.ParkingInfo.saveParkingStatus(self(), Constants.exit) // save status
                val date = Date()
                Helper.ParkingInfo.storeDataNotPay(self(), bluetoothDevice.name, Constants.exit, date) // save data when exit
            }
            alertNotification(getString(R.string.no_internet_connection),content)
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun doObserve(){
        mViewModel.dataPaymentResponse.observe(this){
            if (it.success){
                if (mServiceUuid.isEmpty()) {
                    isSuccessSever = true
                } else {
                    isSuccessSever = false
                    parkingController.openLastParking(mServiceUuid, mCharUuid)
                }
                val dataRespond = it.data
                dataRespond?.let {
                    if (dataRespond.total != null){
                        val paidString = "was debited from your wallet. Thank you for using EZ Pass!"
                        val contentText = String.format("$%.2f USD %s", dataRespond.total, paidString)
                        alertNotification(getString(R.string.app_name),contentText)
                    }else{
                        val thank = "Thank you for using EZ Pass!"
                        val contentText = String.format("%s %s", dataRespond.message, thank)
                        alertNotification(getString(R.string.app_name),contentText)
                    }
                }
                if (Helper.ParkingInfo.getParkingStatus(self()) == Constants.exit) {
                    Helper.ParkingInfo.saveParkingStatus(self(), Constants.entrance)
                } else if (Helper.ParkingInfo.getParkingStatus(self()) == Constants.entrance) {
                    Helper.ParkingInfo.saveParkingStatus(self(), Constants.exit)
                }
            }
        }

        mViewModel.errorMessage.observe(this ){
            val intent = Intent()
            intent.putExtra(errorFromServerKey, it)
            setResult(Activity.RESULT_OK, intent)
            parkingController.cancelScanning()
            parkingController.disconnectFromDevice()
            finish()
        }
    }

    private fun alertNotification(title : String, contentText: String) {
        val CHANNEL_ID = "eazy_pass_notification"
        val description = "checkout_notification"

        val intent = Intent(self(), MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.action = MainActivity.notification
        val pendingIntent =
            PendingIntent.getActivity(self(), 0, intent, PendingIntent.FLAG_IMMUTABLE)

        val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val builder: NotificationCompat.Builder
        val importance = NotificationManager.IMPORTANCE_HIGH
        val sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.packageName + "/raw/notification_sound.mp3")
        builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(CHANNEL_ID, description, importance)
            notificationChannel.enableVibration(true)
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            notificationChannel.setSound(sound, audioAttributes)
            notificationManager.createNotificationChannel(notificationChannel)
            NotificationCompat.Builder(this, CHANNEL_ID)
        } else {
            NotificationCompat.Builder(this)
            //builder.setSound(sound);
        }
        builder.setContentTitle(title)
        builder.setSmallIcon(R.drawable.ez_app_logo)
        builder.setContentText(contentText)
        builder.setSound(sound, AudioManager.STREAM_NOTIFICATION)
        builder.setContentIntent(pendingIntent)
        builder.setStyle(
            NotificationCompat.BigTextStyle().setBigContentTitle("EZ Parking").bigText(contentText)
        )
        notificationManager.notify(notificationId, builder.build())
        notificationId += notificationId
    }

    private fun updateUi(isError: Boolean? = null, massage : String? = null){
        countDownTimer.cancel()
        binding.bluetoothScanAnimationView.visibility = View.GONE
        binding.textDescription.text = if(isError == true) massage else getString(R.string.description_bluetooth_done_scan)
        binding.bluetoothScanAnimationView.cancelAnimation()
        binding.scanSuccessAnimationView.visibility = View.VISIBLE
        binding.scanSuccessAnimationView.setAnimation(if (isError == true) R.raw.error_animation else R.raw.success_animation)
        binding.scanSuccessAnimationView.playAnimation()
        countDownTimer = object : CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val second = millisUntilFinished / 1000
                binding.cancelScanningBtn.text = String.format("%s (%s)", getString(R.string.ok),second.toString() )
            }
            @RequiresApi(Build.VERSION_CODES.S)
            override fun onFinish() {
                parkingController.cancelScanning()
                parkingController.disconnectFromDevice()
                countDownTimer.cancel()
                finish()
            }
        }.start()
    }

    @RequiresApi(Build.VERSION_CODES.S)
    override fun onDestroy() {
        super.onDestroy()
        parkingController.cancelScanning()
        parkingController.disconnectFromDevice()
    }
}