package com.daikou.eazy_pass.ui.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.SliderModel
import com.makeramen.roundedimageview.RoundedImageView
import java.util.Collections.addAll

class SliderAdapter(private val sliderItemList : MutableList<SliderModel>, val viewPager: ViewPager2) : RecyclerView.Adapter<SliderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_slider_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sliderItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(sliderItemList[position])
        if(position == sliderItemList.size -2){
            viewPager.post(runnable)
        }
    }

    inner  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var imageSliderItem : ImageView
        fun bind(sliderModel: SliderModel){
            imageSliderItem = itemView.findViewById(R.id.slideImageItem)
//            imageSliderItem.setImageResource(sliderModel.url)
            imageSliderItem.setImageResource(sliderModel.url)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private val runnable = Runnable {
        sliderItemList.addAll(sliderItemList)
        notifyDataSetChanged()
    }
}