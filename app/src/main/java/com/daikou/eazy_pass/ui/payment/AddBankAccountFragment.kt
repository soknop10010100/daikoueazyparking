package com.daikou.eazy_pass.ui.payment

import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.databinding.FragmentAddBankAccountBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseFragment
import com.daikou.eazy_pass.view_model.PaymentViewModel
import java.util.Random

class AddBankAccountFragment : BaseFragment<PaymentViewModel>() {
    private lateinit var binding: FragmentAddBankAccountBinding
    private lateinit var navController: NavController
    private var bankType: String? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            bankType = it.getString("bankType")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddBankAccountBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAction()
        doObserve()
    }

    private fun initView() {
        navController = Navigation.findNavController(requireView())
        if (bankType != null){
            binding.bankLogo.setImageResource(if (bankType == "acl") R.drawable.acleda_logo else R.drawable.aba_pay_icon)
            binding.bankName.setText(if (bankType == "acl") R.string.acleda_bank_plc else R.string.aba_pay)
            ediTextFormatBankAccount(bankType!!)
        }
    }
    private fun initAction(){
        binding.actionBackImg.setOnClickListener {
            navController.popBackStack()
        }

        binding.actionConfirmMtb.setOnClickListener {
            val accountNumber = binding.accountNumberTfEditext.text ?: ""
            if (accountNumber.isEmpty()){
                binding.accountNumberTfLayout.error = getString(R.string.please_enter_account_number)
            }else{
                val saveBankAccount : SaveBankAccount
                val bankAccountNumber = accountNumber.replace("\\s+".toRegex(), "")
                val body = HashMap<String, Any>()
                body["bank_account_number"] = bankAccountNumber
                if (bankType == "acl"){
                    body["bic"] = Constants.BankBic.bicACL
                }else{
                    body["bic"] = Constants.BankBic.bicABA
                }
                mViewModel.verifyBackAccount(body)
            }
        }
    }

    private fun doObserve(){
        mViewModel.loading.observe(fContext){
            binding.loadingView.root.visibility = if (it) View.VISIBLE else View.GONE
        }

        mViewModel.bankAccountResponse.observe(fContext){
            if (it.success){
                val saveBankAccount = it.data
                saveBankAccount?.let {saveAccount ->
                        //Random().nextInt(10000)
                    if (saveAccount.accountNumber?.length == 9) {
                        saveAccount.logo = R.drawable.aba_pay_icon
                        saveAccount.bic = Constants.BankBic.bicABA
                    }else{
                        saveAccount.logo = R.drawable.acleda_logo
                        saveAccount.bic = Constants.BankBic.bicACL
                    }
                    saveAccount.accountNumberFormat = binding.accountNumberTfEditext.text.toString()
                    Helper.saveListBank(fContext, saveBankAccount)
                    accessParentFragment()?.saveListBankAccountListener?.invoke(true, saveBankAccount)
                    accessParentFragment()?.dismiss()
                }
            }
        }
        mViewModel.errorMessage.observe(fContext){
            MessageUtil.showError(fContext, null, it)
        }

    }
    private fun accessParentFragment() : PaymentMethodFragment?{
        if (fContext.supportFragmentManager.findFragmentByTag("PaymentMethodFragment") is PaymentMethodFragment) {
            return fContext.supportFragmentManager.findFragmentByTag("PaymentMethodFragment") as PaymentMethodFragment
        }
        return null
    }

    private fun ediTextFormatBankAccount(bankType: String) {
        if (bankType == "acl") {
            binding.accountNumberTfEditext.groupLength = 4
            binding.accountNumberTfEditext.groupSeparator = ' '
            binding.accountNumberTfEditext.inputLength = 19
        } else {
            binding.accountNumberTfEditext.groupLength = 3
            binding.accountNumberTfEditext.groupSeparator = ' '
            binding.accountNumberTfEditext.inputLength = 11
        }
    }
}