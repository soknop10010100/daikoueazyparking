package com.daikou.eazy_pass.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.result.ActivityResult
import androidx.core.widget.doOnTextChanged
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.callback.SelectDateListener
import com.daikou.eazy_pass.databinding.ActivitySignUpBinding
import com.daikou.eazy_pass.helper.BiometricSecurity
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.bottom_sheet.DateBottomSheetDialogFragment
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.LanguageManager
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.SignUpViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.properties.Delegates

class SignUpActivity : BaseActivity<SignUpViewModel>() {


    private lateinit var binding : ActivitySignUpBinding
    private var selectGenderIndex : Int? = null
    private lateinit var genderValue : IntArray
    private var dob : Date? = null
    private var kycTemp : HashMap<String, Any> = HashMap()

    companion object{
        const val SIGNUP_BODY_KEY = "sign_up_body_key"
        const val KYC_TEMP_KEY = "kyc_temp_key"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        initAction()
        doObserve()
        validateInputField()
    }

    private fun initView() {
        if(LanguageManager.getLanguage(self()) == LanguageManager.KHMER){
            val genderLabel = resources.getStringArray(R.array.gender_label_km)
            val genderDropDownAdapter = ArrayAdapter(this,R.layout.gender_dropdown_item ,genderLabel)
            binding.genderAutoCompleteTextView.setAdapter(genderDropDownAdapter)
        }else{
            val genderLabel = resources.getStringArray(R.array.gender_label)
            val genderDropDownAdapter = ArrayAdapter(this,R.layout.gender_dropdown_item ,genderLabel)
            binding.genderAutoCompleteTextView.setAdapter(genderDropDownAdapter)
        }
        genderValue = resources.getIntArray(R.array.gender_value)
        // initialize the gender value for the api request (0 male , 1 female)
    }

    private fun doObserve(){
        mViewModel.signUpStateValidate.observe(this) { signupState ->
            if (signupState.hasDoneValidate == true){
                // create body
                val body = HashMap<String , Any>()
                val dobString = Helper.formatDate(dob, "yyyy-MM-dd") ?: ""
                body["birthday"] = dobString
                body["email"] = ""
                body["first_name"] = binding.firstnameTextInputEditText.text.toString()
                body["gender"] = if (selectGenderIndex != null) genderValue[selectGenderIndex!!] else ""
                body["last_name"] = binding.lastnameTextInputEditText.text.toString()
                body["phone"] = binding.phoneNumberTextInputEditText.text.toString()
                body["password"] = binding.passwordTextInputEditText.text.toString()
//                body["hasBiometric"] = if (binding.enableFingerFaceIdSwitchMaterial.isChecked) "1" else "0"
                body["term"] = 1
                RedirectClass.gotoUploadDocument(self(), body, kycTemp, object : BetterActivityResult.OnActivityResult<ActivityResult>{
                    override fun onActivityResult(result: ActivityResult) {
                        if(result.resultCode == Activity.RESULT_OK) {
                            result.data?.let {
                                if (it.hasExtra(KYC_TEMP_KEY)) {
                                    kycTemp =
                                        it.getSerializableExtra(KYC_TEMP_KEY) as HashMap<String, Any>
                                }
                            }
                        }
                    }
                })
            }else {
                    if (TextUtils.isEmpty(binding.firstnameTextInputEditText.text.toString())) {
                        Helper.validateTextInputField(
                            binding.firstnameTextInputEditText.text.toString(),
                            binding.firstnameTextInputLayout,
                            getString(R.string.field_is_required)
                        )
                    } else if (TextUtils.isEmpty(binding.lastnameTextInputEditText.text.toString())) {
                        Helper.validateTextInputField(
                            binding.lastnameTextInputEditText.text.toString(),
                            binding.lastnameTextInputLayout,
                            getString(R.string.field_is_required)
                        )

                    }
//                    else if (TextUtils.isEmpty(binding.genderAutoCompleteTextView.text.toString())) {
//                        Helper.validateTextInputField(
//                            binding.genderAutoCompleteTextView.text.toString(),
//                            binding.genderTextInputLayout,
//                            getString(R.string.field_is_required)
//                        )
//                    }
//                    else if (TextUtils.isEmpty(binding.birthdayTextInputEditText.text.toString())) {
//                        validateField1(
//                            binding.birthdayTextInputLayout,
//                            binding.birthdayTextInputEditText.text.toString(),
//                            getString(R.string.field_is_required)
//                        )
//                    }
                    /*else if (TextUtils.isEmpty(binding.emailTextInputEditText.text.toString())) {
                        validateField1(
                            binding.emailTextInputLayout,
                            binding.emailTextInputEditText.text.toString(),
                            getString(R.string.field_is_required)
                        )
                    }*/ else if (TextUtils.isEmpty(binding.phoneNumberTextInputEditText.text.toString())) {
                        Helper.validateTextInputField(
                            binding.phoneNumberTextInputEditText.text.toString(),
                            binding.phoneNumberTextInputLayout,
                            getString(R.string.field_is_required)
                        )
                    } else if (TextUtils.isEmpty(binding.passwordTextInputEditText.text.toString())) {
                        Helper.validateTextInputField(
                            binding.passwordTextInputEditText.text.toString(),
                            binding.passwordTextInputLayout,
                            getString(R.string.field_is_required)
                        )
                    } else if (TextUtils.isEmpty(binding.confirmPasswordTextInputEditText.text.toString())) {
                        Helper.validateTextInputField(
                            binding.confirmPasswordTextInputEditText.text.toString(),
                            binding.confirmPasswordTextInputLayout,
                            getString(R.string.field_is_required)
                        )
                    }
                MessageUtil.showError(this, "", signupState.firstName?.let { getString(it) })
            }
        }

//        mViewModel.user.observe(this){
//            print(it)
//        }

    }

    private fun initAction() {
        binding.actionBackImg.setOnClickListener {
            finish()
        }

        binding.actionNextMtb.setOnClickListener {
            onValidateInformationUser()
        }

        binding.genderAutoCompleteTextView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            selectGenderIndex = position
        }

        binding.birthdayTextInputEditText.setOnClickListener {
            val date = Date()
            DateBottomSheetDialogFragment.newInstance("", date, object : SelectDateListener{
                override fun selectDateListener(date: Date) {
                    formatDateForUI(date) // set date to text input edit text
                }

            }).show(supportFragmentManager, "DateDialogBottomSheet")
        }

        binding.enableFingerFaceIdSwitchMaterial.setOnClickListener {
            val hasBiometric = BiometricSecurity.checkBiometric(self())
            if(!hasBiometric){
                binding.enableFingerFaceIdSwitchMaterial.isChecked = false
            }
        }
    }

    private fun validateInputField() {

        binding.firstnameTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(
                    it,
                    binding.firstnameTextInputLayout,
                    getString(R.string.field_is_required)
                )
            }
        }
        binding.lastnameTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(
                    it,
                    binding.lastnameTextInputLayout,
                    getString(R.string.field_is_required)
                )
            }
        }
//        binding.genderAutoCompleteTextView.doOnTextChanged { text, start, before, count ->
//            text?.let {
//                Helper.validateTextInputField(
//                    it,
//                    binding.genderTextInputLayout,
//                    getString(R.string.field_is_required)
//                )
//            }
//        }
//        binding.birthdayTextInputEditText.doOnTextChanged { text, start, before, count ->
//            text?.let {
//                Helper.validateTextInputField(
//                    it,
//                    binding.birthdayTextInputLayout,
//                    getString(R.string.field_is_required)
//                )
//            }
//        }
        binding.phoneNumberTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(
                    it,
                    binding.phoneNumberTextInputLayout,
                    getString(R.string.field_is_required)
                )
            }
        }
        binding.passwordTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(
                    it,
                    binding.passwordTextInputLayout,
                    getString(R.string.field_is_required)
                )
            }
        }
        binding.confirmPasswordTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(
                    it,
                    binding.confirmPasswordTextInputLayout,
                    getString(R.string.field_is_required)
                )
            }
        }

    }

    private fun onValidateInformationUser() {
        val firstName = binding.firstnameTextInputEditText.text.toString()
        val lastName = binding.lastnameTextInputEditText.text.toString()
        val phoneNumber = binding.phoneNumberTextInputEditText.text.toString()
        val password = binding.passwordTextInputEditText.text.toString()
        val confirmPassword = binding.confirmPasswordTextInputEditText.text.toString()
        mViewModel.validateSignUpUserField(
            firstName,
            lastName,
            phoneNumber,
            password,
            confirmPassword
        )
    }
    @SuppressLint("SimpleDateFormat")
    private fun formatDateForUI(date: Date){
        this.dob = date
        val dateFm = SimpleDateFormat("dd/MMMM/yyyy")
        val dateString = dateFm.format(date)
        binding.birthdayTextInputEditText.setText(String.format(Locale.US, "%s", dateString))
    }
}