package com.daikou.eazy_pass.ui.wallet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import androidx.activity.result.ActivityResult
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.databinding.ActivityDepositBinding
import com.daikou.eazy_pass.enumerable.PaymentOption
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.payment.PaymentMethodFragment
import com.daikou.eazy_pass.util.AutoFillNumberRecyclerView
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseListenerAutoFillNumber
import com.daikou.eazy_pass.view_model.DepositViewModel
import com.daikou.eazy_pass.ui.webpay.WebPayActivity
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.base.BaseEdittextListener

class DepositActivity : BaseActivity<DepositViewModel>() {
    private lateinit var binding : ActivityDepositBinding
    private var isShow : Boolean = false
    private var isHasBank: Boolean = false
    private var isSelectedAmount: Boolean = false
    private lateinit var savedListBank : ArrayList<SaveBankAccount>
    private lateinit var saveBankAccount : SaveBankAccount
    private var mAmount : Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDepositBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //initAppbar()
        initView()

        initAction()

        doObserve()
    }

    fun initView(){
        saveBankAccount = Helper.getSelectedBank(self())
        savedListBank = Helper.getListBankJson(self())
        if (savedListBank.size >0){
            binding.actionAddCardOtherBankTv.visibility = View.GONE
            binding.actionChangeCardBankAccountTv.visibility = View.VISIBLE
        }else{
            binding.actionAddCardOtherBankTv.visibility = View.VISIBLE
            binding.actionChangeCardBankAccountTv.visibility = View.GONE
        }

        setAmount(0.0)
    }

    private fun initAction() {
        binding.autoFillNumberRecyclerView.setBaseListener(numberClickListener)

        binding.actionBackImage.setOnClickListener {
            finish()
        }
        binding.hideImg.setOnClickListener {
            isShow = !isShow
            if(isShow){
                binding.hideImg.setImageResource(R.drawable.eye_ic)
                binding.hideAmountTv.visibility = View.GONE
                binding.amountTv.visibility = View.VISIBLE
            }else{
                binding.hideImg.setImageResource(R.drawable.visibility_off_ic)
                binding.hideAmountTv.visibility = View.VISIBLE
                binding.amountTv.visibility = View.GONE
            }
        }
        binding.actionAddCardOtherBankTv.setOnClickListener {
            PaymentMethodFragment.newInstance(
                saveBankAccount = SaveBankAccount(),
                actionPaymentOption = PaymentOption.AddNewCard,
                savedBankList = null,
                idTickBankAccount = -1
            ){ _, savedBank ->
               this.saveBankAccount = savedBank // get save bank after added
                initBankViewInUi(true)
            }.apply {
                show(supportFragmentManager, "PaymentMethodFragment")
            }
        }

        binding.actionChangeCardBankAccountTv.setOnClickListener {
            PaymentMethodFragment.newInstance(
                saveBankAccount = saveBankAccount,
                actionPaymentOption = PaymentOption.ChangeCard,
                savedBankList = savedListBank,
                idTickBankAccount = saveBankAccount.userId,
            ){ hasChang , savedBank->
                this.saveBankAccount = savedBank
                initBankViewInUi(hasChang)
            }.apply {
                show(supportFragmentManager, "PaymentMethodFragment")
            }
        }

        binding.depositBtn.setOnClickListener {
            val body  = HashMap<String, Any>()
            val amountString = binding.amountEdt.text.toString()
            body["remark"] = binding.commentEdt.text.toString()
            body["total_amount"] = amountString.toDouble()

            if (!TextUtils.isEmpty(amountString)) {
                mAmount = amountString.toDouble()
            }

            mViewModel.deposit(body)
        }

    }

    private fun doObserve(){
        mViewModel.depositResponseData.observe(this){
            if(it.success){
                it.data?.let { depositResponse->
                    val intent = Intent(self(), WebPayActivity::class.java)
                    intent.putExtra("linkUrl",depositResponse.paymentLink)
                    gotoActivity(this,intent)
                    gotoActivityForResult(this, intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
                        override fun onActivityResult(result: ActivityResult) {
                            if (result.resultCode == Activity.RESULT_OK) {
                                val data = result.data
                                if (data != null && data.hasExtra("status")){
                                    val mStatus = data.getStringExtra("status")
                                    if(mStatus.equals("success=1", true)) {
                                        setAmount(mAmount)
                                        finish()
                                    }
                                }
                            }
                        }
                    })
                }
            }
        }

        mViewModel.onLoading.observe(this){
            binding.loadingView.root.visibility = if (it) View.VISIBLE else View.GONE
        }

        mViewModel.errorMessage.observe(this){
            MessageUtil.showError(self(), "", it)
        }

        binding.amountEdt.addTextChangedListener(object : BaseEdittextListener() {
            override fun afterTextChangedNotEmpty(editable: Editable) {
                isSelectedAmount = true
                enableDepositBtn()
            }

            override fun afterTextChangedIsEmpty() {
                isSelectedAmount = false
                enableDepositBtn()
            }

        })
    }

    private fun setAmount(totalBalance: Double) {
        val user = Helper.AuthHelper.getUserSharePreference(self())
        val mTotalAmount = (user.totalBalance ?: 0.0) + totalBalance
        binding.amountTv.text = Helper.amountFormat("$", mTotalAmount)
    }

    private val numberClickListener : BaseListenerAutoFillNumber<AutoFillNumberRecyclerView.Item?> = object :
        BaseListenerAutoFillNumber<AutoFillNumberRecyclerView.Item?> {
        override fun onResult(data: AutoFillNumberRecyclerView.Item?) {
            if(data != null) {
                binding.amountEdt.setText(String.format("%s", data.numberStr))
                val amountDoubleString = binding.amountEdt.text.toString()
                val amountDouble = amountDoubleString.toDoubleOrNull()
                if(amountDouble != null && amountDouble > 0){
                    isSelectedAmount = true
                    enableDepositBtn()
                }
            }
        }
    }
    fun enableDepositBtn(){
        binding.depositBtn.isEnabled = isSelectedAmount
    }

    private fun initBankViewInUi(isViewChange : Boolean){
        savedListBank = Helper.getListBankJson(context = self())
        if(isViewChange){
            if(savedListBank.size > 0){
                isHasBank = true
                enableDepositBtn()
                binding.actionAddCardOtherBankTv.visibility = View.GONE
                binding.actionChangeCardBankAccountTv.visibility = View.VISIBLE

            }else{
                isHasBank = false
                binding.actionAddCardOtherBankTv.visibility = View.VISIBLE
                binding.actionChangeCardBankAccountTv.visibility = View.GONE
            }
        }
    }

//    private fun initAppbar() {
//        this.setSupportActionBar(binding.appBarLayout.toolbar)
//        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
//        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//
//        binding.appBarLayout.title.text = getString(R.string.withdraw)
//    }
}