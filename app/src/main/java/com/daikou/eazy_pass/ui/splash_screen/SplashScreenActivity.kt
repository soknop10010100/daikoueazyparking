package com.daikou.eazy_pass.ui.splash_screen

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.daikou.eazy_pass.databinding.ActivitySplashScreenBinding
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.HomeViewModel
import com.daikou.eazy_pass.view_model.SignUpViewModel

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var binding : ActivitySplashScreenBinding


    private var parking_status = ""
    private val deviceList = ArrayList<String>()

    private var homeViewModel: HomeViewModel = HomeViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initAction()
    }

    fun initAction(){
        Handler(Looper.getMainLooper()).postDelayed({
            if (Helper.AuthHelper.getAccessToken(this).isNotEmpty()){
                RedirectClass.gotoMainActivity(this, false)
            }else{
                RedirectClass.gotoLoginActivity(this)
                finish()
            }
        }, 2000)
    }
}