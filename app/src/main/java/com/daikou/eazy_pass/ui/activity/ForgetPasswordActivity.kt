package com.daikou.eazy_pass.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.widget.doOnTextChanged
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.handler.NetworkHandler.checkConnection
import com.daikou.eazy_pass.databinding.ActivityForgetPasswordBinding
import com.daikou.eazy_pass.enumerable.ForgotChangeEnum
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.PasswordVM
import java.util.HashMap
import java.util.logging.Handler

class ForgetPasswordActivity : BaseActivity<PasswordVM>() {
    private lateinit var binding : ActivityForgetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()

        initObserved()

        doAction()

    }

    private fun initView() {
        this.setSupportActionBar(binding.appBar.toolbar)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.title.text = getString(R.string.forget_password)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initObserved() {
        mViewModel.loading.observe(this) { hasLoading ->
            binding.loadingView.root.visibility = if (hasLoading) View.VISIBLE else View.GONE
        }

        mViewModel.requestOtpMutableLiveData.observe(this) { it ->
            if (it.success) {
                it.data?.let {
                    val jsonObject = it.asJsonObject    // Json Data
                    if (jsonObject.has("otp_token") && !jsonObject["otp_token"].isJsonNull){
                        val otpToken = jsonObject["otp_token"].asString
                        RedirectClass.gotoVerifyOtpCodeActivity(self(), otpToken)
                    }

                }

            } else {
                it.message.let {
                    MessageUtil.showError(self(), null, it)
                }
            }
        }

        mViewModel.onErrorData.observe(this){
            MessageUtil.showError(self(), null, it.toString())
        }

    }

    private fun doAction() {
        binding.actionSubmit.setOnClickListener {
            if (checkConnection(this)) {
                val phoneNumber = binding.phoneNumberTf.text.toString()
                if (phoneNumber.isEmpty() ||
                    phoneNumber.length < Constants.PhoneNumberConfig.PHONE_MIN_LENGTH ||
                    phoneNumber.length > Constants.PhoneNumberConfig.PHONE_MAX_LENGTH ||
                    !Helper.isPhoneValid(phoneNumber)
                ) {
                    binding.phoneNumberTfLayout.error =
                        getString(R.string.please_enter_a_valid_mobile_number)
                } else {
                    binding.phoneNumberTfLayout.error = null
                    val body = HashMap<String, Any>()
                    body["phone"] = binding.phoneNumberTf.text.toString().trim()
                    mViewModel.submitRequestOtp(ForgotChangeEnum.SubmitRequestOtp, body)
                }
            }
        }

        binding.phoneNumberTf.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(
                    text,
                    binding.phoneNumberTfLayout,
                    getString(R.string.field_is_required)
                )
            }
        }
    }
}