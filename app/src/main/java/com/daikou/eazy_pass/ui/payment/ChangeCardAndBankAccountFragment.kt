package com.daikou.eazy_pass.ui.payment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.ui.adapter.BanksAdapter
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.databinding.FragmentChangeCardAndBankAccountBinding
import com.daikou.eazy_pass.helper.Helper

class ChangeCardAndBankAccountFragment : Fragment() {

    private lateinit var fContext : FragmentActivity
    private lateinit var binding : FragmentChangeCardAndBankAccountBinding
    private lateinit var navController: NavController
    private lateinit var savedBankList : ArrayList<SaveBankAccount>
    private var tickIdBankAccount : Int? = null
    private lateinit var banksAdapter: BanksAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            this.savedBankList = it.getSerializable(SaveBankAccountKey) as ArrayList<SaveBankAccount>
            this.tickIdBankAccount = it.getInt(idTickBankAccountKey)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentChangeCardAndBankAccountBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAction()
    }

    private fun initView() {
        navController = Navigation.findNavController(requireView())
        if(savedBankList.size>0 ){
            banksAdapter = BanksAdapter(context = fContext, savedBankList = savedBankList, tickIdBankAccount ?: -1)
            banksAdapter.initCheckedPosition(savedBankList,tickIdBankAccount ?: -1)
            binding.mySaveBankRecyclerView.apply {
                layoutManager = LinearLayoutManager(fContext)
                adapter = banksAdapter
            }
        }


    }
    private  fun  initAction(){
        banksAdapter.onClickBank = { isClick, saveBackAccount ->
            if (isClick){
                accessParentFragment()?.saveListBankAccountListener?.invoke(true, saveBackAccount)
                Helper.saveSelectedBank(fContext, saveBackAccount) // save selected bank
//                tickIdBankAccount?.let {
//                    banksAdapter.updateAdapter(savedBankList, it)
//                }
            }
        }
        binding.actionAddCardOtherBankTv.setOnClickListener {
            navController.navigate(R.id.action_changeCardAndBankAccountFragment_to_selectBankFragment)
        }

        binding.actionCloseImg.setOnClickListener {
            accessParentFragment()?.dismiss()
        }
    }

    companion object {
        const val idTickBankAccountKey: String = "idTickBankAccountKey"
        const val firstSaveBankAccountKey: String = "firstSaveBankAccountKey"
        const val SaveBankAccountKey: String = "SaveBankAccountKey"

        @JvmStatic
        fun newInstance() =
            ChangeCardAndBankAccountFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
    private fun accessParentFragment() : PaymentMethodFragment?{
        if (fContext.supportFragmentManager.findFragmentByTag("PaymentMethodFragment") is PaymentMethodFragment) {
            return fContext.supportFragmentManager.findFragmentByTag("PaymentMethodFragment") as PaymentMethodFragment
        }
        return null
    }
}