package com.daikou.eazy_pass.ui.change_password

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.widget.doOnTextChanged
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.ActivityChangePasswordBinding
import com.daikou.eazy_pass.enumerable.ForgotChangeEnum
import com.daikou.eazy_pass.enumerable.VerifyPinEnum
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.redirect.RedirectClass
import com.daikou.eazy_pass.view_model.PasswordVM

class ChangePasswordActivity : BaseActivity<PasswordVM>() {

    private lateinit var mBinding : ActivityChangePasswordBinding
    private var verifyPinEnum: VerifyPinEnum = VerifyPinEnum.Other
    private var pinCodeOrOtp: String = ""
    private var otpToken: String = ""

    companion object {
        const val PinCodeKey = "pin_code_key"
        const val Otp_token_key = "otp_token_key"
        const val ScreenThatRequestKey = "screen_that_request_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initView()

        initObserved()

        doAction()
    }

    private fun initView() {
        this.setSupportActionBar(mBinding.appBarLayout.toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mBinding.appBarLayout.title.text = resources.getString(R.string.change_password)

        // Init Data
        if (intent.hasExtra(PinCodeKey)) {
            pinCodeOrOtp = intent.getStringExtra(PinCodeKey) ?: ""
        }

        if (intent.hasExtra(Otp_token_key)) {
            otpToken = intent.getStringExtra(Otp_token_key) ?: ""
        }

        if (intent.hasExtra(ScreenThatRequestKey)) {
            verifyPinEnum = intent.getSerializableExtra(ScreenThatRequestKey) as VerifyPinEnum
        }
    }

    private fun doAction() {
        mBinding.actionSubmitPassword.setOnClickListener {
            mViewModel.validateField(
                mBinding.newPasswordTextInputEditText.text.toString(),
                mBinding.confirmNewPasswordTextInputEditText.text.toString()
            )
        }

        //new
        mBinding.newPasswordTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(text, mBinding.newPasswordTextInputLayout, getString(R.string.field_is_required))
            }
        }
        //old
        mBinding.confirmNewPasswordTextInputEditText.doOnTextChanged { text, start, before, count ->
            text?.let {
                Helper.validateTextInputField(text, mBinding.confirmNewPasswordTextInputLayout, getString(R.string.field_is_required))
            }
        }

    }

    private fun initObserved() {
        mViewModel.changePwdValidate.observe(this) {
            if (it.hasDoneValidate == true) {
                val requestBody = HashMap<String, Any>()
                requestBody["password"] =
                    mBinding.newPasswordTextInputEditText.text.toString().trim()
                requestBody["password_confirmation"] =
                    mBinding.confirmNewPasswordTextInputEditText.text.toString().trim()

                when (verifyPinEnum) {
                    VerifyPinEnum.VerificationPinScreen -> {
                        requestBody["current_password"] = pinCodeOrOtp
                        mViewModel.submitChangePwd(
                            requestBody
                        )
                    }
                    VerifyPinEnum.ForgetPasswordScreen -> {
                        requestBody["otp_token"] = otpToken
                        requestBody["otp"] = pinCodeOrOtp
                        mViewModel.submitRequestOtp(
                            ForgotChangeEnum.SubmitChangePasswordByOtp,
                            requestBody
                        )
                    }
                    else -> {
                        RedirectClass.gotoMainActivity(self())
                    }
                }

            } else {
                when {
                    it.newPwd != null -> {
                        mBinding.newPasswordTextInputLayout.error = getString(it.newPwd)
                    }
                    it.confirmPwd != null -> {
                        mBinding.confirmNewPasswordTextInputLayout.error =
                            getString(it.confirmPwd)
                    }
                    it.pwdNotMatch != null -> {
                        MessageUtil.showError(self(), "", getString(it.pwdNotMatch))
                        mBinding.confirmNewPasswordTextInputLayout.error = null
                        mBinding.newPasswordTextInputLayout.error = null
                    }
                    else -> {
                        mBinding.confirmNewPasswordTextInputLayout.error = null
                        mBinding.newPasswordTextInputLayout.error = null
                    }
                }
            }
        }

        mViewModel.loading.observe(this) { hasLoading ->
            mBinding.loadingView.root.visibility = if (hasLoading) View.VISIBLE else View.GONE
        }


        mViewModel.submitChangePasswordMutableLiveData.observe(this) {
            Helper.AuthHelper.savePassword(self(), mBinding.newPasswordTextInputEditText.text.toString().trim())
            MessageUtil.showSuccess(
                this,
                "",
                getString(R.string.chang_successfully),
                true,
                ) {
                it.dismiss()
                RedirectClass.gotoMainActivity(self(), isClearTop = true)
            }
        }

        mViewModel.onErrorData.observe(this){
            MessageUtil.showError(self(), null, it.toString())
        }

        mViewModel.requestOtpMutableLiveData.observe(this) { it ->
            if (it.success) {
                RedirectClass.gotoLoginActivity(self(), isClearTop = true)
            } else {
                it.message.let {
                    MessageUtil.showError(self(), null, it)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            android.R.id.home -> finish()
        }
        return true
    }
}