package com.daikou.eazy_pass.ui.bottom_sheet

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.FragmentCallServiceBottomSheetBinding
import com.daikou.eazy_pass.enumerable.LanguageSetting
import com.daikou.eazy_pass.util.LanguageManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class CallServiceBottomSheet : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentCallServiceBottomSheetBinding
    private lateinit var fContext: FragmentActivity
    private lateinit var phoneNumber : String


    companion object {
        @JvmStatic
        fun newInstance(
            phoneNumber : String
        ) =
            CallServiceBottomSheet().apply {
                this.phoneNumber = phoneNumber
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.TopRoundCornerBottomSheetDialogTheme)
        arguments?.let {
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCallServiceBottomSheetBinding.inflate(inflater, container, false)

        initAction()
        initView()

        return binding.root
    }



    private fun initAction(){
        binding.callBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + this.phoneNumber)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
        binding.cancelBtn.setOnClickListener {
            dialog?.dismiss()
        }
    }

    private fun initView(){
        binding.callBtn.text = String.format("%s: %s",fContext.getString(R.string.call), this.phoneNumber)
    }
}