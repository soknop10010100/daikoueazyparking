package com.daikou.eazy_pass.ui.bottom_sheet

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.payment.PaymentResponse
import com.daikou.eazy_pass.databinding.FragmentPaymentBinding
import com.daikou.eazy_pass.databinding.FragmentTransactionDetailBinding
import com.daikou.eazy_pass.helper.Helper
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class PaymentFragment : BottomSheetDialogFragment() {

    private lateinit var binding : FragmentPaymentBinding
    private lateinit var paymentResponse: PaymentResponse
    private lateinit var fContext: FragmentActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.TopRoundCornerBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPaymentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    companion object {
        @JvmStatic
        fun newInstance(paymentResponse: PaymentResponse) =
            PaymentFragment().apply {
                this.paymentResponse = paymentResponse
            }
    }
    private fun initView(){
//        val createDate = customTransaction.createdAt ?: ""
        val dateString = Helper.formatDatFromDatetime("2022-05-05 10:21:34", "yyyy-MM-dd HH:mm:ss",
            "EEEE, dd MMMM yyyy")
        binding.dateTrxDetailTv.text = dateString

//        binding.payByTrxDetailTv.text = customTransaction.payBy ?: "N/A"
//        binding.trxIdTrxDetailTv.text = customTransaction.mUserWithdraw?.transactionId?.toString() ?: ""
//        binding.statusTrxDetailTv.text = customTransaction.walletTransactionStatus ?: "N/A"

//        val amount = customTransaction.total ?: 0.0
        val amount = -2.00
//        val amountColor =
//            if (amount < 0 ) ContextCompat.getColor(fContext, R.color.red_300) else ContextCompat.getColor(fContext,
//                R.color.green_600
//            )
        binding.totalAmountTv.text = Helper.amountFormat("$", amount)
//        binding.totalAmountTv.setTextColor(amountColor)
    }
}