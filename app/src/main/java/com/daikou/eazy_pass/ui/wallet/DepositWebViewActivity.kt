package com.daikou.eazy_pass.ui.wallet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebViewClient
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.data.model.response_model.DepositResponse
import com.daikou.eazy_pass.databinding.ActivityDepositWebViewBinding
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseViewModel
import com.daikou.eazy_pass.view_model.DepositViewModel

class DepositWebViewActivity : BaseActivity<DepositViewModel>() {

    private lateinit var binding : ActivityDepositWebViewBinding

    companion object{
        const val Deposit_Data_Key = "deposit_data_key"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDepositWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.setSupportActionBar(binding.appBarLayout.toolbar)
        this.supportActionBar!!.setDisplayShowTitleEnabled(false)
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.appBarLayout.title.setText(R.string.deposit)
        initView()
    }

    private fun initView(){
        if (intent.hasExtra(Deposit_Data_Key) && intent.getSerializableExtra(Deposit_Data_Key) != null){
            val depositResponse = intent.getSerializableExtra(Deposit_Data_Key) as DepositResponse
            binding.web.loadUrl(depositResponse.paymentLink ?: "")
            binding.web.settings.javaScriptEnabled = true
            binding.web.webViewClient = WebViewClient()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}