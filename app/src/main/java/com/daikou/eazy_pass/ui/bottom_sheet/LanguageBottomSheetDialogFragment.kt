package com.daikou.eazy_pass.ui.bottom_sheet

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.databinding.FragmentBiometricDialogBottomSheetBinding
import com.daikou.eazy_pass.databinding.FragmentLanguageBottomSheetDailogBinding
import com.daikou.eazy_pass.enumerable.LanguageSetting
import com.daikou.eazy_pass.util.LanguageManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class LanguageBottomSheetDialogFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentLanguageBottomSheetDailogBinding
    private lateinit var fContext: FragmentActivity
    private lateinit var chooseLanguage: (LanguageSetting) -> Unit


    companion object {
        @JvmStatic
        fun newInstance(
            chosenLanguage : (LanguageSetting) -> Unit,
        ) =
            LanguageBottomSheetDialogFragment().apply {
                this.chooseLanguage = chosenLanguage
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.TopRoundCornerBottomSheetDialogTheme)
        arguments?.let {
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLanguageBottomSheetDailogBinding.inflate(inflater, container, false)

        initAction()
        initView()

        return binding.root
    }



    private fun initAction(){
       binding.actionCloseImg.setOnClickListener {
           dialog?.dismiss()
       }

        binding.actionEnglishMenu.setOnClickListener {
            dialog?.dismiss()
            chooseLanguage.invoke(LanguageSetting.English)
        }

        binding.actionKhmerMenu.setOnClickListener {
            dialog?.dismiss()
            chooseLanguage.invoke(LanguageSetting.Khmer)
        }
    }

    private fun initView(){
        val isKhmer = LanguageManager.getLanguage(fContext) == "km"
        val isEnglish = LanguageManager.getLanguage(fContext) == "en"

        binding.imageChoseKh.visibility = if (isKhmer) View.VISIBLE else View.GONE
        binding.imageChoseEn.visibility = if (isEnglish) View.VISIBLE else View.GONE
    }
}