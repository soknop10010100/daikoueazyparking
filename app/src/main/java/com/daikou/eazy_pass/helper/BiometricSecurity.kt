package com.daikou.eazy_pass.helper
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricManager
import androidx.core.app.ActivityCompat.startActivityForResult


object BiometricSecurity {
    fun checkBiometric(context: Context) : Boolean{
        var error : String? = null
        var hasBiometric = false
        val biometricManager: BiometricManager = BiometricManager.from(context)
        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.BIOMETRIC_WEAK)) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                Log.d(
                    "MY_APP_TAG",
                    "App can authenticate using biometrics."
                )
                hasBiometric = true
            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                Log.e(
                    "MY_APP_TAG",
                    "No biometric features available on " +
                            " device."
                )
                error = "No biometric features available on this device."
                hasBiometric = false
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                Log.e(
                    "MY_APP_TAG",
                    "Biometric features are currently unavailable."
                )
                error = "Biometric features are currently unavailable."
                hasBiometric = false
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                error = "The user hasn't associated any biometric credentials with their account."
                hasBiometric = false
            }

            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> {
                error = "Biometric features are currently security update required."
                hasBiometric = false
            }

            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> {
                error = "Biometric features are currently unsupported."
                hasBiometric = false
            }

            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> {
                error = "Biometric features are currently status unknown."
                hasBiometric = false
            }
        }
        if(error != null){
            MessageUtil.showError(context, "", error)
        }
        return hasBiometric
    }
}
