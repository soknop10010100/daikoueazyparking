package com.daikou.eazy_pass.helper

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.contract.ActivityResultContracts
import com.daikou.eazy_pass.util.BetterActivityResult
import java.util.concurrent.atomic.AtomicInteger

object PermissionHelper {
    fun requestMultiPermission(context: ActivityResultCaller, permissionList: Array<String>, callback : (Boolean)-> Unit) {
        val sizeOfPermissionRequest = AtomicInteger()
        val activityMultiPermission: BetterActivityResult<Array<String>, Map<String, Boolean>> = BetterActivityResult.registerForActivityResult(
                context,
                ActivityResultContracts.RequestMultiplePermissions(),object :
                BetterActivityResult.OnActivityResult<Map<String, Boolean>> {
                override fun onActivityResult(result: Map<String, Boolean>) {
                    result.forEach { (_, hasPermission) ->
                        if(hasPermission){
                            sizeOfPermissionRequest.getAndIncrement()
                        }
                    }
                    val isHasPermission = sizeOfPermissionRequest.toInt() == permissionList.size
                    callback.invoke(isHasPermission)
                }

            })
        activityMultiPermission.launch(permissionList)
    }


    fun requestBluetoothPermission(context: ActivityResultCaller, callback : (Boolean)-> Unit) {
        val permissionList: Array<String>
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            permissionList = arrayOf(Manifest.permission.BLUETOOTH_CONNECT)
        }else{
            permissionList = arrayOf(Manifest.permission.BLUETOOTH)
        }
        val sizeOfPermissionRequest = AtomicInteger()
        val activityMultiPermission: BetterActivityResult<Array<String>, Map<String, Boolean>> = BetterActivityResult.registerForActivityResult(
            context,
            ActivityResultContracts.RequestMultiplePermissions(),object :
                BetterActivityResult.OnActivityResult<Map<String, Boolean>> {
                override fun onActivityResult(result: Map<String, Boolean>) {
                    result.forEach { (_, hasPermission) ->
                        if(hasPermission){
                            sizeOfPermissionRequest.getAndIncrement()
                        }
                    }
                    val isHasPermission = sizeOfPermissionRequest.toInt() == permissionList.size
                    callback.invoke(isHasPermission)
                }

            })
        activityMultiPermission.launch(permissionList)
    }

    fun isHasExternalStoragePermission(context: Context) : Boolean{
        return context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }
    fun isHasCamaraPermission(context: Context) : Boolean{
        return  context.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

}