package com.daikou.eazy_pass.helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.text.format.DateUtils
import android.util.Base64
import android.util.Log
import android.util.Patterns
import androidx.annotation.RequiresApi
import com.daikou.eazy_pass.MyApplication
import com.daikou.eazy_pass.data.model.PassedActivityModel
import com.daikou.eazy_pass.data.model.SaveBankAccount
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.LanguageManager
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import kotlin.collections.HashMap
import kotlin.math.abs

@SuppressLint("SimpleDateFormat")
object Helper {

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    return true
                }

                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    return true
                }

                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    return true
                }
            }
        }
        return false
    }

    fun hasNotNetworkAvailable(context: Context): Boolean {
        return !isNetworkAvailable(context)
    }

    fun setStringSharePreference(context: Context, key : String, value: String){
        val pref = context.getSharedPreferences(key, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(key, value)
        editor.apply()
    }
    fun setBooleanSharePreference(context: Context, key : String, value: Boolean){
        val pref = context.getSharedPreferences(key, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }
    fun getBooleanSharePreference(context: Context, key: String) : Boolean{
        val pref = context.getSharedPreferences(key, Context.MODE_PRIVATE)
        return pref.getBoolean(key, false)
    }

    fun getStringSharePreference(context: Context, key: String) : String{
        val pref = context.getSharedPreferences(key,Context.MODE_PRIVATE)
        return pref.getString(key, "") ?: ""
    }

    fun convertQRImageToText(bMap: Bitmap) : String{
        var content = ""
        try {
            val intArray = IntArray(bMap.width * bMap.height)
            bMap.getPixels(intArray, 0, bMap.width, 0, 0, bMap.width, bMap.height)
            val source: LuminanceSource = RGBLuminanceSource(bMap.width, bMap.height, intArray)
            val bitmap = BinaryBitmap(HybridBinarizer(source))
            val reader: Reader = MultiFormatReader()
            var result: Result? = null
            result = reader.decode(bitmap)
            content = result.text
        }catch (e : NotFoundException) {
            e.printStackTrace();
        } catch (e:ChecksumException) {
            e.printStackTrace();
        } catch (e : FormatException) {
            e.printStackTrace();
        } catch (e : NullPointerException) {
            e.printStackTrace();
        }
        return  content
    }


    fun validateTextInputField(text: CharSequence, inputField : TextInputLayout, errorMsg : String?) {
        inputField.error = if(text.isEmpty()) errorMsg else null
    }

    fun convertBitmap(context: Context, uri: Uri) : Bitmap{
        return MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
    }
    fun convertToBase64(
        bitmap: Bitmap,
    ): String? {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    fun formatDate(date: Date?, pattern  : String) : String? {
        val format = SimpleDateFormat(pattern, Locale.US)
        return try {
            format.format(date)
        } catch (dte: Exception) {
            ""
        }
    }


    fun formatDatFromDatetime(dateTime : String, oldPatten : String, newPattern: String) : String{
        val old = SimpleDateFormat(oldPatten)
        val new = SimpleDateFormat(newPattern)
        val date = old.parse(dateTime)
        val printedDate = date?.let { new.format(it) }
        return printedDate ?: ""
    }

    fun isToday(dateTime: String, pattern: String) : Boolean{
        val date = formatToDate(dateTime, pattern)
        return if (date != null) DateUtils.isToday(date.time) else false
    }
    fun isYesterday(dateTime: String, pattern: String) : Boolean{
        val date = formatToDate(dateTime, pattern)
        return if (date != null) DateUtils.isToday(date.time + DateUtils.DAY_IN_MILLIS) else false
    }
    fun formatToDate(dateString : String, pattern: String) : Date?{
        val fm = SimpleDateFormat(pattern)
        return fm.parse(dateString)
    }
    fun formatTimeFromDate(dateTime : String, oldPatten : String, newPattern: String) : String{
        val oldFm = SimpleDateFormat(oldPatten)
        val date = oldFm.parse(dateTime)
        val timeFm = SimpleDateFormat(newPattern)
        val time = date?.let { timeFm.format(it) }
        return time ?: ""
    }

    fun amountFormat(currencySymbol: String, amount: Double): String {
        if(amount < 0){
            val positiveAmount = abs(amount)
            return String.format(Locale.US, "-%s%.2f", currencySymbol, positiveAmount)
        }else{
            return String.format(Locale.US, "%s%.2f", currencySymbol, amount)
        }
    }

    fun saveListBank(context: Context ,saveBankAccount: SaveBankAccount){
        var bankList = getListBankJson(context)
        if (bankList.size > 0) {
            for ( bankAccount in bankList) {
                if (bankAccount.accountNumber != saveBankAccount.accountNumber) {
                    bankList.add(saveBankAccount)
                    saveSelectedBank(context, saveBankAccount)
                    break
                }
            }
        }else{
            bankList.add(saveBankAccount)
            saveSelectedBank(context, saveBankAccount)
        }
        val jsonBankList = Gson().toJson(bankList)
        setStringSharePreference(context, getBankListKey(context), jsonBankList)
    }

    fun getBankListKey(context: Context): String{
        if (AuthHelper.hasUserInSharePreference(context)) {
            return String.format(
                "%s%s",
                Constants.LIST_BANK_KEY,
                AuthHelper.getUserSharePreference(context).id!!.toString()
            )
        }else{
            return ""
        }
    }

    fun saveSelectedBank(context: Context, saveBankAccount: SaveBankAccount){
        val selectedBankJson = Gson().toJson(saveBankAccount)
        setStringSharePreference(context, getSelectedBankKey(context), selectedBankJson)
    }
    private fun getSelectedBankKey(context: Context) : String{
        if (AuthHelper.hasUserInSharePreference(context)) {
            return String.format(
                "%s%s",
                Constants.SELECTED_BANK,
                AuthHelper.getUserSharePreference(context).id!!.toString()
            )
        }else{
            return ""
        }
    }

    fun getSelectedBank(context: Context): SaveBankAccount {
        val gson = Gson()
        if (getStringSharePreference(context, getSelectedBankKey(context)) == ""){
            return SaveBankAccount()
        }else {
            val saveBankJson = getStringSharePreference(context, getSelectedBankKey(context))
            val bankAccountTypeToken =
                object : TypeToken<SaveBankAccount>() {}.type
            return gson.fromJson(saveBankJson, bankAccountTypeToken)
        }
    }

    fun getListBankJson(context: Context): ArrayList<SaveBankAccount>{
        if (getStringSharePreference(context, getBankListKey(context)) == ""){
            return ArrayList()
        }else{
            val gson = Gson()
            val jsonListBank = getStringSharePreference(context, getBankListKey(context))
            val bankAccountTypeToken =
                object : TypeToken<ArrayList<SaveBankAccount>>() {}.type

            return gson.fromJson(jsonListBank, bankAccountTypeToken)
        }
    }

    fun isPhoneValid(phoneNumber : String) : Boolean{
        return  Patterns.PHONE.matcher(phoneNumber).matches()
    }
    fun isEmailValid(email : String) : Boolean{
        return  Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    object AuthHelper{

        fun hasUserInSharePreference(context: Context) : Boolean{
            return getStringSharePreference(context, Constants.Auth.USER_KEY) != ""
        }
        fun saveUserSharePreference(context: Context,user: User){
            val userJson = Gson().toJson(user)
            setStringSharePreference(context, Constants.Auth.USER_KEY, userJson)
        }
        fun getUserSharePreference(context: Context): User {
            val userJson = getStringSharePreference(context, Constants.Auth.USER_KEY)
            if(userJson!= ""){
                val typeToken = object : TypeToken<User>(){}.type
                val gson = Gson()
                return gson.fromJson(userJson, typeToken)
            }else{
                return User()
            }
        }

        fun saveAccessToken(context: Context, token : String){
            setStringSharePreference(context, Constants.Auth.TOKEN_KEY, token)
        }

        fun getAccessToken(context: Context) : String{
            return getStringSharePreference(context, Constants.Auth.TOKEN_KEY)
        }

        fun isKycRefused(context: Context) : Boolean{
            if (hasUserInSharePreference(context)){
                val user = getUserSharePreference(context)
                return user.kycDocument?.status == "REFUSED"
            }else{
                return false
            }
        }

        fun isHasBalance(context: Context): Boolean {
            if (hasUserInSharePreference(context)) {
                val user = getUserSharePreference(context)
                user.totalBalance?.let {
                    return it > 0
                }
            }
            return false
        }

        fun savePassword(context: Context, password: String){
            setStringSharePreference(context, Constants.Auth.PASSWORD_KEY, password)
        }

        fun getPassword(context: Context) : String{
            return getStringSharePreference(context, Constants.Auth.PASSWORD_KEY)
        }


        fun clearSession(context: Context){
            setStringSharePreference(context, Constants.Auth.USER_KEY, "")
            setStringSharePreference(context, Constants.Auth.TOKEN_KEY, "")
            setStringSharePreference(context, Constants.BIOMETRIC_KEY, "")
        }

        fun getUserId(context: Context): String{
            val user = getUserSharePreference(context)
            if(user.id != null){
                return  user.id.toString()
            }else{
                return ""
            }
        }

    }

    object StringResource{
        fun getResourceString(stringIdName: String) : String{
            val packageName = MyApplication.context.packageName
            val newId = stringIdName.replace(" ", "_")
                .replace(".","")
                .replace("!","")
                .replace("$","").toLowerCase()
            val resId = MyApplication.context.resources.getIdentifier(newId, "string", packageName )
            val resource = MyApplication.context.resources

            val local = Locale(LanguageManager.getLanguage(MyApplication.context))

            val conf: Configuration = resource.configuration
            conf.setLocale(local)

            resource.updateConfiguration(conf, resource.displayMetrics)
            return resource.getString(resId)
        }
        fun isHasInResource(stringIdName : String): Boolean{
            val packageName = MyApplication.context.packageName
            val newId = stringIdName.replace(" ", "_")
                .replace(".","")
                .replace("!","")
                .replace("$","").toLowerCase()
            val resId = MyApplication.context.resources.getIdentifier(newId, "string", packageName)
            return resId != 0;
        }
    }

    object PassedActivityHelper{

        const val ACTIVITY_LIST_KEY = "activity_list_key"
        const val ENTRANCE_DATE_KEY = "entrance_date_key"
        fun savePassedActivity( context: Context, passedActivityModel: PassedActivityModel){
            val activityList = getSavePassedActivity(context)
            activityList.add(passedActivityModel)
            val gson = Gson()
            val listJson = gson.toJson(activityList)
            val userId = AuthHelper.getUserSharePreference(context).id.toString()
            setStringSharePreference(context, String.format("%s-%s", ACTIVITY_LIST_KEY, userId), listJson)
        }

        fun getSavePassedActivity(context: Context) : ArrayList<PassedActivityModel>{
            val userId = AuthHelper.getUserSharePreference(context).id.toString()
            val listJson =  getStringSharePreference(context, String.format("%s-%s", ACTIVITY_LIST_KEY, userId))
            val gson = Gson()
            val typeToken = object : TypeToken<ArrayList<PassedActivityModel>>(){}.type
            if (listJson != ""){
                return gson.fromJson(listJson, typeToken)
            }else{
                return ArrayList<PassedActivityModel>()
            }
        }

        fun saveEntranceDate(context: Context, date : Date){
            val dateString = formatDate(date, "E,dd-MM-yyy h:mm:ss a") ?: ""
            setStringSharePreference(context, ENTRANCE_DATE_KEY,dateString)
        }

        fun getEntranceDate(context: Context) : String{
            return getStringSharePreference(context, ENTRANCE_DATE_KEY)
        }
    }

    object ParkingInfo{
        const val isAlreadyRequest = "isAlreadyRequest"
        fun saveParkingStatus(context: Context, status : String){
            Log.d("stussadsadsad", status)
            setStringSharePreference(context, parkingStatusKey(context), status)
        }
        fun getParkingStatus(context: Context) : String{
            return  getStringSharePreference(context, parkingStatusKey(context))
        }
        fun saveParkingDevices(context: Context, devices : HashMap<String, String>){
            val gson = Gson()
            val listJson = gson.toJson(devices)
            setStringSharePreference(context,Constants.PARKING_DEVICE_LIST, listJson)
        }
        fun getParkingDevice(context: Context) : HashMap<String, String>{
            val listJson = getStringSharePreference(context, Constants.PARKING_DEVICE_LIST)
            val gson = Gson()
            val type = object : TypeToken<HashMap<String , Any>>(){}.type
            if (listJson != ""){
                return gson.fromJson(listJson, type)
            }else{
                return  HashMap()
            }
        }

        fun storeDataNotPay(context: Context, barrierId : String, status : String, date : Date){
            val parkingDataStore = getDataNotPay(context)
            val bodyData = HashMap<String,Any>()
            val dateStr = formatDate(date, "yyyy-MM-dd HH:mm:ss")
            bodyData["barrier_id"] = barrierId
            bodyData["date"] = dateStr ?: ""
            parkingDataStore[status] = bodyData
            val json  = Gson().toJson(parkingDataStore)
            setStringSharePreference(context, userNotPayKey(context), json)
        }

        private fun userNotPayKey(context: Context) : String{
            return Constants.userNotPay.userIdKey + AuthHelper.getUserId(context)
        }

        private fun parkingStatusKey(context: Context) : String {
            return Constants.PARKING_STATUS + AuthHelper.getUserId(context)
        }


        fun clearUserNotPay(context: Context){
            setStringSharePreference(context, userNotPayKey(context), "")
        }
        fun getDataNotPay(context: Context) : HashMap<String , HashMap<String ,Any>>{
            val json = getStringSharePreference(context, userNotPayKey(context))
            val type = object : TypeToken<HashMap<String , HashMap<String ,Any>>>(){}.type
            val gson = Gson()
            if (json != ""){
                return gson.fromJson(json, type)
            }else{
                return  HashMap()
            }

        }

        fun isNeedPay(context: Context): Boolean{
            val data = getDataNotPay(context)
            return data.isNotEmpty()
        }

        fun removeNotPayData(context: Context , key : String){
            val parkingDataStore = getDataNotPay(context)
            parkingDataStore.forEach { t, u ->
                if(key == t){
                    parkingDataStore.remove(key)
                }
            }
            val json  = Gson().toJson(parkingDataStore)
            setStringSharePreference(context, userNotPayKey(context), json)
        }

        fun getDeviceRange(context: Context) : HashMap<String, Int> {
            var deviceRange = HashMap<String, Int>()
            val user = AuthHelper.getUserSharePreference(context)
            val ranges = user.barrierRangeList
            ranges?.let {
                for (value in ranges){
                    deviceRange[value.name ?: ""] = value.range ?: 0
                }
            }
            return deviceRange
        }


//        fun doPayment(context: Context){
//            if (Helper.ParkingInfo.isNeedPay(context)){
//                val data = Helper.ParkingInfo.getUserNotPay(context)
//                val body = HashMap<String, Any>()
//                body["barrier_id"] = data.barrier
//                PaymentViewModel().openGateDoPayment(body)
//            }
//        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Throws(java.lang.Exception::class)
    fun encrypt(plaintext: String, secretKey: String): String? {
        val keySpec = SecretKeySpec(secretKey.toByteArray(), "AES")
        val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

        val iv = ByteArray(16)
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, IvParameterSpec(iv))
        val encryptedBytes: ByteArray = cipher.doFinal(plaintext.toByteArray())
        return java.util.Base64.getEncoder().encodeToString(encryptedBytes)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Throws(java.lang.Exception::class)
    fun decrypt(encryptedText: String?, secretKey: String): String? {
        val keySpec = SecretKeySpec(secretKey.toByteArray(), "AES")
        val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

        val iv = ByteArray(16)
        cipher.init(Cipher.DECRYPT_MODE, keySpec, IvParameterSpec(iv))
        val encryptedBytes: ByteArray = java.util.Base64.getDecoder().decode(encryptedText)
        val decryptedBytes = cipher.doFinal(encryptedBytes)
        return String(decryptedBytes)
    }


}