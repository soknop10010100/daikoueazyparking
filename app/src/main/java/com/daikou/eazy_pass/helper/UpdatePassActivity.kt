package com.daikou.eazy_pass.helper

import com.daikou.eazy_pass.data.model.CustomPassedActivity
import com.daikou.eazy_pass.data.model.PassedActivityModel

open class UpdatePassActivity {

    open fun initListPassedActivity() : ArrayList<PassedActivityModel>{
        val list : ArrayList<PassedActivityModel> = ArrayList()
        list.add(PassedActivityModel(2.10, "2023-07-12 13:00:56"))
        list.add(PassedActivityModel(2.50, "2023-07-12 15:00:56"))
        list.add(PassedActivityModel(1.80, "2023-07-15 09:00:56"))
        list.add(PassedActivityModel(2.90, "2023-07-15 11:00:56"))
        list.add(PassedActivityModel(1.10, "2023-07-16 19:00:56"))
        list.add(PassedActivityModel(1.60, "2023-07-22 08:00:56"))
        list.add(PassedActivityModel(2.40, "2023-07-22 12:00:56"))
        list.reverse()
        return list
    }

    open fun getUpdatedPassedActivityList(passedActivityList : ArrayList<PassedActivityModel>) : ArrayList<CustomPassedActivity>{
        val tempList : ArrayList<CustomPassedActivity> = ArrayList()
        val dateGroup = HashMap<String, String>()

        passedActivityList.forEach{
            val customActivityModel = CustomPassedActivity(
                it.total,
                it.message,
                it.entranceAt,
                it.exitAt
            )
            if (it.entranceAt != "") {
                val dateString =
                    Helper.formatDatFromDatetime(it.entranceAt, "E,dd-MM-yyy h:mm:ss a", "dd MMM, yyyy")
                if (!dateGroup.containsKey(dateString)){
                    dateGroup[dateString] = dateString
                    customActivityModel.isShowHeadDate = true
                }
            }
            if (it.exitAt != ""){
                val dateString =
                    Helper.formatDatFromDatetime(it.entranceAt, "E,dd-MM-yyy h:mm:ss a", "dd MMM, yyyy")
                if (!dateGroup.containsKey(dateString)){
                    dateGroup[dateString] = dateString
                    customActivityModel.isShowHeadDate = true
                }
            }

            tempList.add(customActivityModel)
        }

        return tempList
    }
}