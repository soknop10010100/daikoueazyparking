package com.daikou.eazy_pass.helper

import android.app.Activity
import android.content.Context
import cn.pedant.SweetAlert.SweetAlertDialog
import com.daikou.eazy_pass.R

object MessageUtil {

    fun showError(context: Context, title: String?, text: String?) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(title ?: context.getString(R.string.oops))
                    .setContentText(String.format("%s", text))
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: RuntimeException) {
            ex.printStackTrace()
        }
    }

    fun showError(
        context: Context,
        title: String?,
        text: String?,
        onSweetClickListener: SweetAlertDialog.OnSweetClickListener?
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(title ?: context.getString(R.string.oops))
                    .setContentText(String.format("%s", text))
                    .setConfirmText(
                        context.getString(R.string.ok)
                    )
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: RuntimeException) {
            ex.printStackTrace()
        }
    }
    fun showError(
        context: Context,
        title: String?,
        text: String?,
        confirmBtnText: String?,
        onSweetClickListener: SweetAlertDialog.OnSweetClickListener?
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(title ?: context.getString(R.string.oops))
                    .setContentText(String.format("%s", text))
                    .setCancelText(context.getString(R.string.action_cancel))
                    .setConfirmText(
                        confirmBtnText
                    )
                    .showCancelButton(true)
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: RuntimeException) {
            ex.printStackTrace()
        }
    }

    fun showConfirm(
        context: Context,
        title: String?,
        text: String?,
        confirmBtnText : String? = null,
        onSweetClickListener: SweetAlertDialog.OnSweetClickListener?,
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(title)
                    .setContentText(text)
                    .setConfirmText( confirmBtnText ?:
                        context.getString(R.string.yes_m)
                    )
                    .setCancelButtonTextColor(R.color.black)
                    .showCancelButton(true)
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: java.lang.RuntimeException) {
            ex.printStackTrace()
        }
    }

    fun showConfirm(
        context: Context,
        title: String?,
        text: String?,
        confirmBtnText : String? = null,
        cancelAble : Boolean? = null,
        onSweetClickListener: SweetAlertDialog.OnSweetClickListener?,
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(title)
                    .setContentText(text)
                    .setCancelText(context.getString(R.string.no))
                    .setConfirmText( confirmBtnText ?:
                    context.getString(R.string.yes_m)
                    )
                    .setCancelButtonTextColor(R.color.black)
                    .showCancelButton(true)
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: java.lang.RuntimeException) {
            ex.printStackTrace()
        }
    }

    fun showSuccess(
        context: Context,
        title: String?,
        text: String?,
        isNoBtnNo : Boolean,
        onSweetClickListener: (SweetAlertDialog) -> Unit,
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText(title)
                    .setContentText(text)
                    .setConfirmText(
                        context.getString(R.string.yes_m)
                    )
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: java.lang.RuntimeException) {
            ex.printStackTrace()
        }
    }

    fun showWarning(
        context: Activity,
        title: String?,
        text: String?,
        confirmBtnText : String? = null,
        onSweetClickListener: SweetAlertDialog.OnSweetClickListener?,
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(title)
                    .setContentText(text)
                    .setConfirmText( confirmBtnText ?: context.getString(R.string.yes_m))
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.show()
        } catch (ex: java.lang.RuntimeException) {
            ex.printStackTrace()
        }
    }
}