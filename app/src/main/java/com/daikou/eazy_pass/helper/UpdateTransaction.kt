package com.daikou.eazy_pass.helper

import com.daikou.eazy_pass.data.model.CustomTransactionModel
import com.daikou.eazy_pass.data.model.TransactionModel
import com.daikou.eazy_pass.data.model.response_model.transaction.Transaction

open class UpdateTransaction {

    open fun getCustomTransactions(transactionList : ArrayList<Transaction>) : ArrayList<CustomTransactionModel>{
        val updateList : ArrayList<CustomTransactionModel> = ArrayList()
        val dateGroup = HashMap<String, String>()
        val totalAmount = HashMap<String, Double>()
        var totalAmountDouble = 0.0
        transactionList.forEach{
            val newTransaction= CustomTransactionModel(
                it.bookingCode,
                it.kessTransactionId,
                it.payBy,
                it.total,
                it.walletTransactionStatus,
                it.createdAt,
                it.userWithdraw,
                it.type,
                it.remark,
                it.label
            )
            val amount = it.total ?: 0.0
            val createAt = it.createdAt?: ""
            val dateStr = Helper.formatDatFromDatetime(createAt, "yyyy-MM-dd HH:mm:ss",
                "EEEE, dd MMMM yyyy")
            if (!dateGroup.containsKey(dateStr)){
                dateGroup[dateStr] = dateStr
                newTransaction.isShowHeader = true
                totalAmountDouble = amount
            }else{
                totalAmountDouble += amount
            }
            totalAmount[dateStr] = totalAmountDouble
            newTransaction.totalAmount = totalAmount
            updateList.add(newTransaction)
        }
        return  updateList
    }

    open fun initTransactions() : ArrayList<TransactionModel>{
        val list = ArrayList<TransactionModel>()
        list.add(TransactionModel(id = "T100001", "KessPay", -5.0,"2023-07-12 15:00:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 100.0,"2023-07-12 15:02:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 20.0,"2023-07-12 15:06:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", -200.0,"2023-07-12 15:07:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 1000.0,"2023-07-14 12:04:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 10.0,"2023-07-14 12:09:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 10.0,"2023-07-14 13:04:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 100.0,"2023-07-14 13:07:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", -200.0,"2023-07-14 16:04:56" ))
        list.add(TransactionModel(id = "T100001","KessPay", 1000.0,"2023-07-14 17:04:56" ))
        return list
    }
}