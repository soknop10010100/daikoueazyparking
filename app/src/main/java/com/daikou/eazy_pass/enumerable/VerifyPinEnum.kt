package com.daikou.eazy_pass.enumerable

enum class VerifyPinEnum {
    ForgetPasswordScreen,VerificationPinScreen, Other
}

enum class ForgotChangeEnum {
    SubmitRequestOtp, SubmitVerifyOtp,SubmitChangePasswordByOtp
}