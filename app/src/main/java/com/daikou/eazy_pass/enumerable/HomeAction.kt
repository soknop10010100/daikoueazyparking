package com.daikou.eazy_pass.enumerable

enum class HomeAction {
    Wallet, Profile, ScanQR, Pay
}