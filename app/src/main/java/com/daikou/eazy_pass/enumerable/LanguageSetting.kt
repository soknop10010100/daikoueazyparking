package com.daikou.eazy_pass.enumerable

enum class LanguageSetting {
    English, Khmer
}