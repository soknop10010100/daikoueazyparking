package com.daikou.eazy_pass.enumerable

enum class NetworkErrorStatus {
    ON_ERROR,
    BAD_REQUEST, // 400
    UNAUTHORIZED, //401
    ON_TIMEOUT,
    ON_NETWORK_ERROR,
    NOT_FOUND //404
}