package com.daikou.eazy_pass.enumerable

enum class PaymentOption {
    ChangeCard , AddNewCard
}