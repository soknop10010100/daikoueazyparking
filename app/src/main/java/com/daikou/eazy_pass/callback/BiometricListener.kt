package com.daikou.eazy_pass.callback

interface BiometricListener {
    fun onSuccess()
    fun onFailed(error : CharSequence)
}