package com.daikou.eazy_pass.callback

import com.google.gson.JsonObject

interface ApiResponseCallback<T> {

    fun onGetData(date : T)

    fun onError(message: String)
}