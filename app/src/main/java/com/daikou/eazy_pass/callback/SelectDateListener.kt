package com.daikou.eazy_pass.callback

import java.util.*

interface SelectDateListener {
    fun selectDateListener(date: Date)
}