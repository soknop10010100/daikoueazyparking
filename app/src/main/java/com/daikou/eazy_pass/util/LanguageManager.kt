package com.daikou.eazy_pass.util

import android.content.Context
import com.daikou.eazy_pass.helper.Helper
import java.util.Locale

object LanguageManager {

    const val ENGLISH = "en"
    const val KHMER = "km"

    fun getLanguage(context: Context) : String{
        val pref = context.getSharedPreferences("LANG",Context.MODE_PRIVATE)
        return pref.getString("LANG", "en") ?: "en"
    }

    fun updateResource(context: Context,languageCode: String) : Context{
        val locale = Locale(languageCode)
        Locale.setDefault(locale)
        val resources = context.resources
        val configuration = resources.configuration
        configuration.setLocale(locale)
        Helper.setStringSharePreference(context, "LANG", languageCode)
        return context.createConfigurationContext(configuration)
    }

    fun setLanguage(context: Context, languageCode : String) : Context{
        return updateResource(context, languageCode)
    }
}