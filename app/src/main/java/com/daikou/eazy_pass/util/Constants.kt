package com.daikou.eazy_pass.util

import com.daikou.eazy_pass.BuildConfig
import com.daikou.eazy_pass.helper.Helper

object Constants {

    const val CHANG_PASSWORD_CALLER = "change_password_activity"
    const val SPLASH_SCREEN = "splash_screen_activity"
    const val LOGIN_STATUS = "login_status"
    const val BIOMETRIC_KEY = "biometric_key"
    const val LIST_BANK_KEY = "listBankKey"
    const val SELECTED_BANK = "selected_bank"
    const val DEVICE_ENTRANCE_LIST = "device_entrance_list"
    const val DEVICE_EXIT_LIST = "device_exit_list"
    const val PARKING_STATUS = "parking_status"
    const val PARKING_DEVICE_LIST = "parking_device_list"
    const val exit = "exit"
    const val entrance = "entrance"

    object Config{
        const val BASE_URL = BuildConfig.BASE_URL
        const val APP_TYPE = "parking_customer"
        const val BASE_URL_TEST = "https://jsonplaceholder.typicode.com/"

        object TypeDeepLink {
            const val kessChatDeepLink = "io.kessinnovation.kesschat"
            const val acledaDeepLink = "com.domain.acledabankqr"
            const val sathapanaDeepLink = "kh.com.sathapana.consumer"
            const val abaDeepLink = "com.paygo24.ibank"
        }

        object TypeScheme {
            const val abaScheme = "abamobilebank"
            const val acledaScheme = "market"
            const val kessChatScheme = "kesspay.io"
            const val spnScheme = "spnb"
        }
    }

    object WebPay{
        const val kess_url = "https://kesspay.io"
    }

    object BankBic {
        const val bicABA = "ABAAKHPP"
        const val bicACL = "ACLBKHPP"
    }

    object KycDocStatus {
        const val Pending = "PENDING"
        const val Verified = "VERIFIED"
        const val Refused = "REFUSED"
    }

    object  Auth{
        const val TOKEN_KEY = "token_key"
        const val USER_KEY = "user_key"
        const val PASSWORD_KEY = "password_key"
    }

    object PhoneNumberConfig{
        const val PHONE_MIN_LENGTH = 9
        const val PHONE_MAX_LENGTH = 19 //last one 12
    }

    object userNotPay{
        val userIdKey  = "user_id"
        val dataEntranceKey = "dataEntranceKey"
    }

    object KycDocKey {
        const val profile = "profile"
        const val idCard = "idCard"
        const val vehicleType = "plateNumberImg"
        const val vehiclePicture = "vehiclePicture"
        const val plateNumberImg = "plateNumberImg"
        const val plateNumberText = "plateNumberText"
    }

    object QRCodeSec{
        const val secretKey = "EZ_PASS_PARKING."
        const val headerPlaintDev = "EZ_PASS_DEV_USER_QRCODE_"
        const val headerPlaintProd = "EZ_PASS_PROD_USER_QRCODE_"
    }

}