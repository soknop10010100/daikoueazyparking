package com.daikou.eazy_pass.util.redirect

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.activity.result.ActivityResult
import com.daikou.eazy_pass.data.model.response_model.user.User
import com.daikou.eazy_pass.enumerable.VerifyPinEnum
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.ui.MainActivity
import com.daikou.eazy_pass.ui.activity.*
import com.daikou.eazy_pass.ui.change_password.ChangePasswordActivity
import com.daikou.eazy_pass.ui.profile.EditProfileActivity
import com.daikou.eazy_pass.ui.profile.LongTermHistoryActivity
import com.daikou.eazy_pass.ui.splash_screen.SplashScreenActivity
import com.daikou.eazy_pass.ui.uploadDocument.UploadDocumentActivity
import com.daikou.eazy_pass.ui.wallet.DepositActivity
import com.daikou.eazy_pass.ui.webpay.WebPayActivity
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.base.BaseActivity
import com.daikou.eazy_pass.util.base.BaseViewModel

object RedirectClass : BaseActivity<BaseViewModel>() {

    fun gotoVerificationActivity(activity: Activity, action: String){
        val intent = Intent(activity, VerificationActivity::class.java)
        intent.action = action
        gotoActivity(activity, intent)
    }
    fun gotoVerificationActivity(activity: Activity, action: String, hasBiometric : Boolean){
        val intent = Intent(activity, VerificationActivity::class.java)
        intent.action = action
        intent.putExtra(VerificationActivity.hasBiometricKey, hasBiometric)
        gotoActivity(activity, intent)
        if(action == Constants.SPLASH_SCREEN){
            activity.finish()
        }
    }

    fun gotoUploadDocument(activity: Activity, bodySignup : HashMap<String, Any> ,kycTemp : HashMap<String, Any>, result: BetterActivityResult.OnActivityResult<ActivityResult>){
        val intent = Intent(activity, UploadDocumentActivity::class.java)
        intent.putExtra(SignUpActivity.SIGNUP_BODY_KEY, bodySignup)
        intent.putExtra(SignUpActivity.KYC_TEMP_KEY, kycTemp)
        gotoActivityForResult(activity, intent, result)
    }
    fun gotoUploadDocument(activity: Activity, isReUpload : Boolean ){
        val intent = Intent(activity, UploadDocumentActivity::class.java)
        intent.putExtra(UploadDocumentActivity.isReUpload, isReUpload )
        gotoActivity(activity, intent)
    }

    fun gotoSplashScreenActivity(activity: Activity){
        val intent = Intent(activity, SplashScreenActivity::class.java)
        gotoActivity(activity, intent)
    }

    fun gotoLoginActivity(activity: Activity, isClearTop: Boolean = false){
        val intent = Intent(activity, LoginActivity::class.java)
        Helper.AuthHelper.clearSession(activity)
        if (isClearTop) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        gotoActivity(activity, intent)
    }

    fun gotoMainActivity(activity: Activity, isClearTop : Boolean = false){
        val intent = Intent(activity, MainActivity::class.java)
        if (isClearTop) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        gotoActivity(activity, intent)
    }
    fun gotoForgetPasswordActivity(activity: Activity){
        val intent = Intent(activity, ForgetPasswordActivity::class.java)
        gotoActivity(activity, intent)
    }
    fun gotoSignUpActivity(activity: Activity){
        val intent = Intent(activity, SignUpActivity::class.java)
        gotoActivity(activity, intent)
    }

    fun gotoChangePasswordActivity(activity: Activity,
                                   pinCode: String,
                                   otpToken: String,
                                   verifyPinEnum: VerifyPinEnum,){
        val intent = Intent(activity, ChangePasswordActivity::class.java)
        intent.putExtra(ChangePasswordActivity.PinCodeKey, pinCode)
        intent.putExtra(ChangePasswordActivity.Otp_token_key, otpToken)
        intent.putExtra(ChangePasswordActivity.ScreenThatRequestKey, verifyPinEnum)
        gotoActivity(activity, intent)
    }

    fun gotoDepositActivity(activity: Activity){
        val intent = Intent(activity, DepositActivity::class.java)
        gotoActivity(activity, intent)
    }


    fun gotoEditProfileActivity(activity: Activity, user: User){
        val intent  = Intent(activity, EditProfileActivity::class.java)
        intent.putExtra(EditProfileActivity.user_key, user)
        gotoActivity(activity, intent)
    }

    fun gotoIdentityVerifyActivity(activity: Activity, kycData : HashMap<String, Any>){
        val intent = Intent(activity, IdentityVerifyActivity::class.java)
        intent.putExtra(IdentityVerifyActivity.KycDataKey, kycData)
        gotoActivity(activity, intent)
    }

    fun gotoWebPay(activity: Activity , url: String , result : BetterActivityResult.OnActivityResult<ActivityResult>){
        val intent = Intent(activity, WebPayActivity::class.java)
        intent.putExtra("linkUrl", url)
        gotoActivityForResult(activity, intent, result)
    }

    fun openDeepLink(activity: Activity, uri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = uri
        gotoActivity(activity, intent)
    }

    fun gotoPlayStore(activity: Activity, applicationId: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        try {
            intent.data = Uri.parse(String.format("%s%s", "market://details?id=", applicationId))
        } catch (e: Exception) {
            intent.data = Uri.parse(
                String.format(
                    "%s%s",
                    "https://play.google.com/store/apps/details?id=",
                    applicationId
                )
            )
        }
        gotoActivity(activity, intent)
    }

    fun gotoVerifyOtpCodeActivity(activity: Activity, otpToken: String) {
        val intent = Intent(activity, VerifyOtpCodeActivity::class.java)
        intent.putExtra(VerifyOtpCodeActivity.OTP_TOKEN_KEY, otpToken)
        gotoActivity(activity, intent)
    }

    fun gotoLongTermHistoryActivity(activity: Activity, projectId : Int){
        val intent  = Intent(activity, LongTermHistoryActivity::class.java)
        intent.putExtra(LongTermHistoryActivity.PROJECT_ID, projectId)
        gotoActivity(activity, intent)
    }

}