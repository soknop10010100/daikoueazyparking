package com.daikou.eazy_pass.util.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.daikou.eazy_pass.MyApplication
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.Helper.ParkingInfo.clearUserNotPay
import com.daikou.eazy_pass.helper.Helper.ParkingInfo.getDataNotPay
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.MainActivity
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.Constants
import com.daikou.eazy_pass.util.LanguageManager
import com.daikou.eazy_pass.util.broadcast_receiver.BluetoothBroadcastReceiver
import com.daikou.eazy_pass.util.broadcast_receiver.NetworkChangeReceiver
import pl.aprilapps.easyphotopicker.ChooserType
import pl.aprilapps.easyphotopicker.EasyImage
import java.lang.reflect.ParameterizedType
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

open class BaseActivity< VM: BaseViewModel> : AppCompatActivity(){

    var loadingDialog: LoadingDialog? = null
    val mContext = MyApplication.context
    protected lateinit var  mViewModel : VM
    private lateinit var networkChangeReceiver: NetworkChangeReceiver
    protected val activityLauncher: BetterActivityResult<Intent, ActivityResult> = BetterActivityResult.registerActivityForResult(self())
    private var easyImage : EasyImage? = null
    private var easyImageCallbacks : EasyImage.Callbacks? = null
    private lateinit var bluetoothReceiver: BluetoothBroadcastReceiver
    var dataNotPay: HashMap<String, HashMap<String, Any>>? = null

    fun <T : BaseActivity<VM>?> self(): T {
        return this as T
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = LoadingDialog(self())
        networkChangeReceiver = NetworkChangeReceiver()
        registerNetworkBroadcastForNougat()
        LanguageManager.setLanguage(this, LanguageManager.getLanguage(this))
        // set the whole app to PORTRAIT
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

        val parameterizedType = javaClass.genericSuperclass as? ParameterizedType

        @Suppress("UNCHECKED_CAST")
        val vmClass = parameterizedType?.actualTypeArguments?.getOrNull(0) as? Class<VM>?

        if (vmClass != null){
            mViewModel = ViewModelProvider(this)[vmClass]
        }

        doObserveLocalPayment()
    }

    open fun doObserveLocalPayment() {
        MainActivity.paymentViewModel.dataPaymentResponse.observe(this) {
            loadingDialog?.hide()
            if (it.success && hasInternetConnection()) {
                val dataNotPay = getDataNotPay(mContext)
                if (dataNotPay.containsKey(Constants.exit)) {
                    if (dataNotPay.size == 2 && !MainActivity.alreadyPaid) {
                        MainActivity.alreadyPaid = true
                        MainActivity.paymentViewModel.openGateDoPayment(dataNotPay[Constants.exit]!!)
                    } else {
                        clearUserNotPay(mContext)
                    }
                } else {
                    clearUserNotPay(mContext)
                }
                if (MainActivity.alreadyPaid){
                    clearUserNotPay(mContext)
                    MainActivity.alreadyPaid = false
                }
            }
        }

        MainActivity.paymentViewModel.errorMessage.observe(this) { error: String? ->
            loadingDialog?.hide()
            Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show()
            clearUserNotPay(mContext)
        }
    }

    open fun isConnected(mContext: Context?, isConnect: Boolean) {
        if (isConnect) {
            if (!MainActivity.isCheckConnect) {
                MainActivity.isCheckConnect = true
                dataNotPay = getDataNotPay(mContext!!)
                if (!dataNotPay!!.isEmpty()) {
                    if (dataNotPay!!.containsKey(Constants.entrance)) {
                        loadingDialog?.show()
                        MainActivity.paymentViewModel.openGateDoPayment(dataNotPay!![Constants.entrance]!!)
                    } else {
                        loadingDialog?.show()
                        MainActivity.paymentViewModel.openGateDoPayment(dataNotPay!![Constants.exit]!!)
                    }
                }
            }
        } else {
            MainActivity.isCheckConnect = false
        }
    }

     open fun registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(
                networkChangeReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(
                networkChangeReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        }
    }

    private fun unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterNetworkChanges()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LanguageManager.updateResource(newBase!!,LanguageManager.getLanguage(newBase)))
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LanguageManager.setLanguage(this, LanguageManager.getLanguage(this))
    }


    open fun isLogin() : Boolean {
        return Helper.AuthHelper.getAccessToken(self()) != ""
    }

    open fun setLoginStatus(boolean: Boolean){
        Helper.setBooleanSharePreference(self(), Constants.LOGIN_STATUS, boolean)
    }
    open fun setLoginStatus(context: Context, boolean: Boolean){

    }

    open fun getBiometric() : String? {
        return Helper.getStringSharePreference(self(), Constants.BIOMETRIC_KEY)
    }

    open  fun gotoActivity(activity: Activity,intent: Intent){
        activity.startActivity(intent)
    }
    open fun gotoActivityForResult(activity: Activity,intent: Intent, activityResult: BetterActivityResult.OnActivityResult<ActivityResult>){
        if(activity is BaseActivity<*>) {
            activity.activityLauncher.launch(intent, activityResult)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage?.let { easyImage->
            easyImageCallbacks?.let {
                easyImage.handleActivityResult(requestCode, resultCode, data,this, it)
            }
        }
    }

    open fun chooseFromGallery(chooserType: ChooserType, easyImageCallbacks: EasyImage.Callbacks){
        if(easyImage == null) initEasyImage(chooserType)
        easyImage?.let {
            it.openGallery(this)
            this.easyImageCallbacks = easyImageCallbacks
        }
    }


    open  fun initEasyImage(chooserType: ChooserType){
        easyImage = EasyImage.Builder(this)
            // Chooser only
            // Will appear as a system chooser title, DEFAULT empty string
            //.setChooserTitle("Pick media")
            // Will tell chooser that it should show documents or gallery apps
            //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)  you can use this or the one below
            .setChooserType(chooserType)
            // saving EasyImage state (as for now: last camera file link)

            // Setting to true will cause taken pictures to show up in the device gallery, DEFAULT false
            .setCopyImagesToPublicGalleryFolder(true)
            // Sets the name for images stored if setCopyImagesToPublicGalleryFolder = true
            .setFolderName("Eazy Parking")

            // Allow multiple picking
            .allowMultiple(true)
            .build();
    }

    @SuppressLint("HardwareIds")
    fun deviceId(): String? {
        return Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun hasInternetConnection(): Boolean {
        return !Helper.hasNotNetworkAvailable(self())
    }
    fun hasInternetConnectionShowDialog(): Boolean {
        return if (Helper.hasNotNetworkAvailable(self())) {
            MessageUtil.showError(self(), "", getString(R.string.no_connection_hint))
            false
        } else {
            true
        }
    }

    @Throws(Exception::class)
    open fun encryptAES128(data: ByteArray?, secretKey: SecretKey?): ByteArray? {
        val keyGenerator: KeyGenerator = KeyGenerator.getInstance("EZ_PASS_PARKING.")
        val secretKey: SecretKey = keyGenerator.generateKey()
        val cipher: Cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        return cipher.doFinal(data)
    }

    @Throws(Exception::class)
    open fun decryptAES128(encryptedData: ByteArray?, secretKey: SecretKey?): ByteArray? {
        val cipher: Cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.DECRYPT_MODE, secretKey)
        return cipher.doFinal(encryptedData)
    }
}