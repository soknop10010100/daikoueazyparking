package com.daikou.eazy_pass.util.broadcast_receiver

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService


class BluetoothBroadcastReceiver(private val activity: Activity) :BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent) {
        val action = intent.action
        if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
            when (state) {
                BluetoothAdapter.STATE_OFF -> {
                    gotoEnableBluetooth()
                }
                BluetoothAdapter.STATE_TURNING_OFF -> Toast.makeText(
                    context,
                    "Turning Bluetooth off",
                    Toast.LENGTH_LONG
                )
                BluetoothAdapter.STATE_ON -> Toast.makeText(
                    context,
                    "Bluetooth on",
                    Toast.LENGTH_LONG
                )
                BluetoothAdapter.STATE_TURNING_ON -> Toast.makeText(
                    context,
                    "Turning Bluetooth on",
                    Toast.LENGTH_LONG
                )
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun gotoEnableBluetooth(){
        val bluetoothManager: BluetoothManager = activity.getSystemService(BluetoothManager::class.java)
        val bluetoothAdapter: BluetoothAdapter = bluetoothManager.adapter
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED  &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH), 1001)
            }else{
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.BLUETOOTH), 1000)
            }

        }else{
            activity.startActivity(intent)
        }

    }
}