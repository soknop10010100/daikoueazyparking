package com.daikou.eazy_pass.util.base

interface BaseListenerAutoFillNumber<T> {
    fun onResult(data: T)
}