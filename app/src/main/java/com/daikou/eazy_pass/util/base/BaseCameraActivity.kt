package com.daikou.eazy_pass.util.base

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import com.daikou.eazy_pass.databinding.LayoutEditProfileImgBinding
import com.daikou.eazy_pass.helper.PermissionHelper
import com.github.dhaval2404.imagepicker.ImagePicker

class BaseCameraActivity: BaseActivity<BaseViewModel>() {
    private lateinit var binding : LayoutEditProfileImgBinding
    private var isProfile : Boolean = false
    private var isIdCard : Boolean = false
    private var isVehicle : Boolean = false
    private var isPlateNumber : Boolean = false
    companion object{
        const val isIdCardKey = "camera_only"
        const val isProfileKey = "selfie_profile"
        const val isVehicleKey = "take_vehicle_picture"
        const val isPlateNumberKey = "take_plate_number"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LayoutEditProfileImgBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = "Select Option"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            PermissionHelper.requestMultiPermission(self(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_MEDIA_IMAGES)){ hasPermission ->
                if (hasPermission){
                    binding.selectPhoto.setOnClickListener {
                        chooseFromGallery()
                    }
                    binding.takePhoto.setOnClickListener {
                        takeCameraPhoto()
                    }
                }
            }
        }else{
            PermissionHelper.requestMultiPermission(self(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)){ hasPermission ->
                if (hasPermission){
                    binding.selectPhoto.setOnClickListener {
                        chooseFromGallery()
                    }
                    binding.takePhoto.setOnClickListener {
                        takeCameraPhoto()
                    }
                }
            }
        }

        if (intent.hasExtra(isIdCardKey)) {
            isIdCard = intent.getBooleanExtra(isIdCardKey,false)
        }else if(intent.hasExtra(isVehicleKey)){
            isVehicle = intent.getBooleanExtra(isVehicleKey, false)
        }else if(intent.hasExtra(isPlateNumberKey)){
            isPlateNumber = intent.getBooleanExtra(isPlateNumberKey, false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            val intent = Intent()
            assert(data != null)
            val imageUri = data!!.data
            intent.putExtra("image", imageUri.toString())
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun chooseFromGallery(){
        val builder: ImagePicker.Builder =
            ImagePicker.Builder(this).galleryOnly().crop()
        builder.start()
    }
    private fun takeCameraPhoto(){
        if (isVehicle){
            val builder: ImagePicker.Builder =
                ImagePicker.Builder(this).cameraOnly().crop(0.9f, 0.7f)
            builder.start()
        }else if (isIdCard || isPlateNumber){
            val builder: ImagePicker.Builder =
                ImagePicker.Builder(this).cameraOnly().crop(0.9f, 0.6f)
            builder.start()
        }else if (isProfile){
            val builder: ImagePicker.Builder =
                ImagePicker.Builder(this).cameraOnly().cropSquare().crop()
            builder.start()
        }else{
            val builder: ImagePicker.Builder =
                ImagePicker.Builder(this).cameraOnly().cropSquare().crop()
            builder.start()
        }
    }
}