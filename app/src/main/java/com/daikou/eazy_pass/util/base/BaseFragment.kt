package com.daikou.eazy_pass.util.base

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.helper.Helper
import com.daikou.eazy_pass.helper.MessageUtil
import com.daikou.eazy_pass.ui.bluetooth_scan.DoScanBluetoothActivity
import com.daikou.eazy_pass.util.BetterActivityResult
import com.daikou.eazy_pass.util.redirect.RedirectClass
import java.lang.reflect.ParameterizedType

open class BaseFragment<VM : BaseViewModel> : Fragment() {

    protected lateinit var fContext : FragmentActivity
    protected val activityLauncher: BetterActivityResult<Intent, ActivityResult> =
        BetterActivityResult.registerActivityForResult(this)

    protected lateinit var mViewModel: VM
    override fun onAttach(context: Context) {
        super.onAttach(context)
        fContext = context as FragmentActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val parameterizedType = javaClass.genericSuperclass as? ParameterizedType

        @Suppress("UNCHECKED_CAST")
        val vmClass = parameterizedType?.actualTypeArguments?.getOrNull(0) as? Class<VM>?

        if(vmClass != null)
            mViewModel = ViewModelProvider(this)[vmClass]
    }

    fun gotoEnableBluetooth() {
        if (!isBluetoothEnable()) {
            val intent1 = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivity(intent1)
        }
    }
    fun isBluetoothEnable() : Boolean{
        val bluetoothManager: BluetoothManager = fContext.getSystemService<BluetoothManager>(
            BluetoothManager::class.java
        )
        val bluetoothAdapter = bluetoothManager.adapter
        return  bluetoothAdapter.isEnabled
    }

    fun gotoDoScanBluetoothActivity(activity: Activity, result : BetterActivityResult.OnActivityResult<ActivityResult> ){
        val intent = Intent(activity, DoScanBluetoothActivity::class.java)
        RedirectClass.gotoActivityForResult(activity, intent, result);
    }

    fun hasInternetConnectionShowDialog(): Boolean {
        return if (Helper.hasNotNetworkAvailable(fContext)) {
            MessageUtil.showError(fContext, "", getString(R.string.no_connection_hint))
            false
        } else {
            true
        }
    }

    fun hasInternetConnection(): Boolean {
        return !Helper.hasNotNetworkAvailable(fContext)
    }

}