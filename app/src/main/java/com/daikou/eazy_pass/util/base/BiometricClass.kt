package com.daikou.eazy_pass.util.base


import android.content.Context
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.PromptInfo
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.daikou.eazy_pass.R
import com.daikou.eazy_pass.callback.BiometricListener
import java.util.concurrent.Executor

class BiometricClass {

    private lateinit var listener : BiometricListener
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var context: Context
    private lateinit var promptInfo: PromptInfo
    private var executor : Executor? = null

    companion object {
        fun newInstance(
            context: Context,
            listener: BiometricListener
        ) = BiometricClass().apply {
            this.listener = listener
            this.context = context
            checkBiometric()
        }
    }

    private fun checkBiometric() {
        val biometricManager = BiometricManager.from(
            context
        )
        val error: String

        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.BIOMETRIC_WEAK)) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                showBiometricDialog()
            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                error = "No biometric features available on this device"
                showToast(error)
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                error = "Biometric features are currently unavailable."
                showToast(error)
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                error = "The user hasn't associated any biometric credentials with their account."
                showToast(error)
            }
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> {
                error = "Biometric features are currently security update required."
                showToast(error)
            }
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> {
                error = "Biometric features are currently unsupported."
                showToast(error)
            }
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> {
                error = "Biometric features are currently status unknown."
                showToast(error)
            }
        }
    }

    private fun showBiometricDialog() {
        executor = ContextCompat.getMainExecutor(context)
        biometricPrompt = BiometricPrompt(
            (context as FragmentActivity?)!!,
            executor!!, object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    listener.onFailed(errString)
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    listener.onSuccess()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    showToast("Authentication failed")
                    listener.onFailed("Authentication failed")
                }
            })


        //create prompt dialog by using Biometric Library
        promptInfo = PromptInfo.Builder()
            .setTitle(context.getString(R.string.biometric_title) + " " + "Eazy Parking")
            .setSubtitle(context.getString(R.string.auth_with_biometric))
            .setNegativeButtonText(context.getString(R.string.action_cancel))
            .setConfirmationRequired(false)
            .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.BIOMETRIC_WEAK)
            .build()

        biometricPrompt.authenticate(promptInfo)
    }

    private fun showToast(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }
}