package com.daikou.eazy_pass.util.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.daikou.eazy_pass.MyApplication

open class BaseViewModel : ViewModel() {
    val context = MyApplication.context
    var loading = MutableLiveData<Boolean>()
}