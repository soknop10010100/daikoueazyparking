package com.daikou.eazy_pass

import android.annotation.SuppressLint
import android.provider.Settings
import androidx.multidex.MultiDexApplication

class MyApplication : MultiDexApplication() {
    companion object{
        lateinit var context : MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
    @SuppressLint("HardwareIds")
    fun deviceId(): String? {
        return Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
    }
}